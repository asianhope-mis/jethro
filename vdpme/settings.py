'''
Django settings for vdpme project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
'''

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
from os.path import join
from django.utils.translation import ugettext_lazy as _
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
from django.forms import Field
from jethro_database import DATABASES
from jethro_database import GITLAB

Field.default_error_messages = {
    'required': _('This field is required.'),
    'max_whole_digits':_('Ensure that there are no more than 3 digits before the decimal point.'),
    'max_digits':_('Ensure that there are no more than 5 digits in total.'),
    'invalid_choice':_('Select a valid choice. That choice is not one of the available choices.')
}


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
from jethro_database import SECRET_KEY

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            join(BASE_DIR, 'templates/'),
            join(BASE_DIR, 'mande/templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': True,
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages',
                'mande.context_processors.menu',
                'mande.context_processors.search_sugestion',
                'django.template.context_processors.request'
            ],
        },
    },
]
ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'mathfilters',
    'bootstrapform',
    'mande',
    'bootstrap3',
    'django_crontab',
    'django_extensions'
)

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
   'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'vdpme.middleware.LoginRequiredMiddleware',
    'django.middleware.cache.FetchFromCacheMiddleware',
)

STRONGHOLD_DEFAULTS = True

ROOT_URLCONF = 'vdpme.urls'

WSGI_APPLICATION = 'vdpme.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

'''
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'vdpme',
	'USER': 'django',
	'PASSWORD': 'django',
	'OPTIONS': {
			'charset': 'utf8',
			'use_unicode': True, },
    }
}
'''
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

CACHE_MIDDLEWARE_ALIAS = 'default'
CACHE_MIDDLEWARE_SECONDS = 0
from jethro_database import CACHE_MIDDLEWARE_KEY_PREFIX
# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

LANGUAGES = (
    ('en', _('English')),
    ('kh', _('Khmer')),
)
LOCALE_PATHS = (
        os.path.join(BASE_DIR, 'locale'),
    )

TIME_ZONE = 'Asia/Phnom_Penh'

USE_I18N = True

USE_L10N = True
LOGIN_EXEMPT_URLS =['authorize','oauth2callback']
LOGIN_URL = 'login'
LOGIN_REDIRECT_URL = 'index'
USE_TZ = True
# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
# MEDIA_URL = '/media/'
# MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/student_photos/'
from jethro_database import STATIC_ROOT
from jethro_database import MEDIA_ROOT

CRONTAB_DJANGO_PROJECT_NAME = BASE_DIR.split('/')[-1];
CRONJOBS = [
    ('00 01 * * *', 'mande.cron.my_scheduled_job','>> '+BASE_DIR+'/generatestudentcachetable.log')
]



GOOGLEAUTHENTICATION_CREDENTIALS = {}
GOOGLE_OAUTH2_SCOPES = [
        'https://www.googleapis.com/auth/userinfo.email',
        'https://www.googleapis.com/auth/userinfo.profile'
        ]
GOOGLE_OAUTH2_CLIENT_SECRETS_JSON = os.path.join(
    os.path.dirname(__file__),
    'client_secret_OAuth_2.0_Web_client.json'
    )

AUTHENTICATION_BACKENDS = [
    'mande.backend.GoogleAuthBackend'
]
