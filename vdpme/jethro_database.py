import os
from os.path import join
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

DATABASES = {
        'default': {
            'ENGINE':'django.db.backends.sqlite3',
            'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
            }
}

GITLAB = {
    'TOKEN': 'your token go here',
    'PROJECT_ID' : '4479026'
}

CACHE_MIDDLEWARE_KEY_PREFIX = 'jethro'
STATIC_ROOT='/var/www/jethro/mande/static'
MEDIA_ROOT ='/opt/student_photos'
SECRET_KEY = 'hm#&=u&2%*)-#^p!p@bd08wj80s6#n4i(3)!fom#ko8x!7*4^)'
