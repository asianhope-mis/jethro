# Asian Hope's Village Development Program's Monitoring and Evaluation
=====================================

* Technology : Python, Django
* Datebase : MySQL

## Requirements
* `pip install -r requirements.txt`

## Configuration
1. edit the `vdpme/jethro_database.py` file to suit your setup
    * `DATABASES`

    Example:
    ```
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.mysql',
            'NAME': 'replace_with_your_database_name',
            'USER': 'replace_with_your_database_username',
            'PASSWORD': 'replace_with_your_database_password',
            'OPTIONS': {
                            'charset': 'utf8',
                            'use_unicode': True,
                        },
        }
    }
    ```
2. add google OAuth 2.0 client secrets file `vdpme/client_secret_OAuth_2.0_Web_client.json` (client secrets file get from https://console.developers.google.com/)


## Additional command
* `python manage.py makemigrations,` for creating new migrations based on the changes you have made to your models.
* `python manage.py migrate` for applying migrations
* `python manage.py runscript update_group_permissions` for updating permission to DB based on permission file `mande/permissions.py`
* `python manage.py compilemessages -l kh` for compiling Khmer translation based on the changes in file `locale/kh/LC_MESSAGES/django.po` (#Note: it is required gettext to run this command `sudo apt-get install gettext`)

## Django-cron
we use django cron to sync student data to `CurrentStudentInfo` table, it make `School Management` -> `Student Information` fetching data faster.

Here is the script for synchronization `mande/cron.py`

This is crontab configuration in `vdpme/settings.py`, it run 1:00 am every day.

```
CRONJOBS = [
    ('00 01 * * *', 'mande.cron.my_scheduled_job','>> '+BASE_DIR+'/generatestudentcachetable.log')
]
```

#### How to set up ?

*  show current crontab job: `python manage.py crontab show`
*  add all defined jobs from `CRONJOBS` to crontab: `python manage.py crontab add`
* remove all defined job `python manage.py crontab remove`

https://pypi.org/project/django-crontab/


## Running a local testing server
* `python manage.py runserver`


## Running unittests
* `python manage.py test`

## Deployment:
#### CI/CD:
https://gitlab.com/asianhope-mis/jethro/pipelines

* push a new branch => deploy to  `testing`
* push or merge to `master` branch => deploy to `staging`
* push a new tag => deploy to `live`
