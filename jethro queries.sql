/***************************************************
PROGRAM STATS - for site overview info spreadsheet
***************************************************/

-- # of students
select school_name, cohort, COUNT(student_id_id)
from (
	select sc.school_name,
		ce.student_id_id,
		case (c.cohort DIV 10) when 0 then 'Catch-Up' when 7 then 'Vietnamese' when 5 then 'English' end as cohort
	from mande_classroomenrollment ce
		join mande_classroom c on ce.classroom_id_id = c.classroom_id
		join mande_school sc on c.school_id_id = sc.school_id
	where c.active=true
		and ce.drop_date is null
		--and sc.school_name = 'VDP-KR'
	order by ce.student_id_id
) t
group by school_name, cohort
order by school_name, cohort

-- # of students at PKPN enrolled in catch-up and Vietnamese
select COUNT(num_classes)
from (
	select ce.student_id_id,
		count(*) as num_classes
	from mande_classroomenrollment ce
		join mande_classroom c on ce.classroom_id_id = c.classroom_id
		join mande_school sc on c.school_id_id = sc.school_id
	where c.active=true
		and ce.drop_date is null
		and sc.school_name = 'VDP-PKPN'
	group by ce.student_id_id
	having count(*) > 1
) t

-- % of students enrolled in public school, by site
select site, SUM(enrolled) / COUNT(student_id)
from (
	-- most recent public school history record
	select sc.school_name as site,
		case c.cohort DIV 10 when 0 then concat('G',c.cohort) when 7 then 'VN' when 5 then concat('E',replace(c.classroom_number, 'Level ', '')) end as cohort,
		st.student_id,
		st.name,
		ifnull((select enroll_date from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as enroll_date,
		(select case when enroll_date is null then '' else ifnull(drop_date, 'Ongoing') end from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1) as drop_date,
		(select case when status='Y' then 'Enrolled' when status='N' then 'Not Enrolled' else ifnull(status,'') end from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1) as status,
		ifnull((select concat('Grade ',grade) from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as grade,
		ifnull((select school_name from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as public_school_name,
		ifnull((select replace(reasons, '\r\n', ' ') from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as reasons,
		(select case when status='Y' then 1 else 0 end from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1) as enrolled
	from mande_intakesurvey st
		join mande_classroomenrollment ce on st.student_id = ce.student_id_id
		join mande_classroom c on ce.classroom_id_id = c.classroom_id
		join mande_school sc on c.school_id_id = sc.school_id
	where c.active=true
		and ce.drop_date is null
		and c.cohort DIV 10 <> 5 -- excluding English students
		and c.cohort DIV 10 <> 7 -- excluding Vietnamese students
	order by 1,2,4,5
) t
group by site

-- average lag, by site
select school_name, AVG(lag)
from (
	select sc.school_name,
		ce.student_id_id,
		i.name,
		i.dob,
		DATE_FORMAT(FROM_DAYS(DATEDIFF(CURDATE(),i.dob)), '%Y')+0 as age,
		case when MONTH(CURDATE()) < 8 then YEAR(CURDATE()) - YEAR(i.dob) - 6 else YEAR(CURDATE()) - YEAR(i.dob) - 5 end as age_appropriate_grade,
		c.cohort as grade,
		case when MONTH(CURDATE()) < 8 then YEAR(CURDATE()) - YEAR(i.dob) - 6 else YEAR(CURDATE()) - YEAR(i.dob) - 5 end - c.cohort as lag
	from mande_classroomenrollment ce
		join mande_classroom c on ce.classroom_id_id = c.classroom_id
		join mande_school sc on c.school_id_id = sc.school_id
		join mande_intakesurvey i on ce.student_id_id = i.student_id
	where c.active=true
		and ce.drop_date is null
		and c.cohort DIV 10 = 0
	order by sc.school_name, grade, lag
) t
group by school_name

-- count of students ages 10 and up in G1 or G2, by site
select school_name, COUNT(student_id_id)
from (
	select sc.school_name,
		ce.student_id_id,
		i.name,
		i.dob,
		DATE_FORMAT(FROM_DAYS(DATEDIFF(CURDATE(),i.dob)), '%Y')+0 as age,
		c.cohort as grade
	from mande_classroomenrollment ce
		join mande_classroom c on ce.classroom_id_id = c.classroom_id
		join mande_school sc on c.school_id_id = sc.school_id
		join mande_intakesurvey i on ce.student_id_id = i.student_id
	where c.active=true
		and ce.drop_date is null
		and DATE_FORMAT(FROM_DAYS(DATEDIFF(CURDATE(),i.dob)), '%Y')+0 >= 10
		and c.cohort in (1,2)
	order by sc.school_name, grade, age
) t
group by school_name

-- % of students that participate in a local church, by site
select site, SUM(attend_local_church) / COUNT(student_id)
from (
	-- get spiritual activity surveys - all students, most recent survey
	select sc.school_name 'site',
		case c.cohort DIV 10 when 5 then 'English' when 7 then 'VN' else c.cohort end as grade,
		c.classroom_number,
		st.student_id,
		st.name,
		st_i.enrollment_date,
		ifnull((select date from mande_spiritualactivitiessurvey where student_id_id = st.student_id order by date desc limit 1),'') as date,
		ifnull((select case personal_attend_church when 'Y' then 'Yes' when 'N' then 'No' else personal_attend_church end from mande_spiritualactivitiessurvey where student_id_id = st.student_id order by date desc limit 1),'') as personal_attend_church,
		ifnull((select case frequency_of_attending when 'EVERY_YEAR' then 'Once or twice per year' when 'EVERY_WEEK' then 'Almost every week' when 'EVERY_MONTH' then 'Once every 1-2 months' else frequency_of_attending end from mande_spiritualactivitiessurvey where student_id_id = st.student_id order by date desc limit 1),'') as frequency,
		ifnull((select church_name from mande_spiritualactivitiessurvey where student_id_id = st.student_id order by date desc limit 1),'') as church_name,
		(select case when personal_attend_church='Y' and (frequency_of_attending='EVERY_WEEK' or frequency_of_attending='EVERY_MONTH') then 1 else 0 end  from mande_spiritualactivitiessurvey where student_id_id = st.student_id order by date desc limit 1) as attend_local_church
	from mande_intakesurvey st
	  join mande_school sc on st.site_id = sc.school_id
	  join mande_intakeinternal st_i on st.student_id = st_i.student_id_id
	  left join (select st.student_id, max(exit_date) as exit_date from mande_intakesurvey st left join mande_exitsurvey ex on st.student_id = ex.student_id_id group by st.student_id) ex on st.student_id = ex.student_id
	  join mande_classroomenrollment ce on ce.student_id_id = st.student_id
	  join mande_classroom c on ce.classroom_id_id = c.classroom_id
	where ex.exit_date is null
	  and c.active=true
	  and ce.drop_date is null
	order by sc.school_name, attend_local_church, grade, c.classroom_number, st.name
) t
where attend_local_church is not null
group by site

/***************************************************
M&E - for M&E team
***************************************************/
-- list of students enrolled in Vietnamese and Catch-Up
select st.student_id, st.name
from (
	select ce.student_id_id,
		count(*) as num_classes
	from mande_classroomenrollment ce
		join mande_classroom c on ce.classroom_id_id = c.classroom_id
		join mande_school sc on c.school_id_id = sc.school_id
	where c.active=true
		and ce.drop_date is null
		and sc.school_name = 'VDP-PKPN'
	group by ce.student_id_id
	having count(*) > 1) t
join mande_intakesurvey st on t.student_id_id = st.student_id

/***************************************************
PUBLIC SCHOOL - for public school enrollment spreadsheet
***************************************************/

-- public school info for current school year
select sc.school_name as site,
	concat('G',c.cohort) as vdp_grade,
	st.student_id,
	st.name,
	ifnull(psh.enroll_date,'') as enroll_date,
	case when psh.enroll_date is null then '' else ifnull(psh.drop_date, 'Ongoing') end as drop_date,
	case when psh.status='Y' then 'Enrolled' when psh.status='N' then 'Not Enrolled' else ifnull(psh.status,'') end as public_school_status,
	ifnull(concat('Grade ',psh.grade),'') as public_school_grade,
	ifnull(psh.school_name,'') as public_school_name,
	ifnull(replace(psh.reasons, '\r\n', ' '),'') as public_school_reason,
	case when psh.status='Y' then 1 else 0 end as enrolled
from mande_intakesurvey st
	join mande_classroomenrollment ce on st.student_id = ce.student_id_id
	join mande_classroom c on ce.classroom_id_id = c.classroom_id
	join mande_school sc on c.school_id_id = sc.school_id
	join mande_publicschoolhistory psh on st.student_id = psh.student_id_id
where c.active=true
	and ce.drop_date is null
	and psh.enroll_date >= '2017-11-01' -- current school year
	and c.cohort DIV 10 = 0 -- only catch-up students
order by 1,2,4,5

-- audit: current year missing
select *
from
	(select sc.school_name as site,
		concat('G',c.cohort) as vdp_grade,
		st.student_id,
		st.name,
		st_i.enrollment_date as vdp_enroll_date,
		ifnull((select enroll_date from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1), '') as last_public_school_enroll_date,
		ifnull((select case when enroll_date is null then '' else ifnull(drop_date, 'Ongoing') end from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1), '') as last_public_school_drop_date,
		(select case when status='Y' then 'Enrolled' when status='N' then 'Not Enrolled' else ifnull(status,'') end from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1) as status,
		ifnull((select concat('Grade ',grade) from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as grade,
		ifnull((select school_name from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as public_school_name,
		ifnull((select replace(reasons, '\r\n', ' ') from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as reasons
	from mande_intakesurvey st
		join mande_classroomenrollment ce on st.student_id = ce.student_id_id
		join mande_classroom c on ce.classroom_id_id = c.classroom_id
		join mande_school sc on c.school_id_id = sc.school_id
		join mande_intakeinternal st_i on st.student_id = st_i.student_id_id
	where c.active=true
		and ce.drop_date is null
		and c.cohort DIV 10 = 0 -- only catch-up students
	order by 1,2,4
)t
where vdp_enroll_date < DATE_SUB(CURDATE(), INTERVAL 10 MONTH) -- enrolled in VDP over 10 months ago
	and (last_public_school_enroll_date = '' or
		last_public_school_enroll_date < '2017-08-01')

-- audit: previous year not yet dropped
select sc.school_name as site,
	concat('G',c.cohort) as vdp_grade,
	st.student_id,
	st.name,
	st_i.enrollment_date as vdp_enroll_date,
	psh.enroll_date,
	psh.drop_date,
	case when status='Y' then 'Enrolled' when status='N' then 'Not Enrolled' end as status,
	ifnull(psh.grade,'') as grade,
	ifnull(psh.school_name,'') as public_school_name,
	replace(reasons, '\r\n', ' ') as reasons
from mande_intakesurvey st
	join mande_classroomenrollment ce on st.student_id = ce.student_id_id
	join mande_classroom c on ce.classroom_id_id = c.classroom_id
	join mande_school sc on c.school_id_id = sc.school_id
	join mande_intakeinternal st_i on st.student_id = st_i.student_id_id
	join mande_publicschoolhistory psh on st.student_id = psh.student_id_id
where c.active=true
	and ce.drop_date is null
	and c.cohort DIV 10 = 0 -- only catch-up students
	and YEAR(psh.enroll_date) = 2016
	and psh.drop_date is null
order by 1,2,4

-- audit: current public school not found
select *
from
	(select sc.school_name as site,
		concat('G',c.cohort) as vdp_grade,
		st.student_id,
		st.name,
		st_i.enrollment_date as vdp_enroll_date,
		ifnull((select enroll_date from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1), '') as last_public_school_enroll_date,
		ifnull((select case when enroll_date is null then '' else ifnull(drop_date, 'Ongoing') end from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1), '') as last_public_school_drop_date,
		(select case when status='Y' then 'Enrolled' when status='N' then 'Not Enrolled' else ifnull(status,'') end from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1) as status,
		ifnull((select concat('Grade ',grade) from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as grade,
		ifnull((select school_name from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as public_school_name,
		ifnull((select replace(reasons, '\r\n', ' ') from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as reasons
	from mande_intakesurvey st
		join mande_classroomenrollment ce on st.student_id = ce.student_id_id
		join mande_classroom c on ce.classroom_id_id = c.classroom_id
		join mande_school sc on c.school_id_id = sc.school_id
		join mande_intakeinternal st_i on st.student_id = st_i.student_id_id
	where c.active=true
		and ce.drop_date is null
		and c.cohort DIV 10 = 0 -- only catch-up students
	order by 1,2,4
)t
where vdp_enroll_date < DATE_SUB(CURDATE(), INTERVAL 10 MONTH) -- enrolled in VDP over 10 months ago
	and last_public_school_drop_date <> 'Ongoing'

-- audit: not enrolled; has school name and grade
select *
from
	(select sc.school_name as site,
		concat('G',c.cohort) as vdp_grade,
		st.student_id,
		st.name,
		st_i.enrollment_date as vdp_enroll_date,
		ifnull((select enroll_date from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1), '') as last_public_school_enroll_date,
		ifnull((select case when enroll_date is null then '' else ifnull(drop_date, 'Ongoing') end from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1), '') as last_public_school_drop_date,
		(select case when status='Y' then 'Enrolled' when status='N' then 'Not Enrolled' else ifnull(status,'') end from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1) as status,
		ifnull((select concat('Grade ',grade) from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as grade,
		ifnull((select school_name from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as public_school_name,
		ifnull((select replace(reasons, '\r\n', ' ') from mande_publicschoolhistory where student_id_id = st.student_id order by enroll_date desc limit 1),'') as reasons
	from mande_intakesurvey st
		join mande_classroomenrollment ce on st.student_id = ce.student_id_id
		join mande_classroom c on ce.classroom_id_id = c.classroom_id
		join mande_school sc on c.school_id_id = sc.school_id
		join mande_intakeinternal st_i on st.student_id = st_i.student_id_id
	where c.active=true
		and ce.drop_date is null
		and c.cohort DIV 10 = 0 -- only catch-up students
	order by 1,2,4
)t
where status = 'Not Enrolled' and (grade <> '' or public_school_name <> '')


-- duplicate public school info
select student_id_id, enroll_date, count(*) from mande_publicschoolhistory group by student_id_id, enroll_date having count(*) > 1

/***************************************************
CHURCH SURVEY - for church survey spreadsheet
***************************************************/
-- get church surveys - all students, all surveys
select sc.school_name 'site',
	case when c.cohort between 1 and 6 then concat('G',c.cohort) when c.cohort = 70 then 'VN' when c.cohort = 50 then concat('E',replace(c.classroom_number, 'Level ', '')) end as cohort,
	st.student_id,
	st.name,
	ifnull(sas.date,'') as date,
	case sas.personal_attend_church when 'Y' then 'Yes' when 'N' then 'No' else ifnull(sas.personal_attend_church,'') end as personal_attend_church,
	case sas.frequency_of_attending when 'EVERY_YEAR' then 'Once or twice per year' when 'EVERY_WEEK' then 'Almost every week' when 'EVERY_MONTH' then 'Once every 1-2 months' else ifnull(sas.frequency_of_attending,'') end as frequency,
	ifnull(sas.church_name,'') as church_name
from mande_intakesurvey st
  join mande_school sc on st.site_id = sc.school_id
  join mande_intakeinternal st_i on st.student_id = st_i.student_id_id
  join mande_classroomenrollment ce on ce.student_id_id = st.student_id
  join mande_classroom c on ce.classroom_id_id = c.classroom_id
  left join mande_spiritualactivitiessurvey sas on st.student_id = sas.student_id_id
where c.active=true
  and ce.drop_date is null
order by sc.school_name, cohort, c.classroom_number, st.name, sas.date desc

select * from mande_spiritualactivitiessurvey

-- check for duplicate spiritual activity surveys
select student_id_id from mande_spiritualactivitiessurvey group by student_id_id having count(*) > 1 order by student_id_id

/***************************************************
ACHIEVEMENT TEST
***************************************************/

-- for achievement test results spreadsheet
select sc.school_name 'site', st.name, st.student_id, a.test_date,
	a.test_level,
	a.test_grade_math,
	a.test_grade_khmer,
	if (a.promote, 1, 0) as promote,
	st_i.enrollment_date as enroll_date,
	ifnull(ex.exit_date, "") as exit_date,
	TIMESTAMPDIFF(MONTH, st_i.enrollment_date, ifnull(ex.exit_date, NOW())) as months_enrolled,
	TIMESTAMPDIFF(MONTH, st_i.enrollment_date, a.test_date) as months_since_enrollment
from mande_academic a
  join mande_intakesurvey st on a.student_id_id = st.student_id
  join mande_school sc on st.site_id = sc.school_id
  join mande_intakeinternal st_i on st.student_id = st_i.student_id_id
  left join (select st.student_id, max(exit_date) as exit_date from mande_intakesurvey st left join mande_exitsurvey ex on st.student_id = ex.student_id_id group by st.student_id) ex on a.student_id_id = ex.student_id
where a.test_grade_math is not null
  and a.test_grade_khmer is not null
  and a.test_date = '2017-07-31'
  and sc.school_name = 'VDP-PKPN'
order by sc.school_name, a.student_id_id, a.test_date

-- get promotion results for last two test cycles
select sc.school_name, i.student_id, replace(i.name, '\n', ' ') as name, st_i.enrollment_date, ifnull(ex.exit_date, "") as exit_date,
	ifnull((select promote from mande_academic where test_date between '2017-01-20' and '2017-02-28' and student_id_id = i.student_id and test_grade_math is not null and test_grade_khmer is not null order by test_date limit 1),'N/A') as promote_Feb2017,
	ifnull((select promote from mande_academic where test_date between '2017-07-14' and '2017-07-31' and student_id_id = i.student_id and test_grade_math is not null and test_grade_khmer is not null order by test_date limit 1),'N/A') as promote_Jul2017
from mande_intakesurvey i
join mande_school sc on i.site_id = sc.school_id
join mande_intakeinternal st_i on i.student_id = st_i.student_id_id
left join (select student_id_id, max(exit_date) as exit_date from mande_exitsurvey ex group by student_id_id) ex on i.student_id = ex.student_id_id
left join (select student_id_id, count(*) from mande_academic where test_date between '2017-01-20' and '2017-07-31' and test_grade_math is not null and test_grade_khmer is not null group by student_id_id) as tests on i.student_id = tests.student_id_id
where (ex.exit_date is null or ex.exit_date >= '2017-01-20')
	and tests.student_id_id is not null
	--and sc.school_name = 'VDP-PPT'
order by sc.school_name

-- number of student promotions by year
select school_year, count(distinct student_id_id)
from (select *,
	case when test_date between '2014-10-03' and '2015-07-28' then '2014-15'
		when test_date between '2016-01-28' and '2016-07-29' then '2015-16'
		when test_date between '2017-01-27' and '2017-07-31' then '2016-17'
		else test_date end as school_year
	from mande_academic
	where promote = 1
		and test_date > '2014-10-01'
		and test_grade_math is not null
		and test_grade_khmer is not null) t
group by school_year

/***************************************************
TOTAL STUDENTS IMPACTED
***************************************************/

-- total students impacted = sum of net increase + turnover for all years

-- number of students exited, by year (related to turnover)
select year(exit_date), count(distinct student_id_id) from mande_exitsurvey ex group by year(exit_date) order by 1

-- turnover = (number of exits) / (starting enrollment)

-- total count of unique students in jethro (helpful comparison to validate total # of students impacted)
select count(student_id) from mande_intakesurvey

/***************************************************
OTHER QUERIES
***************************************************/

/*
age_appropriate_grade = case when MONTH(CURDATE()) < 8 then YEAR(CURDATE()) - YEAR(i.dob) - 6 else YEAR(CURDATE()) - YEAR(i.dob) - 5 end
lag = age_appropriate_grade - current_grade
*/

-- list of all classes
select sc.school_name, case c.cohort when 50 then 'English' when 70 then 'VN' else c.cohort end as grade, c.classroom_number, ct.teachers, students.num_students
from mande_classroom c
	join mande_school sc on c.school_id_id = sc.school_id
	join (select classroom_id_id, count(*) num_students
		from mande_classroomenrollment
		where drop_date is null
		group by classroom_id_id) students on students.classroom_id_id = c.classroom_id
	join (select ct.classroom_id_id, group_concat(t.name separator ', ') as teachers
		from mande_classroomteacher ct
			join mande_teacher t on ct.teacher_id_id = t.teacher_id
		group by ct.classroom_id_id) ct on ct.classroom_id_id = c.classroom_id
where c.active=true
order by sc.school_name, grade, classroom_number

-- get number of promotions for each student
set @begin_sy = '2016-01-01', @end_sy = '2016-12-31';

select sc.school_name, s.student_id, i.name, st_i.enrollment_date, ifnull(ex.exit_date, "") as exit_date, s.test_sy, s.promotions, s.tests_taken, s.last_test_date,
	(select max(test_grade_math) from mande_academic where student_id_id = s.student_id and test_date = s.last_test_date) as last_math,
	(select max(test_grade_khmer) from mande_academic where student_id_id = s.student_id and test_date = s.last_test_date) as last_khmer
from (select student_id_id AS student_id,
		if (month(test_date)>=8,concat(year(test_date),'-',(year(test_date)+1)%100),concat(year(test_date)-1,'-',(year(test_date))%100)) as test_sy,
		SUM( promote ) AS promotions,
		COUNT( id ) AS tests_taken,
		MAX(test_grade_math) as max_math,
		MAX(test_grade_khmer) as max_khmer,
		MAX(test_date) as last_test_date
	from mande_academic
	where test_date between @begin_sy and @end_sy and
	  test_grade_math is not null and
	  test_grade_khmer is not null
	GROUP BY student_id_id, test_sy) s
join mande_intakesurvey i on s.student_id = i.student_id
join mande_school sc on i.site_id = sc.school_id
join mande_intakeinternal st_i on s.student_id = st_i.student_id_id
left join (select student_id_id, max(exit_date) as exit_date from mande_exitsurvey ex group by student_id_id) ex on s.student_id = ex.student_id_id
left join (select student_id_id, max(test_level)+1 as current_grade from mande_academic where promote = 1 group by student_id_id) p on i.student_id = p.student_id_id
order by current_grade, s.promotions desc, sc.school_name

	--and (ex.exit_date is null or ex.exit_date >= @begin_sy)
	--and sc.school_name = 'VDP-TK'

-- get students enrolled full year but did not take a test
set @begin_sy = '2015-08-01', @end_sy = '2016-07-31';

select sc.school_name, i.student_id, i.name, st_i.enrollment_date, ifnull(ex.exit_date, "") as exit_date --, p.starting_grade, st_i.starting_grade
from mande_intakesurvey i
	join mande_school sc on i.site_id = sc.school_id
	join mande_intakeinternal st_i on i.student_id = st_i.student_id_id
	left join (select student_id_id, max(exit_date) as exit_date from mande_exitsurvey ex group by student_id_id) ex on i.student_id = ex.student_id_id
	left join (select student_id_id, max(test_level)+1 as starting_grade from mande_academic where test_date < @begin_sy and promote = 1 group by student_id_id) p on i.student_id = p.student_id_id
	left join (select distinct student_id_id from mande_academic where test_date between @begin_sy and @end_sy) tests on i.student_id = tests.student_id_id
where ifnull(p.starting_grade, st_i.starting_grade) between 1 and 6
	and st_i.enrollment_date < @begin_sy + INTERVAL 3 MONTH
	and (ex.exit_date is null or ex.exit_date >= (@end_sy - INTERVAL 16 DAY))
	and tests.student_id_id is null

-- get number of promotions for each student (all school years)
select sc.school_name, s.student_id, i.name, st_i.enrollment_date, ifnull(ex.exit_date, "") as exit_date, s.test_sy, s.promotions, s.tests_taken, s.last_test_date,
	(select max(test_grade_math) from mande_academic where student_id_id = s.student_id and test_date = s.last_test_date) as last_math,
	(select max(test_grade_khmer) from mande_academic where student_id_id = s.student_id and test_date = s.last_test_date) as last_khmer,
	ifnull(p.current_grade, st_i.starting_grade) as current_grade
from (select student_id_id AS student_id,
		if (month(test_date)>=8,concat(year(test_date),'-',(year(test_date)+1)%100),concat(year(test_date)-1,'-',(year(test_date))%100)) as test_sy,
		SUM( promote ) AS promotions,
		COUNT( id ) AS tests_taken,
		MAX(test_grade_math) as max_math,
		MAX(test_grade_khmer) as max_khmer,
		MAX(test_date) as last_test_date
	from mande_academic
	where test_date >= '2014-08-01' and
	  test_grade_math is not null and
	  test_grade_khmer is not null
	GROUP BY student_id_id, test_sy) s
join mande_intakesurvey i on s.student_id = i.student_id
join mande_school sc on i.site_id = sc.school_id
join mande_intakeinternal st_i on s.student_id = st_i.student_id_id
left join (select student_id_id, max(exit_date) as exit_date from mande_exitsurvey ex group by student_id_id) ex on s.student_id = ex.student_id_id
left join (select student_id_id, max(test_level)+1 as current_grade from mande_academic where promote = 1 group by student_id_id) p on i.student_id = p.student_id_id
where st_i.enrollment_date < '2015-11-01'
	and (ex.exit_date is null or ex.exit_date >= '2016-07-15')
	and test_sy = '2015-16'
order by current_grade, s.promotions desc, sc.school_name

-- list of parent occupations
select sc.school_name 'site', st.guardian1_relationship, st.guardian1_profession, st.guardian2_relationship, st.guardian2_profession
from mande_intakesurvey st
  join mande_school sc on st.site_id = sc.school_id
  left join (select student_id_id, max(exit_date) as exit_date from mande_exitsurvey ex group by student_id_id) ex on st.student_id = ex.student_id_id
where ex.exit_date is null
  and sc.school_name = 'VDP-PKPN'

select * from mande_publicschoolhistory

-- list of student public school info
select sc.school_name 'site',
	case c.cohort when 50 then 'English' when 70 then 'VN' else c.cohort end as grade,
	c.classroom_number,
	st.student_id,
	st.name,
	st.enrolled,
	st.public_school_name,
	if(st.enrolled='Y', st.grade_current, 'NA') as grade_current,
	replace(st.reasons, '\r\n', ' ') as reasons,
	replace(st.notes, '\r\n', ' ') as notes,
	psh.*
from mande_intakesurvey st
  join mande_school sc on st.site_id = sc.school_id
  join mande_intakeinternal st_i on st.student_id = st_i.student_id_id
  left join (select st.student_id, max(exit_date) as exit_date from mande_intakesurvey st left join mande_exitsurvey ex on st.student_id = ex.student_id_id group by st.student_id) ex on st.student_id = ex.student_id
  join mande_classroomenrollment ce on ce.student_id_id = st.student_id
  join mande_classroom c on ce.classroom_id_id = c.classroom_id
  left join mande_publicschoolhistory psh on st.student_id = psh.student_id_id
where ex.exit_date is null
  and c.active=true
  and ce.drop_date is null
  --and sc.school_name = 'VDP-TK'
order by sc.school_name, grade, c.classroom_number, st.name, psh.academic_year

-- check for multiple public school history records
select student_id_id, count(*) from mande_publicschoolhistory group by student_id_id having count(*) > 1

-- list of student public school info (old)
select sc.school_name 'site',
	case c.cohort when 50 then 'English' when 70 then 'VN' else c.cohort end as grade,
	c.classroom_number,
	st.student_id,
	st.name,
	st.enrolled,
	st.public_school_name,
	if(st.enrolled='Y', st.grade_current, 'NA') as grade_current,
	replace(st.reasons, '\r\n', ' ') as reasons,
	replace(st.notes, '\r\n', ' ') as notes
from mande_intakesurvey st
  join mande_school sc on st.site_id = sc.school_id
  join mande_intakeinternal st_i on st.student_id = st_i.student_id_id
  left join (select st.student_id, max(exit_date) as exit_date from mande_intakesurvey st left join mande_exitsurvey ex on st.student_id = ex.student_id_id group by st.student_id) ex on st.student_id = ex.student_id
  join mande_classroomenrollment ce on ce.student_id_id = st.student_id
  join mande_classroom c on ce.classroom_id_id = c.classroom_id
where ex.exit_date is null
  and c.active=true
  and ce.drop_date is null
order by sc.school_name, grade, c.classroom_number, st.name


select * from mande_intakesurvey where student_id = 50980
select * from mande_intakeupdate where student_id_id = 50980
select * from mande_publicschoolhistory where student_id_id = 50980
select * from mande_spiritualactivitiessurvey where personal_attend_church <> 'Y' and personal_attend_church <> 'N'

-- get # of achievement tests and promotion rate
select count(*) as 'num_tests', sum(a.promote) as 'num_promoted', sum(a.promote) / count(*) as 'promote_rate'
from mande_academic a
where a.test_grade_math is not null
  and a.test_grade_khmer is not null
  and a.test_date between '2017-01-15' and '2017-02-05'

select test_date, if (month(test_date)>=8,concat(year(test_date),'-',(year(test_date)+1)%100),concat(year(test_date)-1,'-',(year(test_date))%100)) as test_sy
from mande_academic
where test_grade_math is not null and
  test_grade_khmer is not null
order by test_date

-- get test info for students who did not promote during SY 2015-16
select sc.school_name 'site', i.name, i.student_id, a.test_date,
	a.test_level,
	a.test_grade_math,
	a.test_grade_khmer,
	st_i.enrollment_date as enroll_date,
	ifnull(ex.exit_date, "") as exit_date
from (select student_id_id AS student_id, SUM( promote ) AS promotions
	from mande_academic
	where test_date between '2015-08-01' and '2016-07-31' and
	  test_grade_math is not null and
	  test_grade_khmer is not null
	GROUP BY student_id_id
	HAVING SUM(promote) = 0) s
join mande_academic a on s.student_id = a.student_id_id
join mande_intakesurvey i on s.student_id = i.student_id
join mande_school sc on i.site_id = sc.school_id
join mande_intakeinternal st_i on s.student_id = st_i.student_id_id
left join (select student_id_id, max(exit_date) as exit_date from mande_exitsurvey ex group by student_id_id) ex on s.student_id = ex.student_id_id
where test_date between '2015-08-01' and '2016-07-31'
  and st_i.enrollment_date < '2015-11-01'
  and (ex.exit_date is null or ex.exit_date > '2016-07-01')
order by sc.school_name, i.student_id, a.test_date



select * from mande_intakeinternal where student_id_id = 50406

-- check students with multiple exit surveys
select st.student_id, st.date, st.enrolled, ex.exit_date
from (select st.student_id from mande_intakesurvey st left join mande_exitsurvey ex on st.student_id = ex.student_id_id group by st.student_id having count(ex.id) > 1) mult_exits
  join mande_intakesurvey st on mult_exits.student_id = st.student_id
  join mande_exitsurvey ex on st.student_id = ex.student_id_id
order by st.student_id, ex.exit_date

-- check how many students have multiple exit surveys
select st.student_id, count(ex.id) as num_exits
from mande_intakesurvey st
  left join mande_exitsurvey ex on st.student_id = ex.student_id_id
group by st.student_id
order by num_exits desc

select sc.school_name, a.*
from mande_academic a
  join mande_intakesurvey st on a.student_id_id = st.student_id
  join mande_school sc on st.site_id = sc.school_id
where sc.school_name = 'VDP-PKPN'

select sc.school_name, a.test_level, a.promote, count(*)
from mande_academic a
  join mande_intakesurvey st on a.student_id_id = st.student_id
  join mande_school sc on st.site_id = sc.school_id
group by sc.school_name, a.test_level, a.promote
order by sc.school_name, a.test_level, a.promote

select a.student_id_id, count(*) as num_promotes
from (select distinct student_id_id from mande_academic) s
  left join mande_academic a on a.student_id_id = s.student_id_id and a.promote = 1
group by a.student_id_id
order by count(*) desc, a.student_id_id

select * from mande_academic where student_id_id = 50019

-- get distinct count of students taking an assessment, by site
select sc.school_name, count(distinct student_id_id), count(student_id_id)
from mande_academic a
join mande_intakesurvey i on a.student_id_id = i.student_id
join mande_school sc on i.site_id = sc.school_id
group by sc.school_name

select promotions, count(*)
from (
SELECT student_id_id AS student_id, SUM( promote ) AS promotions, COUNT( id ) AS tests_taken
FROM `mande_academic`
WHERE student_id_id
IN (
	SELECT student_id
	FROM mande_intakesurvey
	WHERE site_id =3
)
GROUP BY student_id_id) p
group by promotions

select count(*), count(distinct student_id_id) from mande_academic where test_date between '2015-01-01' and '2016-01-01'

-- get number of promotions for each student (old)
select sc.school_name, s.student_id_id, i.gender, i.dob, i.grade_current, ifnull(sp.num_promotes,0) as num_promotes_2014_15
from (select distinct student_id_id from mande_academic) s
join mande_intakesurvey i on s.student_id_id = i.student_id
join mande_school sc on i.site_id = sc.school_id
left join
(
	select student_id_id, count(*) as num_promotes
	from mande_academic
	where promote = 1
	group by student_id_id
) as sp on s.student_id_id = sp.student_id_id
order by 2 desc, 1
