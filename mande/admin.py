from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.admin.models import LogEntry

from django import forms

# Register your models here.
from mande.models import School
from mande.models import Classroom
from mande.models import Teacher

from mande.models import IntakeSurvey
from mande.models import IntakeInternal
from mande.models import IntakeUpdate
from mande.models import ExitSurvey
from mande.models import PostExitSurvey
from mande.models import ExitSurveyLog
from mande.models import PostExitSurveyLog


from mande.models import StudentEvaluation
from mande.models import SpiritualActivitiesSurvey

from mande.models import AttendanceDayOffering
from mande.models import Attendance

from mande.models import Discipline
from mande.models import Academic
from mande.models import EnglishTest
from mande.models import Health
from mande.models import Diseases, Vaccines

from mande.models import ClassroomEnrollment
from mande.models import ClassroomTeacher

from mande.models import PublicSchoolHistory
from mande.models import AcademicMarkingPeriod
from mande.models import CurrentStudentInfo
from mande.models import EvaluationMarkingPeriod
from mande.models import NotificationLog
from mande.models import CredentialsModel
from mande.models import StudentNote

class LogEntryAdmin(admin.ModelAdmin):
    list_display = ('action_time','user','message','content_type')
    list_filter = ['action_time','user','content_type']
    search_fields = ['change_message']

    list_display_links = None
    ordering = ('-action_time',)

    readonly_fields = ('content_type',
        'user',
        'action_time',
        'object_id',
        'object_repr',
        'action_flag',
        'change_message'
    )

    def message(self,obj):
        text = obj.__str__()
        url = obj.get_admin_url()

        return '<a href="'+url+'">'+text.decode('utf-8')+'</a>'
    message.allow_tags = True

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj=None):
        return False

    def get_actions(self, request):
        actions = super(LogEntryAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

class UserCreateForm(UserCreationForm):
    password1 = forms.CharField(label="Password", required=False,
                        widget=forms.PasswordInput)
    password2 = forms.CharField(label="Password confirmation",
                            widget=forms.PasswordInput, required=False)

    class Meta:
        model = User
        exclude = []

class UserAdmin(UserAdmin):
    list_display = ('username' ,'first_name', 'last_name', 'is_active', 'is_staff')
    add_form = UserCreateForm
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', ),
        }),
    )

# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(LogEntry,LogEntryAdmin)

class SchoolAdmin(admin.ModelAdmin):
        list_display = ('school_name','school_location','active')
        list_filter = ('active',)

class ClassroomAdmin(admin.ModelAdmin):
        list_display = ('school_id','classroom_id', 'cohort','classroom_number','attendance_calendar','active')
        list_filter = ('active','school_id','cohort')
        search_fields = ['cohort']

class TeacherAdmin(admin.ModelAdmin):
        list_display = ('teacher_id','name','active')
        list_filter = ('active',)

class IntakeSurveyAdmin(admin.ModelAdmin):
    fieldsets = [
        ('',
        {'fields': ['date','site']}),
        ('Student Biographical Information',
        {'fields': ['name','dob','pob','grade_appropriate','gender','address']}),
        ('Information about Guadian 1',
        {'fields': ['guardian1_relationship','guardian1_name','guardian1_phone','guardian1_profession','guardian1_employment']}),
        ('Information about Guardian 2',
        {'fields': ['guardian2_relationship','guardian2_name','guardian2_phone','guardian2_profession','guardian2_employment']}),
        ('Household Information',
        {'fields': ['minors']}),
        ('Notes',
        {'fields': ['notes']}),

    ]
    list_display = ('site','date','student_id','name','grade_appropriate')
    list_filter = ('date','site','grade_appropriate')
    search_fields = ['name','student_id']

class IntakeInternalAdmin(admin.ModelAdmin):
    list_display = ('student_id','enrollment_date','starting_grade')
    list_filter = ('starting_grade','enrollment_date')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class IntakeUpdateAdmin(admin.ModelAdmin):
    list_display = ('student_id','date','current_grade')
    list_filter = ('date',)
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class ExitSurveyAdmin(admin.ModelAdmin):
    list_display = ('site','student_id','exit_date','last_grade','early_exit_reason','early_exit_comment')
    def site(self, obj):
        return obj.student_id.site
    list_filter = ('exit_date','last_grade','early_exit_reason')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class PostExitSurveyAdmin(admin.ModelAdmin):
    list_display = ('student_id','exit_date','enrolled')
    list_filter = ('exit_date','enrolled')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class StudentEvaluationAdmin(admin.ModelAdmin):
    list_display = ('student_id', 'date', 'academic_score', 'study_score', 'personal_score', 'hygiene_score', 'faith_score')
    list_filter = ('date',)
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class SpiritualActivitiesSurveyAdmin(admin.ModelAdmin):
    list_display = ('site','student_id','date','personal_attend_church','frequency_of_attending','church_name')
    list_filter = ('date','personal_attend_church','frequency_of_attending','student_id__site')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

    def site(self, obj):
        return obj.student_id.site
    site.admin_order_field = 'student_id__site'

class AttendanceDayOfferingAdmin(admin.ModelAdmin):
    list_display = ('classroom_id', 'date', 'offered')
    list_filter = ('classroom_id', 'date', 'offered')

class AttendanceAdmin(admin.ModelAdmin):
    list_display = ('student_id','site','classroom','date','attendance')
    list_filter = ('date','attendance','student_id__site','classroom')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']
    def site(self, obj):
        return obj.student_id.site

class DisciplineAdmin(admin.ModelAdmin):
    list_display = ('incident_date','student_id','classroom_id','incident_code')
    list_filter = ('incident_date','incident_code')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class AcademicAdmin(admin.ModelAdmin):
    list_display = ('test_date','student_id','test_level','test_grade_khmer','test_grade_math','activity_test','quiz','monthly_test','middle_test','final_test','promote','notes')
    list_filter = ('test_level','promote','test_date')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class EnglishTestAdmin(admin.ModelAdmin):
    list_display = ('test_date','student_id','test_level','promote','activity_test','quiz','monthly_test','middle_test','final_test','notes')
    list_filter = ('test_level','promote','test_date')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class HealthAdmin(admin.ModelAdmin):
    list_display = ('appointment_date','appointment_type','student_id', 'site')
    def site(self, obj):
        return obj.student_id.site
    list_filter = ('appointment_date','appointment_type')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class ClassroomEnrollmentAdmin(admin.ModelAdmin):
    list_display = ('classroom_id','student_id','enrollment_date','drop_date')
    list_filter = ('classroom_id','enrollment_date','drop_date')
    raw_id_fields = ('student_id',)
    search_fields = ['student_id__student_id']

class ClassroomTeacherAdmin(admin.ModelAdmin):
    list_display = ('classroom_id','teacher_id','year')
    list_filter = ('year','classroom_id','teacher_id')

class PublicSchoolHistoryAdmin(admin.ModelAdmin):
    list_display = ('student_id','status','enroll_date','drop_date','grade','school_name')
    list_filter = ('status','grade')
    search_fields = ['student_id__student_id']

class AcademicMarkingPeriodAdmin(admin.ModelAdmin):
    list_display = ('test_date','marking_period_start','marking_period_end','description',)
    list_filter = ('test_date',)
    search_fields = ['description']

class CurrentStudentInfoAdmin(admin.ModelAdmin):
    list_display = ('student_id','name','site','gender','date','vdp_grade','refresh')
    list_filter = ('site','refresh')
    search_fields = ['name','student_id']

class EvaluationMarkingPeriodAdmin(admin.ModelAdmin):
    list_display = ('test_date','marking_period_start','marking_period_end','description',)
    list_filter = ('test_date',)
    search_fields = ['description']

class NotificationLogAdmin(admin.ModelAdmin):
    list_display = ('user','date','text',)
    list_filter = ('date','user',)
    search_fields = ['text']

class CredentialsAdmin(admin.ModelAdmin):
    list_display = ('user','credential')

class StudentNoteAdmin(admin.ModelAdmin):
    list_display = ('student_id','date','notes')
    list_filter = ('date','student_id',)

class ExitSurveyLogAdmin(admin.ModelAdmin):
    list_display = ('student_id','date','user','survey_date','exit_date','early_exit_reason')
    list_filter = ('date',)
    search_fields = ['student_id__student_id']


class PostExitSurveyLogAdmin(admin.ModelAdmin):
    list_display = ('student_id','date','user','post_exit_survey_date','exit_date')
    list_filter = ('date',)
    search_fields = ['student_id__student_id']


admin.site.register(ExitSurveyLog,ExitSurveyLogAdmin)
admin.site.register(PostExitSurveyLog,PostExitSurveyLogAdmin)
admin.site.register(CredentialsModel,CredentialsAdmin)
admin.site.register(School,SchoolAdmin)
admin.site.register(Classroom,ClassroomAdmin)
admin.site.register(Teacher,TeacherAdmin)

admin.site.register(IntakeSurvey,IntakeSurveyAdmin)
admin.site.register(IntakeInternal,IntakeInternalAdmin)
admin.site.register(IntakeUpdate,IntakeUpdateAdmin)
admin.site.register(ExitSurvey,ExitSurveyAdmin)
admin.site.register(PostExitSurvey,PostExitSurveyAdmin)

admin.site.register(StudentEvaluation,StudentEvaluationAdmin)
admin.site.register(SpiritualActivitiesSurvey,SpiritualActivitiesSurveyAdmin)

admin.site.register(AttendanceDayOffering,AttendanceDayOfferingAdmin)
admin.site.register(Attendance,AttendanceAdmin)

admin.site.register(Discipline,DisciplineAdmin)
admin.site.register(Academic,AcademicAdmin)
admin.site.register(Health,HealthAdmin)
admin.site.register(Vaccines)
admin.site.register(Diseases)

admin.site.register(ClassroomEnrollment,ClassroomEnrollmentAdmin)
admin.site.register(ClassroomTeacher,ClassroomTeacherAdmin)

admin.site.register(PublicSchoolHistory,PublicSchoolHistoryAdmin)
admin.site.register(AcademicMarkingPeriod,AcademicMarkingPeriodAdmin)
admin.site.register(CurrentStudentInfo,CurrentStudentInfoAdmin)
admin.site.register(EvaluationMarkingPeriod,EvaluationMarkingPeriodAdmin)
admin.site.register(NotificationLog,NotificationLogAdmin)
admin.site.register(StudentNote,StudentNoteAdmin)
admin.site.register(EnglishTest,EnglishTestAdmin)
