from django.contrib.auth.models import Group, Permission

#set minimum permissions for access of each view (maybe more required for write access!)
perms_required = {
'classroom_form': 'mande.view_classroom_form',
'academic_select': 'mande.view_academic_select',
'englishtest_select': 'mande.view_englishtest_select',
'student_lag_report': 'mande.view_student_lag_report',
'health_form': 'mande.view_health_form',
'studentevaluation_form_single': 'mande.view_studentevaluation_form_single',
'intake_internal': 'mande.view_intake_internal',
'discipline_form': 'mande.view_discipline_form',
'classroomenrollment_form': 'mande.view_classroomenrollment_form',
'take_class_attendance': 'mande.view_take_class_attendance',
'attendance': 'mande.view_attendance',
'student_dental_report': 'mande.view_student_dental_report',
'intake_update': 'mande.view_intake_update',
'notification_log': 'mande.view_notification_log',
'student_detail': 'mande.view_student_detail',
'intake_survey': 'mande.view_intake_survey',
'take_attendance': 'mande.view_daily_attendance_report',#take_attendance is just a link the the daily_attendance report
'classroomteacher_form': 'mande.view_classroomteacher_form',
'class_list': 'mande.view_class_list',
'students_promoted_times_report': 'mande.view_students_promoted_times_report',
'post_exit_survey_list': 'mande.view_post_exit_survey_list',
'daily_absence_report': 'mande.view_daily_absence_report',
'mande_summary_report': 'mande.view_mande_summary_report',
'exit_survey': 'mande.view_exit_survey',
'student_absence_report': 'mande.view_student_absence_report',
'student_evaluation_report': 'mande.view_student_evaluation_report',
'student_achievement_test_report': 'mande.view_student_achievement_test_report',
'student_englishtest': 'mande.view_student_englishtest',
'academic_form_single': 'mande.view_academic_form_single',
'academic_form_single2': 'mande.view_academic_form_single2',
'englishtest_form_single': 'mande.view_englishtest_form_single',
'attendance_days': 'mande.view_attendance_days',
'classroomenrollment_individual': 'mande.view_classroomenrollment_individual',
'anomalous_data': 'mande.view_anomalous_data',
'students_lag_summary': 'mande.view_students_lag_summary',
'data_audit': 'mande.view_data_audit',
'studentevaluation_select': 'mande.view_studentevaluation_select',
'exit_surveys_list': 'mande.view_exit_surveys_list',
'daily_attendance_report': 'mande.view_daily_attendance_report',
'spiritualactivities_survey': 'mande.view_spiritualactivities_survey',
'delete_spiritualactivities_survey': 'mande.view_delete_spiritualactivities_survey',

'student_medical_report': 'mande.view_student_medical_report',
'attendance_calendar': 'mande.view_attendance_calendar',
'teacher_form': 'mande.view_teacher_form',
'student_promoted_report': 'mande.view_student_promoted_report',
'students_intergrated_in_public_school': 'mande.view_students_intergrated_in_public_school',
'public_school_report': 'mande.view_public_school_report',
'post_exit_survey': 'mande.view_post_exit_survey',
'academic_form': 'mande.view_academic_form',
'englishtests_form': 'mande.view_englishtests_form',
'dashboard': 'mande.view_dashboard',
'student_list': 'mande.view_student_list',
'studentevaluation_form': 'mande.view_studentevaluation_form',
'student_dental_summary_report': 'mande.view_student_dental_summary_report',
'student_attendance_detail': 'mande.view_student_attendance_detail',
'attendance_summary_report': 'mande.view_attendance_summary_report',
'advanced_report': 'mande.view_advanced_report',
'unapproved_absence_with_no_comment': 'mande.view_unapproved_absence_with_no_comment',
'absence_with_special_comment': 'mande.view_absence_with_special_comment',
'publicschool_form': 'mande.view_publicschool_form',
'delete_public_school': 'mande.view_delete_public_school',
'save_photo': 'mande.view_save_photo',
'academic_making_period': 'mande.view_academic_making_period',
'generate': 'mande.view_generate',
'evaluation_making_period': 'mande.view_evaluation_making_period',
'update_current_grade': 'mande.view_update_current_grade',
'spiritual_survey_report': 'mande.view_spiritual_survey_report',
'bug_report': 'mande.view_bug_report',
'achievement_test': 'mande.view_achievement_test',
'index': 'mande.view_dashboard',
'student_search': 'mande.view_student_search',
'student_note': 'mande.view_student_note',
'reenroll_student': 'mande.view_reenroll_student',
'disease_form': 'mande.view_disease_form',
'vaccine_form': 'mande.view_vaccine_form',
'delete_vaccine_survey': 'mande.view_delete_vaccine_survey',
'student_vaccine_report': 'mande.view_student_vaccine_report',
'student_disease_report': 'mande.view_student_disease_report',
'student_enrollment_report': 'mande.view_student_enrollment_report',
'catchup_academic_history_report': 'mande.view_catchup_academic_history_report',
'english_academic_history_report': 'mande.view_english_academic_history_report',
'followup_vdp_ps_studentgrade_report': 'mande.view_followup_vdp_ps_studentgrade_report',
'vdp_catchup_right_level_summary': 'mande.view_vdp_catchup_right_level_summary',
}

# permissions for super user only
admin_permissions_only = ['reenroll_student']
#this is a function to help test that the permissions above are
#correctly implemented in the views
# update to database
def generate_group_perms():
    #set up roles
    group_perms = {
        'Super Administrators':[],
        'Administrator / Site Coordinators':[],
        'Community Workers':[],
        'Teachers':[],
        'Health Worker':[]
    }
    #admins/site coordinators have all permissions
    for function,perm in perms_required.iteritems():
        if function not in admin_permissions_only:
            group_perms['Super Administrators'].append(perm)
            group_perms['Administrator / Site Coordinators'].append(perm)

    #hws is a made up role - just a sanity check with a single permission.
    group_perms['Health Worker'] = __addperms(perms_required['health_form'],group_perms['Health Worker'])
    group_perms['Health Worker'] = __addperms(perms_required['index'],group_perms['Health Worker'])
    group_perms['Health Worker'] = __addperms(perms_required['bug_report'],group_perms['Health Worker'])
    group_perms['Health Worker'] = __addperms(perms_required['student_search'],group_perms['Health Worker'])
    group_perms['Health Worker'] = __addperms(perms_required['disease_form'],group_perms['Health Worker'])
    group_perms['Health Worker'] = __addperms(perms_required['vaccine_form'],group_perms['Health Worker'])
    group_perms['Health Worker'] = __addperms(perms_required['delete_vaccine_survey'],group_perms['Health Worker'])
    group_perms['Health Worker'] = __addperms(perms_required['student_disease_report'],group_perms['Health Worker'])
    group_perms['Health Worker'] = __addperms(perms_required['student_vaccine_report'],group_perms['Health Worker'])
    group_perms['Health Worker'] = __addperms(perms_required['student_enrollment_report'],group_perms['Health Worker'])    
    group_perms['Health Worker'] = __addperms(perms_required['catchup_academic_history_report'],group_perms['Health Worker'])    
    group_perms['Health Worker'] = __addperms(perms_required['english_academic_history_report'],group_perms['Health Worker'])    
    group_perms['Health Worker'] = __addperms(perms_required['academic_form_single2'],group_perms['Health Worker'])   

    #community workers can:
    #submit a bug
    group_perms['Community Workers'] = __addperms(perms_required['bug_report'],group_perms['Community Workers'])
    #view dashboard
    group_perms['Community Workers'] = __addperms(perms_required['index'],group_perms['Community Workers'])
	#add student public school history
    group_perms['Community Workers'] = __addperms(perms_required['publicschool_form'],group_perms['Community Workers'])
	#view students unapproved absence with no comment
    group_perms['Community Workers'] = __addperms(perms_required['unapproved_absence_with_no_comment'],group_perms['Community Workers'])
    #view students absence with special comment
    group_perms['Community Workers'] = __addperms(perms_required['absence_with_special_comment'],group_perms['Community Workers'])
    #view attendance detail
    group_perms['Community Workers'] = __addperms(perms_required['student_attendance_detail'],group_perms['Community Workers'])
	#view attendance summary
    group_perms['Community Workers'] = __addperms(perms_required['attendance_summary_report'],group_perms['Community Workers'])
    #view classroom Attendance
    group_perms['Community Workers'] = __addperms(perms_required['daily_attendance_report'],group_perms['Community Workers'])
    #view class list
    group_perms['Community Workers'] = __addperms(perms_required['class_list'],group_perms['Community Workers'])
    #view exit surveys list
    group_perms['Community Workers'] = __addperms(perms_required['exit_surveys_list'],group_perms['Community Workers'])
    #view student medical report
    group_perms['Community Workers'] = __addperms(perms_required['student_medical_report'],group_perms['Community Workers'])
    #view student dental report
    group_perms['Community Workers'] = __addperms(perms_required['student_dental_report'],group_perms['Community Workers'])
    #view student dental summary report
    group_perms['Community Workers'] = __addperms(perms_required['student_dental_summary_report'],group_perms['Community Workers'])
    #view students public school report
    group_perms['Community Workers'] = __addperms(perms_required['public_school_report'],group_perms['Community Workers'])
    #view students integrated in publich school report
    group_perms['Community Workers'] = __addperms(perms_required['students_intergrated_in_public_school'],group_perms['Community Workers'])
    #take Attendance
    group_perms['Community Workers'] = __addperms(perms_required['take_attendance'],group_perms['Community Workers'])
    group_perms['Community Workers'] = __addperms(perms_required['take_class_attendance'],group_perms['Community Workers'])
    group_perms['Community Workers'] = __addperms(perms_required['attendance'],group_perms['Community Workers'])
    #add intake Surveys
    group_perms['Community Workers'] = __addperms(perms_required['intake_survey'],group_perms['Community Workers'])
    #add exit Surveys
    group_perms['Community Workers'] = __addperms(perms_required['exit_survey'],group_perms['Community Workers'])
    #add post-exit Surveys
    group_perms['Community Workers'] = __addperms(perms_required['post_exit_survey'],group_perms['Community Workers'])
    #add spiritual activities
    group_perms['Community Workers'] = __addperms(perms_required['spiritualactivities_survey'],group_perms['Community Workers'])
	#delete spiritual activities
    group_perms['Community Workers'] = __addperms(perms_required['delete_spiritualactivities_survey'],group_perms['Community Workers'])
    #add/edit health
    group_perms['Community Workers'] = __addperms(perms_required['health_form'],group_perms['Community Workers'])    
    #add/edit disease/vaccine
    group_perms['Community Workers'] = __addperms(perms_required['disease_form'],group_perms['Community Workers'])
    group_perms['Community Workers'] = __addperms(perms_required['vaccine_form'],group_perms['Community Workers'])
    group_perms['Community Workers'] = __addperms(perms_required['delete_vaccine_survey'],group_perms['Community Workers'])
    #view disease/vaccine report
    group_perms['Community Workers'] = __addperms(perms_required['student_disease_report'],group_perms['Community Workers'])
    group_perms['Community Workers'] = __addperms(perms_required['student_vaccine_report'],group_perms['Community Workers'])
    #view student Information
    group_perms['Community Workers'] = __addperms(perms_required['student_list'],group_perms['Community Workers'])
    #view Enrollment
    group_perms['Community Workers'] = __addperms(perms_required['classroomenrollment_form'],group_perms['Community Workers'])
    #view discipline
    group_perms['Community Workers'] = __addperms(perms_required['discipline_form'],group_perms['Community Workers'])
    #add/change student details
    group_perms['Community Workers'] = __addperms(perms_required['student_detail'],group_perms['Community Workers'])
	#view Advanced report
    group_perms['Community Workers'] = __addperms(perms_required['advanced_report'],group_perms['Community Workers'])
	#delete public school
    group_perms['Community Workers'] = __addperms(perms_required['delete_public_school'],group_perms['Community Workers'])
	#save photo
    group_perms['Community Workers'] = __addperms(perms_required['save_photo'],group_perms['Community Workers'])
    group_perms['Community Workers'] = __addperms(perms_required['student_search'],group_perms['Community Workers'])
    group_perms['Community Workers'] = __addperms(perms_required['student_note'],group_perms['Community Workers'])
    #view student enrollment report
    group_perms['Community Workers'] = __addperms(perms_required['student_enrollment_report'],group_perms['Community Workers'])
    #view student accamdic report
    group_perms['Community Workers'] = __addperms(perms_required['catchup_academic_history_report'],group_perms['Community Workers'])
    group_perms['Community Workers'] = __addperms(perms_required['english_academic_history_report'],group_perms['Community Workers'])
    group_perms['Community Workers'] = __addperms(perms_required['academic_form_single2'],group_perms['Community Workers'])


    #Teacherss can:
    #view dashboard
    group_perms['Teachers'] = __addperms(perms_required['index'],group_perms['Teachers'])
    #submit a bug
    group_perms['Teachers'] = __addperms(perms_required['bug_report'],group_perms['Teachers'])
	#add student public school history
    group_perms['Teachers'] = __addperms(perms_required['publicschool_form'],group_perms['Teachers'])
	#view student unapproved absence with no comment
    group_perms['Teachers'] = __addperms(perms_required['unapproved_absence_with_no_comment'],group_perms['Teachers'])
    #view student absence with special comment
    group_perms['Teachers'] = __addperms(perms_required['absence_with_special_comment'],group_perms['Teachers'])
    #view attendance detail
    group_perms['Teachers'] = __addperms(perms_required['student_attendance_detail'],group_perms['Teachers'])
	#view attendance summary
    group_perms['Teachers'] = __addperms(perms_required['attendance_summary_report'],group_perms['Teachers'])
    #view dailiy absences
    group_perms['Teachers'] = __addperms(perms_required['daily_absence_report'],group_perms['Teachers'])
    #view class list
    group_perms['Teachers'] = __addperms(perms_required['class_list'],group_perms['Teachers'])
    #view student lag reports
    group_perms['Teachers'] = __addperms(perms_required['student_lag_report'],group_perms['Teachers'])
    #view student evaluation reports
    group_perms['Teachers'] = __addperms(perms_required['student_evaluation_report'],group_perms['Teachers'])
	#view student achievement test reports
    group_perms['Teachers'] = __addperms(perms_required['student_achievement_test_report'],group_perms['Teachers'])
    #view students promoted reports
    group_perms['Teachers'] = __addperms(perms_required['student_promoted_report'],group_perms['Teachers'])
    #view students promoted times report
    group_perms['Teachers'] = __addperms(perms_required['students_promoted_times_report'],group_perms['Teachers'])
    #take attendance
    group_perms['Teachers'] = __addperms(perms_required['take_attendance'],group_perms['Teachers'])
    #view attendance calendars
    group_perms['Teachers'] = __addperms(perms_required['attendance_calendar'],group_perms['Teachers'])
    #view student Information
    group_perms['Teachers'] = __addperms(perms_required['student_list'],group_perms['Teachers'])
    #view student detail
    group_perms['Teachers'] = __addperms(perms_required['student_detail'],group_perms['Teachers'])
    #add intakeupdate
    group_perms['Teachers'] = __addperms(perms_required['intake_update'],group_perms['Teachers'])
    #view Enrollment
    group_perms['Teachers'] = __addperms(perms_required['classroomenrollment_form'],group_perms['Teachers'])
    #add discipline
    group_perms['Teachers'] = __addperms(perms_required['discipline_form'],group_perms['Teachers'])
    #add achievement tests
    group_perms['Teachers'] = __addperms(perms_required['academic_select'],group_perms['Teachers'])
    #add english tests
    group_perms['Teachers'] = __addperms(perms_required['englishtest_select'],group_perms['Teachers'])
    #add student evaluations
    group_perms['Teachers'] = __addperms(perms_required['studentevaluation_select'],group_perms['Teachers'])
    group_perms['Teachers'] = __addperms(perms_required['studentevaluation_form'],group_perms['Teachers'])
    group_perms['Teachers'] = __addperms(perms_required['studentevaluation_form_single'],group_perms['Teachers'])
    #take attendance
    group_perms['Teachers'] = __addperms(perms_required['take_class_attendance'],group_perms['Teachers'])
    group_perms['Teachers'] = __addperms(perms_required['attendance'],group_perms['Teachers'])
	#view Advanced report
    group_perms['Teachers'] = __addperms(perms_required['advanced_report'],group_perms['Teachers'])
	#delete public school
    group_perms['Teachers'] = __addperms(perms_required['delete_public_school'],group_perms['Teachers'])
	#save photo
    group_perms['Teachers'] = __addperms(perms_required['save_photo'],group_perms['Teachers'])
	# achievement test report
    group_perms['Teachers'] = __addperms(perms_required['achievement_test'],group_perms['Teachers'])
    group_perms['Teachers'] = __addperms(perms_required['student_search'],group_perms['Teachers'])
    group_perms['Teachers'] = __addperms(perms_required['student_note'],group_perms['Teachers'])
    #student enrollment report
    group_perms['Teachers'] = __addperms(perms_required['student_enrollment_report'],group_perms['Teachers'])    
    #student academic report
    group_perms['Teachers'] = __addperms(perms_required['catchup_academic_history_report'],group_perms['Teachers'])
    group_perms['Teachers'] = __addperms(perms_required['english_academic_history_report'],group_perms['Teachers'])
    group_perms['Teachers'] = __addperms(perms_required['academic_form_single2'],group_perms['Teachers'])   
    #view students catchup public school report
    group_perms['Teachers'] = __addperms(perms_required['followup_vdp_ps_studentgrade_report'],group_perms['Teachers'])
    #view vdp_catchup_right_level_summary
    group_perms['Teachers'] = __addperms(perms_required['vdp_catchup_right_level_summary'],group_perms['Teachers'])
    return group_perms

#helper function for above that makes sure the items in the list are unique
def __addperms(perms,addto):
    addto.append(perms)
    return list(set(addto))

# update group permisson to db
def update_group_perms():
	group_perms = generate_group_perms()
        for group,perms in group_perms.iteritems():
            newgroup,created = Group.objects.get_or_create(name=group)
            # clear all permissions in group
            newgroup.permissions.clear()
            for perm in perms:
                try:
                    permission = Permission.objects.get(codename=perm[6:],content_type__model='jethroperms')
                    newgroup.permissions.add(permission)
                except Exception as e:
                    print perm[6:] + ': '+ str(e)
	print '***finished update group permissions***'
