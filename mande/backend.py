from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User
from mande.models import Profile


class GoogleAuthBackend(ModelBackend):
    def authenticate(self, identifier=None, attributes=None):

        email = attributes.get('email', None)
        try:
            user = User.objects.get(username=email)
            user.first_name = attributes.get('first_name') or ''
            user.last_name = attributes.get('last_name') or ''
            user.save()

            profile,created = Profile.objects.get_or_create(user=user)
            profile.profile_picture = attributes.get('profile') or ''
            profile.save()
            
        except User.DoesNotExist:
            return None
        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            pass
