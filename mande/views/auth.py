import requests
import os

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib import messages
from django.contrib import auth
from django.contrib.auth.views import logout as django_logout
from django.conf import settings

from oauth2client.contrib.django_util.storage import DjangoORMStorage
import google.oauth2.credentials
import google_auth_oauthlib.flow

from mande.models import CredentialsModel


SCOPES = settings.GOOGLE_OAUTH2_SCOPES
GOOGLE_OAUTH2_CLIENT_SECRETS_JSON = settings.GOOGLE_OAUTH2_CLIENT_SECRETS_JSON


def authorize(request):
      flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        GOOGLE_OAUTH2_CLIENT_SECRETS_JSON, scopes=SCOPES)
      # 'http://localhost:800/oauth2callback'
      flow.redirect_uri = request.build_absolute_uri(reverse('oauth2callback'))

      authorization_url, state = flow.authorization_url()

      # Store the state so the callback can verify the auth server response.
      request.session['state'] = state
      return HttpResponseRedirect(authorization_url)

def oauth2callback(request):
    # Specify the state when creating the flow in the callback so that it can
    # verified in the authorization server response.
    state = request.session['state']

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        GOOGLE_OAUTH2_CLIENT_SECRETS_JSON, scopes=SCOPES, state=state)
    flow.redirect_uri = request.build_absolute_uri(reverse('oauth2callback'))

    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = request.build_absolute_uri().replace('http:', 'https:')
    flow.fetch_token(authorization_response=authorization_response)

    # Store credentials in the session..
    credentials = flow.credentials
    request.session['access_token'] = credentials.token

    # get user profile after fetch token
    session = flow.authorized_session()
    profile_info = session.get('https://www.googleapis.com/userinfo/v2/me').json()

    attributes = {
      'email':profile_info['email'],
      'last_name':profile_info['family_name'],
      'first_name':profile_info['given_name'],
      'profile':profile_info['picture'],
      'access_token':credentials.token,
    }
    user = auth.authenticate(attributes=attributes)

    # if user not found in database, redirect to login page
    if not user:
        messages.error(request, 'Account not exist. Please contact system administrators.', extra_tags='login_error')
        return HttpResponseRedirect(reverse('login'))

    # login and redirect to home page
    auth.login(request, user)
    # store credentials to CredentialsModel, and keyed by User
    storage = DjangoORMStorage(CredentialsModel, 'user', request.user, 'credential')
    storage.put(credentials)
    return HttpResponseRedirect(reverse('index'))

def logout(request):
    # get credentials token from CredentialsModel for logout
    storage = DjangoORMStorage(CredentialsModel, 'user', request.user, 'credential')
    credentials = storage.get()
    access_token = credentials.token
    requests.post('https://accounts.google.com/o/oauth2/revoke',
        params={'token': access_token},
        headers = {'content-type': 'application/x-www-form-urlencoded'})
    return django_logout(request)
