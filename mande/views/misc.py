from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext, loader
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import PermissionDenied
from django.forms.models import modelformset_factory
from django.db.models import Q

from django.utils.html import conditional_escape as esc
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse

from calendar import HTMLCalendar, monthrange

from datetime import date
from datetime import datetime
from datetime import timedelta
from collections import OrderedDict
import requests
import json

from django.conf import settings

from django.views.generic import ListView
from mande.models import IntakeSurvey
from mande.models import IntakeUpdate
from mande.models import Classroom
from mande.models import Teacher
from mande.models import ClassroomEnrollment
from mande.models import ClassroomTeacher
from mande.models import Attendance
from mande.models import ExitSurvey
from mande.models import PostExitSurvey
from mande.models import SpiritualActivitiesSurvey
from mande.models import AttendanceDayOffering
from mande.models import School
from mande.models import Academic
from mande.models import NotificationLog
from mande.models import Health
from mande.models import AttendanceLog
from mande.models import IntakeInternal
from mande.models import PublicSchoolHistory
from mande.models import CurrentStudentInfo

from mande.models import GRADES
from mande.models import CATCH_UP_GRADES
from mande.models import ENGLISH_GRADES
from mande.models import COMPUTER_COURSES
from mande.models import ATTENDANCE_CODES

from mande.forms import IntakeSurveyForm
from mande.forms import IntakeUpdateForm
from mande.forms import ExitSurveyForm
from mande.forms import PostExitSurveyForm
from mande.forms import SpiritualActivitiesSurveyForm
from mande.forms import DisciplineForm
from mande.forms import TeacherForm
from mande.forms import ClassroomForm
from mande.forms import ClassroomTeacherForm
from mande.forms import ClassroomEnrollmentForm
from mande.forms import IndividualClassroomEnrollmentForm
from mande.forms import AttendanceForm
from mande.forms import AcademicForm
from mande.forms import IntakeInternalForm
from mande.forms import HealthForm

from mande.utils import getEnrolledStudents
from mande.utils import getStudentGradebyID
from mande.utils import studentAtSchoolGradeLevel
from mande.utils import studentAtAgeAppropriateGradeLevel

from django.contrib.auth.models import User
from mande.utils import user_permissions

import inspect

'''
*****************************************************************************
Dashboard
 - summarize important student information
*****************************************************************************
'''
def dashboard(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      CATCH_UP = dict(CATCH_UP_GRADES)
      ENGLISH = dict(ENGLISH_GRADES)
      COMPUTER = dict(COMPUTER_COURSES)

      notifications = NotificationLog.objects.order_by('-date')[:10]

      ''' enrolled students are those who have:
          - completed an intake survey
          - have completed an internal intake
          AND
              - do not have an exit survey
              OR
              - have an exit survey with an exit date after today

      '''
      surveys = CurrentStudentInfo.objects.all()
      all_students = surveys.count()
      total_female = surveys.filter(gender='F').count()

      total_catchup = surveys.filter(vdp_grade__in=CATCH_UP).count()
      total_catchup_female = surveys.filter(vdp_grade__in=CATCH_UP,gender='F').count()

      total_skills = all_students - total_catchup

      total_english = surveys.filter(vdp_grade__in=ENGLISH).count()
      total_english_female = surveys.filter(vdp_grade__in=ENGLISH,gender='F').count()
      #add by hok           
      total_computer_skills = all_students - total_catchup - total_english
      
      total_computer = surveys.filter(vdp_grade__in=COMPUTER).count()
      total_computer_female = surveys.filter(vdp_grade__in=COMPUTER,gender='F').count()
      #set up for collecting school breakdowns
      schools = School.objects.filter(active=True)
      catchup_gender_breakdown = OrderedDict()
      english_gender_breakdown = OrderedDict()
      computer_gender_breakdown = OrderedDict()  # add by hok
      program_breakdown = OrderedDict()

      students_by_grade_by_site = OrderedDict()
      students_by_skill_by_site = OrderedDict()
      students_by_computer_by_site = OrderedDict()  # add by hok
      students_by_grade = OrderedDict()
      students_by_skill = OrderedDict()
      students_by_computer = OrderedDict() #add by hok

      students_at_gl_by_grade = OrderedDict()

      #zero things out for accurate counts
      for key,grade in OrderedDict(GRADES).iteritems():
          # print grade
        if key in CATCH_UP:
            students_by_grade_by_site[key] = {}
            students_by_grade[key] = surveys.filter(vdp_grade=key).count()
            students_at_gl_by_grade[key] = surveys.filter(at_grade_level=True,vdp_grade=key).count()

        if key in ENGLISH:
            students_by_skill_by_site[key] = {}
            students_by_skill[key] = surveys.filter(vdp_grade=key).count()
        
        if key in COMPUTER:
            students_by_computer_by_site[key] = {}
            students_by_computer[key] = surveys.filter(vdp_grade=key).count()

        for school in schools:
            name = school.school_name
            try:
                students_by_grade_by_site[key][unicode(name)] = surveys.filter(site=school.school_id,vdp_grade=key).count()
            except KeyError:
                pass

            try:
                students_by_skill_by_site[key][unicode(name)] = surveys.filter(site=school.school_id,vdp_grade=key).count()
            except KeyError:
                pass
            #add by hok
            try:
                students_by_computer_by_site[key][unicode(name)] = surveys.filter(site=school.school_id,vdp_grade=key).count()
            except KeyError:
                pass
            #end

      #get information for morris donut charts
      for school in schools:
        name = school.school_name
        school_id = school.school_id

        catchup_gender_breakdown[name] = {'F':0, 'M':0, 'Total':0}
        catchup_gender_breakdown[name]['F'] = surveys.filter(site=school_id,vdp_grade__in=CATCH_UP,gender='F').count()
        catchup_gender_breakdown[name]['M'] = surveys.filter(site=school_id,vdp_grade__in=CATCH_UP,gender='M').count()
        catchup_gender_breakdown[name]['Total'] = catchup_gender_breakdown[name]['F'] + catchup_gender_breakdown[name]['M']

        english_gender_breakdown[name] = {'F':0, 'M':0, 'Total':0}
        english_gender_breakdown[name]['F'] = surveys.filter(site=school_id,vdp_grade__in=ENGLISH,gender='F').count()
        english_gender_breakdown[name]['M'] = surveys.filter(site=school_id,vdp_grade__in=ENGLISH,gender='M').count()
        english_gender_breakdown[name]['Total'] = english_gender_breakdown[name]['F'] + english_gender_breakdown[name]['M']

        computer_gender_breakdown[name] = {'F':0, 'M':0, 'Total':0}
        computer_gender_breakdown[name]['F'] = surveys.filter(site=school_id,vdp_grade__in=COMPUTER,gender='F').count()
        computer_gender_breakdown[name]['M'] = surveys.filter(site=school_id,vdp_grade__in=COMPUTER,gender='M').count()
        computer_gender_breakdown[name]['Total'] = computer_gender_breakdown[name]['F'] + computer_gender_breakdown[name]['M']

        program_breakdown[name] = {'Grades': 0, 'Skills': 0, 'Total':0}
        program_breakdown[name]['Grades'] = surveys.filter(site=school_id,vdp_grade__in=CATCH_UP).count()
        program_breakdown[name]['Skills'] = surveys.filter(site=school_id,vdp_grade__in=ENGLISH).count()
        program_breakdown[name]['Total'] = program_breakdown[name]['Grades'] + program_breakdown[name]['Skills']

      #clean up students_by_grade_by_site so we're not displaying a bunch of blank data
      clean_students_by_grade_by_site = students_by_grade_by_site
      for key,grade in students_by_grade_by_site.iteritems():
        if students_by_grade[key] == 0:
            del clean_students_by_grade_by_site[key]
      clean_students_by_skill_by_site = students_by_skill_by_site
      for key,grade in students_by_skill_by_site.iteritems():
        if students_by_skill[key] == 0:
            del clean_students_by_skill_by_site[key]
      #add by hok
      clean_students_by_computer_by_site = students_by_computer_by_site
      for key,grade in students_by_computer_by_site.iteritems():
        if students_by_computer[key] == 0:
            del clean_students_by_computer_by_site[key]
      #end      
      #find students with unapproved absences and no notes ; get only current school year
      today = date.today()
    #   today = datetime.strptime("2014-08-01", "%Y-%m-%d").date()
      if (today < datetime.strptime(str(today.year)+"-08-01", "%Y-%m-%d").date()):
          school_year = today.year - 1
      else:
          school_year = today.year
      school_year_start_date = str(school_year)+"-08-01"
      school_year_end_date = str(school_year+1)+"-07-31"

      unapproved_absence_no_comment = Attendance.objects.all().filter(attendance__exact="UA").filter(Q(Q(notes=u"") |Q(notes=None)) & Q(Q(date__gte=school_year_start_date) & Q(date__lte=school_year_end_date))).order_by('-date')
      context = { 'all_students': all_students,
                'total_female': total_female,
                'catchup_gender_breakdown':catchup_gender_breakdown,
                'program_breakdown':program_breakdown,
                'total_skills':total_skills,
                'total_computer_skills':total_computer_skills, #hok
                'students_by_grade':students_by_grade,
                'students_at_gl_by_grade': students_at_gl_by_grade,
                'students_by_grade_by_site':clean_students_by_grade_by_site,
                'students_by_skill_by_site':clean_students_by_skill_by_site,
                'students_by_computer_by_site':clean_students_by_computer_by_site,  #hok
                'schools':schools,
                'notifications':notifications,
                'unapproved_absence_no_comment':unapproved_absence_no_comment,
                'total_catchup':total_catchup,
                'total_catchup_female': total_catchup_female,
                'total_english':total_english,
                'total_english_female':total_english_female,
                'english_gender_breakdown':english_gender_breakdown,
                #add hok                                
                'total_computer':total_computer,
                'total_computer_female':total_computer_female,
                'computer_gender_breakdown':computer_gender_breakdown
                }
      return render(request, 'mande/index.html', context)
    else:
      raise PermissionDenied


'''
*****************************************************************************
Notification Log
 - display the most recent 500 entires in the notification log
*****************************************************************************
'''
def notification_log(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      notifications = NotificationLog.objects.order_by('-date')[:500]
      context = {'notifications':notifications}
      return render(request, 'mande/notificationlog.html',context)
    else:
      raise PermissionDenied

'''
*****************************************************************************
Bug Report
 - create issues to Gitlab
*****************************************************************************
'''
def bug_report(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        result = {}
        if request.method == 'POST':
            title = request.POST['title']
            description = request.POST['description']
            label = request.POST['label']
            description = description + '<br/> Report by: ' + str(request.user)
            token = settings.GITLAB['TOKEN']
            project_id = settings.GITLAB['PROJECT_ID']
            url = 'https://gitlab.com/api/v4/projects/'+project_id+'/issues'
            response = requests.post(
                url,
                headers={'PRIVATE-TOKEN': token},
                data = {
                    'title' : title,
                    'description' : description,
                    'labels': label,
                    }
            )
            result['status'] = 'success'
            result['message'] = 'Successfully created an issue'
            if response.status_code != 201:
                result['status'] = 'error'
                result['message'] = response.json()['message']
        return HttpResponse(json.dumps(result))
    else:
      raise PermissionDenied
