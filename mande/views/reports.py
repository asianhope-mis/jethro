# -*- coding: utf-8 -*-
import os
from django.conf import settings
from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.http import HttpResponseRedirect
from django.template import RequestContext, loader
from django.core.exceptions import ObjectDoesNotExist
from django.core.exceptions import PermissionDenied
from django.forms.models import modelformset_factory
from django.db.models import Q,Sum,Count
from django.forms.models import model_to_dict

from django.utils.html import conditional_escape as esc
from django.utils.safestring import mark_safe
from django.core.urlresolvers import reverse
import json
from django.utils.translation import ugettext_lazy as _

from calendar import HTMLCalendar, monthrange
from datetime import date
from datetime import datetime
from datetime import timedelta
from django.views.generic import ListView
from django.views.decorators.vary import vary_on_cookie
from mande.models import IntakeSurvey
from mande.models import IntakeUpdate
from mande.models import Classroom
from mande.models import Teacher
from mande.models import ClassroomEnrollment
from mande.models import ClassroomTeacher
from mande.models import Attendance
from mande.models import ExitSurvey
from mande.models import PostExitSurvey
from mande.models import SpiritualActivitiesSurvey
from mande.models import AttendanceDayOffering
from mande.models import School
from mande.models import Academic
from mande.models import EnglishTest
from mande.models import NotificationLog
from mande.models import Health
from mande.models import Diseases
from mande.models import Vaccines
from mande.models import AttendanceLog
from mande.models import IntakeInternal
from mande.models import StudentEvaluation
from mande.models import PublicSchoolHistory
from mande.models import CurrentStudentInfo

from mande.models import GRADES
from mande.models import ENGLISH_GRADES
from mande.models import GENDERS
from mande.models import ATTENDANCE_CODES
from mande.models import RELATIONSHIPS
from mande.models import YN
from mande.models import EMPLOYMENT

from mande.forms import IntakeSurveyForm
from mande.forms import IntakeUpdateForm
from mande.forms import ExitSurveyForm
from mande.forms import PostExitSurveyForm
from mande.forms import SpiritualActivitiesSurveyForm
from mande.forms import DisciplineForm
from mande.forms import TeacherForm
from mande.forms import ClassroomForm
from mande.forms import ClassroomTeacherForm
from mande.forms import ClassroomEnrollmentForm
from mande.forms import IndividualClassroomEnrollmentForm
from mande.forms import AttendanceForm
from mande.forms import AcademicForm
from mande.forms import IntakeInternalForm
from mande.forms import HealthForm

from mande.utils import getEnrolledStudents
from mande.utils import getStudentGradebyID
from mande.utils import studentAtSchoolGradeLevel
from mande.utils import studentAtAgeAppropriateGradeLevel
from mande.utils import getStudentAgeAppropriateGradeLevel
from mande.utils import getCurrentAcademicYear

from django.contrib.auth.models import User
from django.core.cache import cache
from mande.utils import user_permissions

import inspect
import operator
import re
from icu import Locale, Collator
from collections import OrderedDict
from django.db import connection

TOO_YOUNG = 4
TOO_OLD = 30
'''
*****************************************************************************
Daily Attendance Report
 - lists all classrooms who take attendance and their attendance status
*****************************************************************************
'''
def daily_attendance_report(request,attendance_date=date.today().isoformat()):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      #only classrooms who take attendance, and who take attendance today.
      classrooms = Classroom.objects.all().filter(active=True)
      classrooms_who_take_attendance = []
      for classroom in classrooms:
          if classroom.getAttendanceDayOfferings(attendance_date):
              classrooms_who_take_attendance.append(classroom)

    #   takesattendance = AttendanceDayOffering.objects.filter(
    #                                                     date=attendance_date
    #                                               ).values_list(
    #                                                    'classroom_id',flat=True)
    #   classrooms = classrooms.filter(classroom_id__in=takesattendance)

      classroomattendance = {}
      for classroom in classrooms_who_take_attendance:
        try:
            classroomattendance[classroom] = AttendanceLog.objects.get(
                                                           classroom=classroom,
                                                           date=attendance_date)
        except ObjectDoesNotExist:
            classroomattendance[classroom] = None

      return render(request, 'mande/attendancereport.html',
                            {'classroomattendance' : classroomattendance,
                             'attendance_date': attendance_date
                                                                        })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Student Attendance Detail Report
 - list attendance detail of student
*****************************************************************************
'''
def student_attendance_detail(request,student_id=None,start_date=None,end_date=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        if request.method == 'POST':
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            attendances = Attendance.objects.all().filter(Q(student_id=student_id) & Q(Q(date__gte=start_date) & Q(date__lte=end_date))).order_by('-date')
        else:
            attendances = Attendance.objects.all().filter(student_id=student_id).order_by('-date')
        return render(request, 'mande/student_attendance_detail.html',
                            {
                                'attendances' : attendances,
                                'student_id' : student_id,
                                'start_date' : start_date,
                                'end_date' : end_date
                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Attendance Summary
 - lists attendance summary for student
*****************************************************************************
'''
def attendance_summary_report(request,start_date=None,end_date=None,id=None,select_type=None):
 #get current method name
 method_name = inspect.currentframe().f_code.co_name
 if user_permissions(method_name,request.user):

      exit_surveys = ExitSurvey.objects.filter(exit_date__lte=date.today().isoformat()).values_list('student_id',flat=True)
      students = IntakeSurvey.objects.exclude(student_id__in=exit_surveys).filter(date__lte=date.today().isoformat())

      schools = School.objects.filter(active=True)
      classrooms = Classroom.objects.filter(active=True)
      current_selected = None
      studentattendance = {}

      #filter student by site
      if id != None and select_type == 'site':
        #select students who have not dropped the class, or have not dropped it yet. by site
        students = ClassroomEnrollment.objects.all().filter(Q(classroom_id__school_id=id) & Q(student_id__date__lte=date.today().isoformat()) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))
        current_selected =  School.objects.get(school_id=id)
      #filter by classroom
      elif id != None and select_type == 'classroom':
        #select students who have not dropped the class, or have not dropped it yet. by classroom
        students = ClassroomEnrollment.objects.all().filter(Q(classroom_id__classroom_id=id) & Q(student_id__date__lte=date.today().isoformat()) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))
        current_selected =  Classroom.objects.get(classroom_id=id)

      if request.method == 'POST':
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']
        for stu in students:
            if id != None:
                student = stu.student_id
            else:
                student = stu
            try:
                studentattendance[student] = {
                    'classroom' : ClassroomEnrollment.objects.filter(Q(student_id=student) & Q(Q(drop_date=None) | Q(drop_date__gt=date.today().isoformat()))),
                    'present' : Attendance.objects.filter(student_id=student,attendance='P',date__gte=start_date,date__lte=end_date).count(),
                    'unapproved' : Attendance.objects.filter(student_id=student,attendance='UA',date__gte=start_date,date__lte=end_date).count(),
                    'approved' : Attendance.objects.filter(student_id=student,attendance='AA',date__gte=start_date,date__lte=end_date).count(),
                    'total' : Attendance.objects.filter(student_id=student,attendance='P',date__gte=start_date,date__lte=end_date).count()
                            +Attendance.objects.filter(student_id=student,attendance='UA',date__gte=start_date,date__lte=end_date).count()
                            +Attendance.objects.filter(student_id=student,attendance='AA',date__gte=start_date,date__lte=end_date).count(),
                     }
            except ObjectDoesNotExist:
               studentattendance[student] = None
      else:
        for stu in students:
            if id != None:
                student = stu.student_id
            else:
                student = stu
            try:
                studentattendance[student] = {
                    'classroom' : ClassroomEnrollment.objects.filter(Q(student_id=student) & Q(Q(drop_date=None) | Q(drop_date__gt=date.today().isoformat()))),
                    'present' : Attendance.objects.filter(student_id=student,attendance='P').count(),
                    'unapproved' : Attendance.objects.filter(student_id=student,attendance='UA').count(),
                    'approved' : Attendance.objects.filter(student_id=student,attendance='AA').count(),
                    'total' : Attendance.objects.filter(student_id=student,attendance='P').count()
                            +Attendance.objects.filter(student_id=student,attendance='UA').count()
                            +Attendance.objects.filter(student_id=student,attendance='AA').count()
                     }
            except ObjectDoesNotExist:
               studentattendance[student] = None

      return render(request, 'mande/attendance_summary_report.html',
                            {'studentattendance' : studentattendance,
                            'start_date' : start_date,
                            'end_date' : end_date,
                            'schools' : schools,
                            'classrooms' : classrooms,
                            'current_selected' : current_selected,
                            'select_type' : select_type,
                            'id' : id
                                                                        })
 else:
     raise PermissionDenied


'''
*****************************************************************************
Daily Absence Report
 - lists all students with unexcuses absences for the day and their contact info
*****************************************************************************
'''
def daily_absence_report(request,attendance_date=date.today().isoformat(),attendance_end_date=date.today().isoformat()):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        site = None
        classroom_id = None

        current_site = None
        current_classroom = None
        schools = School.objects.filter(active=True)
        classroom_list = Classroom.objects.filter(active=True)

        if request.method == 'POST':
          classroom_id = request.POST['classroom']
          site = request.POST['site']
          attendance_date = request.POST['attendance_date']
          attendance_end_date = request.POST['attendance_end_date']

        #only classrooms who take attendance, and who take attendance today.
        classrooms = Classroom.objects.all().filter(active=True)

        #filter by site, classroom
        if site != 'None' and site != None:
          classrooms = classrooms.filter(school_id=site)
          current_site = School.objects.get(school_id=site)
        elif classroom_id != 'None' and classroom_id != None:
          classrooms = classrooms.filter(classroom_id=classroom_id)
          current_classroom = Classroom.objects.get(classroom_id=classroom_id)


        classroomattendance = {}
        for classroom in classrooms:
           try:
               #only displays unexcused absences
               classroomattendance[classroom] = Attendance.objects.filter(
                                                              Q(Q(date__gte=attendance_date) & Q(date__lte=attendance_end_date))
                                                              & Q(classroom=classroom)
                                                           #    & Q(attendance='UA')
                                                              ).exclude(attendance=None)
           except ObjectDoesNotExist:
               classroomattendance[classroom] = None
        return render(request, 'mande/absencereport.html',
                            {
                             'classroomattendance' : classroomattendance,
                             'attendance_date': attendance_date,
                             'attendance_end_date':attendance_end_date,
                             'schools' : schools,
                             'classroom_list' : classroom_list,
                             'current_site' : current_site,
                             'current_classroom':current_classroom
                                                                        })
    else:
      raise PermissionDenied
'''
*****************************************************************************
Data Audit
 - Generate a list of student records with missing or anomalous data
*****************************************************************************
'''
@vary_on_cookie
def data_audit(request,audit_type='ALL'):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      #modelfields = model_to_dict(IntakeSurvey.objects.all()[0])
      today = date.today()
      #today = datetime.strptime("2014-08-01", "%Y-%m-%d").date()
      if (today < datetime.strptime(str(today.year)+"-08-01", "%Y-%m-%d").date()):
           school_year = today.year - 1
      else:
           school_year = today.year
      school_year_start_date = str(school_year)+"-08-01"
      school_year_end_date = str(school_year+1)+"-07-31"

      students = getEnrolledStudents()
      filters = []

      #a
      anomalies = {}

      for student in students:
        '''students with missing information'''
        text = 'Missing '
        resolution = reverse('intake_update',kwargs={'student_id':student.student_id})

        student_data = student.getRecentFields()
        ''' Update current public School'''           
        exitsurvey_student = ExitSurvey.objects.all().values_list('student_id',flat=True)            
        if (PublicSchoolHistory.objects.all().exclude(student_id__in=exitsurvey_student).filter(student_id=student,enroll_date__gte=school_year_start_date).count())==0 and ClassroomEnrollment.objects.all().filter(student_id=student,enrollment_date__gte=school_year_start_date):            
            text='Update current public School'
            resolution = reverse('student_detail',kwargs={'student_id':student.student_id})
            addAnomaly(anomalies, student, text, resolution)
            filters.append(text)          
        ''' over attendant aa>=18 or au>=9 '''        
        if (Attendance.objects.all().filter(student_id=student,date__gte=school_year_start_date, date__lte=school_year_end_date,attendance='AA').count())>=18:            
            text='Absent with permission'
            resolution = reverse('student_detail',kwargs={'student_id':student.student_id})
            addAnomaly(anomalies, student, text, resolution)
            filters.append(text)                     
        if (Attendance.objects.all().filter(student_id=student,date__gte=school_year_start_date, date__lte=school_year_end_date,attendance='UA').count())>=9:                            
            text='Absent without permission'
            resolution = reverse('student_detail',kwargs={'student_id':student.student_id})
            addAnomaly(anomalies, student, text, resolution)
            filters.append(text)                     

        '''students who have no photo'''
        if(not(os.path.exists(settings.MEDIA_ROOT +'/' +str(student.student_id)+'.jpg'))):
          text = 'No photo'
          resolution = reverse('student_detail',kwargs={'student_id':student.student_id})
          addAnomaly(anomalies, student, text, resolution)
          filters.append(text)


        '''students who are quite young or quite old'''
        if (student.dob.year > (datetime.now().year-TOO_YOUNG)) or (student.dob.year<datetime.now().year-TOO_OLD):
            text = 'Incorrect DOB'
            age = '(~'+unicode(datetime.now().year-student.dob.year)+' years old)'
            resolution = reverse('intake_survey',kwargs={'student_id':student.student_id})
            limit = 'dob'

            addAnomaly(anomalies, student, text+age, resolution, limit)
            filters.append(text)
        '''students who have never been present'''
        if Attendance.objects.filter(student_id=student,attendance='P').count()==0:

            '''are either not enrolled'''
            if len(ClassroomEnrollment.objects.filter(student_id=student))==0:
                text = 'Not enrolled in any classes'
                resolution = reverse('classroomenrollment_form')
                addAnomaly(anomalies, student, text, resolution)
                filters.append(text)

            '''... or just not good at showing up!'''
            if Attendance.objects.filter(student_id=student).count()>0:
                text = 'Has never attended class'
                resolution = reverse('student_detail',kwargs={'student_id':student.student_id})
                addAnomaly(anomalies, student, text, resolution)
                filters.append(text)
        '''student no public school history'''
        if PublicSchoolHistory.objects.filter(student_id=student).count()==0:
            text = 'No Public School History'
            resolution = reverse('student_detail',kwargs={'student_id':student.student_id})
            addAnomaly(anomalies, student, text, resolution)
            filters.append(text)
      ''' students who have unapproved absences with no comment, and get only current school year'''
      
      uastudents = Attendance.objects.all().filter(attendance__exact="UA").filter(Q(Q(notes=u"") |Q(notes=None)) & Q(Q(date__gte=school_year_start_date) & Q(date__lte=school_year_end_date))).order_by('-date')

      for uastudent in uastudents:
        text = 'Unapproved absence with no comment'
        attendance_date = uastudent.date

        attendance_class = uastudent.classroom
        ''' historical data has null classroom. need to determine how to resolve '''
        if attendance_class is None:
          resolution = ''
          text = 'Unapproved abscence with no comment - missing class id'
        if attendance_class is not None:
          resolution = reverse('take_class_attendance',kwargs={'attendance_date':attendance_date.strftime('%Y-%m-%d'), 'classroom_id':attendance_class.classroom_id})
        addAnomaly(anomalies, uastudent.student_id, text, resolution)
        filters.append(text)

      ''' students who exit >=6 months '''  
      post_exit_student = PostExitSurvey.objects.all().values_list('student_id',flat=True)      
      filter_exit_survey = ExitSurvey.objects.all().exclude(student_id__in=post_exit_student)                  
      for stu in filter_exit_survey:      
          from_exit_date_6months=(stu.exit_date + timedelta(days=6 * 365/12)).strftime("%Y-%m-%d")          
          if datetime.now().strftime("%Y-%m-%d")>=from_exit_date_6months: 
            text = 'Post Exit Survey'            
            resolution = reverse('post_exit_survey',kwargs={'student_id':stu.student_id_id})
            addAnomaly(anomalies, stu.student_id, text, resolution)
            filters.append(text)          

      #remove duplicates in a now long array
      filters = set(filters)
      filters = sorted(filters)
      return render(request, 'mande/data_audit.html',
                            {'students' : anomalies,'filters':filters})
    else:
      raise PermissionDenied


def addAnomaly(anomalies, student, text, resolution, limit=None):
    try:
        anomalies[student].append(
                            {'text':text,
                             'resolution':resolution,
                            'limit':limit})
    except KeyError:
        anomalies[student] = [{'text':text,
                               'resolution':resolution,
                               'limit':limit}]
    return anomalies

'''
*****************************************************************************
Class List
 - Generate a summary for each class in each site:
    + VDP Campus
    + Class Name
    + Teacher
    + # of Students Enrolled
*****************************************************************************
'''
@vary_on_cookie
def class_list(request,site='ALL'):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      academic_year = getCurrentAcademicYear()
      class_list={}
      classrooms = Classroom.objects.filter(active=True)
      for classroom in classrooms:
        instance = Classroom.objects.get(classroom_id=classroom.pk)
        class_list[classroom]={
            'site':classroom.school_id,
            'target_grade':classroom.cohort,
            'classroom_number':classroom.classroom_number,
            'teacher': 'Not assigned',
            'students': 0,
            'female': 0,
        }
        try:
            class_list[classroom]['teacher'] = ClassroomTeacher.objects.filter(classroom_id=classroom.pk,year=academic_year)
        except ObjectDoesNotExist:
            pass
        try:
            enrolled_students =  instance.classroomenrollment_set.all().filter(Q(student_id__date__lte=date.today().isoformat()) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))
            female_students = 0
            for student in enrolled_students:
                if student.student_id.gender == 'F':
                    female_students +=1
            class_list[classroom]['students'] = len(enrolled_students)
            class_list[classroom]['female'] = female_students
        except ObjectDoesNotExist:
            pass


      return render(request, 'mande/class_list.html',
                            {'class_list' : class_list,'academic_year':academic_year})
    else:
      raise PermissionDenied

'''
*****************************************************************************
Exit Surveys list
 -filter exit Surveys by date range
*****************************************************************************
'''
def exit_surveys_list(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      if request.method == 'POST':
        from_exit_date = request.POST['from_exit_date']
        to_exit_date = request.POST['to_exit_date']
        exit_surveys = ExitSurvey.objects.all().filter(exit_date__gte=from_exit_date , exit_date__lte=to_exit_date)
      else:
        #get today date and subtract two months
        from_exit_date=(datetime.now()- timedelta(days=2 * 365/12)).strftime("%Y-%m-%d")
        to_exit_date=(datetime.now()).strftime("%Y-%m-%d")
        exit_surveys = ExitSurvey.objects.all().filter(exit_date__gte=from_exit_date , exit_date__lte=to_exit_date)

      post_exit_surveys = PostExitSurvey.objects.all()
      return render(request, 'mande/exitsurveylist.html',
                            {'exit_surveys':exit_surveys,'post_exit_surveys':post_exit_surveys,'from_exit_date':from_exit_date,'to_exit_date':to_exit_date})
    else:
      raise PermissionDenied
'''
*****************************************************************************
Student Absence Report
 - makes a summary of all students and lists their daily absences/presence
*****************************************************************************
'''
def student_absence_report(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      attendances = Attendance.objects.all()

      #set up dict of attendance codes with zero values
      default_attendance ={}
      attendancecodes = dict(ATTENDANCE_CODES)
      for key,code in attendancecodes.iteritems():
        default_attendance[key]=0

      #default out all current students
      attendance_by_sid = {}
      currently_enrolled_students = getEnrolledStudents()
      for student in currently_enrolled_students:
        attendance_by_sid[student]=dict(default_attendance)


      for attendance in attendances:
        try:
            attendance_by_sid[attendance.student_id][attendance.attendance] +=1
        except KeyError:
            pass; #students no longer in attendance that have attendance

      return render(request, 'mande/student_absence_report.html',
                                {'attendance_by_sid':attendance_by_sid, 'attendancecodes':attendancecodes})
    else:
      raise PermissionDenied
'''
*****************************************************************************
Student Lag Report
 - makes a summary of all students and lists their daily absences/presence
*****************************************************************************
'''
@vary_on_cookie
def student_lag_report(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      enrolled_students = getEnrolledStudents()
      students_lag = {}

      if request.method == 'POST':
        view_date = request.POST['view_date']
      else:
        # convert to correct format with html input type date
        view_date = date.today().strftime("%Y-%m-%d")

      for student in enrolled_students:
        current_vdp_grade = student.current_vdp_grade(view_date)
        #only students in the scope of grade levels
        if current_vdp_grade < 12 and current_vdp_grade >= -3:
            age_appropriate_grade = student.age_appropriate_grade(view_date)
            lag = age_appropriate_grade - current_vdp_grade

            if age_appropriate_grade < 0 and current_vdp_grade > 0:
                lag = lag + 1
            elif current_vdp_grade < 0 and age_appropriate_grade > 0:
                lag = lag - 1

            students_lag[student] = {
                    'lag':lag,
                    'appropriate_grade':age_appropriate_grade,
                    'vdp_grade':current_vdp_grade,
                    'date_achieved_age_appropriate_level': student.date_enrolled_grade(current_vdp_grade) if current_vdp_grade - age_appropriate_grade<=0 else student.date_enrolled_grade(current_vdp_grade-(current_vdp_grade - age_appropriate_grade))
            }

      return render(request, 'mande/student_lag_report.html',
                                {'students_lag':students_lag,'view_date':view_date})
    else:
      raise PermissionDenied


'''
*****************************************************************************
Student Evaluation Report
 - lists all student evaluations
*****************************************************************************
'''
def student_evaluation_report(request,grade_id=0,start_date=None,end_date=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      if start_date and end_date:
        evaluations = StudentEvaluation.objects.all().filter(
                                    Q(date__gte=start_date) & Q(date__lte=end_date)
                                ).exclude(
                                    academic_score=None,
                                    study_score=None,
                                    personal_score=None,
                                    hygiene_score=None,
                                    faith_score=None
                                )
      else:
        evaluations = StudentEvaluation.objects.all().exclude(academic_score=None,
                                                              study_score=None,
                                                              personal_score=None,
                                                              hygiene_score=None,
                                                              faith_score=None)
      if grade_id != 0 and grade_id !='0':
        #select students who have not dropped the class, or have not dropped it yet.
        if grade_id == '50':
            enrolled_students = ClassroomEnrollment.objects.all().filter(
                                Q(
                                    Q(student_id__date__lte=date.today().isoformat()) &
                                    Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                                )
                                &
                                Q(classroom_id__cohort__gt=50,classroom_id__cohort__lt=60)
                            ).values_list('student_id',flat=True)
        elif grade_id == '70':
            enrolled_students = ClassroomEnrollment.objects.all().filter(
                                Q(
                                    Q(student_id__date__lte=date.today().isoformat()) &
                                    Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                                )
                                &
                                Q(classroom_id__cohort__gt=70,classroom_id__cohort__lt=80)
                            ).values_list('student_id',flat=True)
        else:
            enrolled_students = ClassroomEnrollment.objects.all().filter(
                                Q(
                                    Q(student_id__date__lte=date.today().isoformat()) &
                                    Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                                )
                                &
                                Q(classroom_id__cohort=grade_id)
                            ).values_list('student_id',flat=True)


        evaluations = evaluations.filter(student_id__in=enrolled_students)
      return render(request,
                    'mande/studentevaluationreport.html',
                    {
                        'evaluations':evaluations,
                        'grade_id':grade_id,
                        'start_date':start_date,
                        'end_date':end_date,
                        'grades':OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]),
                    })
    else:
      raise PermissionDenied
'''
*****************************************************************************
Student Achievement Report
 - lists all student Achievement test
*****************************************************************************
'''
def student_achievement_test_report(request,grade_id=0,start_date=None,end_date=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      if start_date and end_date:
        #achievement_tests = Academic.objects.all().filter(Q(test_date__gte=start_date) & Q(test_date__lte=end_date)).exclude(
        #                                                test_grade_math=None,
        #                                                test_grade_khmer=None,
        #                                                )
        achievement_tests = Academic.objects.all().filter(Q(test_date__gte=start_date) & Q(test_date__lte=end_date)
                                                          & 
                                                           (Q(test_grade_khmer__isnull=False) |
                                                            Q(test_grade_math__isnull=False)) 
                                                             | 
                                                           (Q(activity_test__isnull=False) |
                                                            Q(quiz__isnull=False) |
                                                            Q(monthly_test__isnull=False) |
                                                            Q(middle_test__isnull=False) |
                                                            Q(final_test__isnull=False))
                                                          ).order_by('-test_level')
      else:
          #achievement_tests = Academic.objects.all().exclude(
          #                                                  test_grade_math=None,
          #                                                  test_grade_khmer=None,
          #                                                  )
          achievement_tests = Academic.objects.all().filter(
                                                              (Q(test_grade_khmer__isnull=False) |
                                                              Q(test_grade_math__isnull=False)) 
                                                              | 
                                                              (Q(activity_test__isnull=False) |
                                                              Q(quiz__isnull=False) |
                                                              Q(monthly_test__isnull=False) |
                                                              Q(middle_test__isnull=False) |
                                                              Q(final_test__isnull=False))).order_by('-test_level')      
                                                            
          
      if grade_id != 0 and grade_id !='0':
        #select students who have not dropped the class, or have not dropped it yet.
        enrolled_students = ClassroomEnrollment.objects.all().filter(
                            Q(
                                Q(student_id__date__lte=date.today().isoformat()) &
                                Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                            )
                            &
                            Q(classroom_id__cohort=grade_id)
                        ).values_list('student_id',flat=True)
        achievement_tests = achievement_tests.filter(student_id__in=enrolled_students)
      return render(request, 'mande/studentachievementtestreport.html',
                                {
                                    'achievement_tests':achievement_tests,
                                    'grade_id':grade_id,
                                    'start_date':start_date,
                                    'end_date':end_date,
                                    'grades':OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
                                })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Student English Test Report
 - lists all student English test
*****************************************************************************
'''
def student_englishtest(request,grade_id=0,start_date=None,end_date=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        if start_date and end_date:
          english_tests = EnglishTest.objects.all().filter(Q(test_date__gte=start_date) & Q(test_date__lte=end_date)).exclude(
                                                          activity_test=None,
                                                          quiz=None,
                                                          monthly_test=None,
                                                          middle_test=None,
                                                          final_test=None
                                                          )
        else:
            english_tests = EnglishTest.objects.all().exclude(
                                                               activity_test=None,
                                                               quiz=None,
                                                               monthly_test=None,
                                                               middle_test=None,
                                                               final_test=None
                                                              )
        if grade_id != 0 and grade_id !='0':
          #select students who have not dropped the class, or have not dropped it yet.
          enrolled_students = ClassroomEnrollment.objects.all().filter(
                              Q(
                                  Q(student_id__date__lte=date.today().isoformat()) &
                                  Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                              )
                              &
                              Q(classroom_id__cohort=grade_id)
                          ).values_list('student_id',flat=True)
          english_tests = english_tests.filter(student_id__in=enrolled_students)
        return render(request, 'mande/studentenglishtestreport.html',
                                  {
                                      'english_tests':english_tests,
                                      'grade_id':grade_id,
                                      'start_date':start_date,
                                      'end_date':end_date,
                                      'grades':OrderedDict(ENGLISH_GRADES),
                                  })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Student Medical Report
 - lists all student medical visits
*****************************************************************************
'''
def student_medical_report(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      enrolled_students = getEnrolledStudents()
      visits = {}
      for student in enrolled_students:
        try:
            visits[student] = len(Health.objects.all().filter(student_id=student))
        except ObjectDoesNotExist:
            pass
      return render(request, 'mande/studentmedicalreport.html',
                                {'visits':visits})
    else:
      raise PermissionDenied

'''
*****************************************************************************
Student Dental Summary Report
 - lists all student Dental visits
*****************************************************************************
'''
def student_dental_summary_report(request,site_id=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      if site_id is not None:
        dentals= Health.objects.all().filter(appointment_type='Dental',student_id__site=site_id)
        current_site =  School.objects.get(school_id=site_id)
      else:
        current_site = 'All Site'
        dentals= Health.objects.all().filter(appointment_type='Dental')

      unique_students = dentals.values('student_id').annotate(dcount=Count('student_id')).count()

      year = datetime.now().year-2013
      dentals_by_month_year=[]
      for x in range(year):
        dentals_by_month_year.extend([{'group_by_date':str(datetime.now().year-x)+'-'+format(i+1, '02d'),'dentals':[], 'unique':0} for i in range(12)])

      for dental in dentals:
        for dental_by_month_year in dentals_by_month_year:
            generate_to_date=datetime.strptime(dental_by_month_year['group_by_date'], '%Y-%m')
            if(generate_to_date.year==dental.appointment_date.year and generate_to_date.month==dental.appointment_date.month):
                dental_by_month_year['dentals'].append(dental)
                unique_students_by_month = dentals.filter(appointment_date__year=generate_to_date.year, appointment_date__month=generate_to_date.month).values('student_id').annotate(dcount=Count('student_id')).count()
                dental_by_month_year['unique'] = unique_students_by_month

      sites = School.objects.filter(active=True)
      return render(request, 'mande/studentdentalreport.html',
                            {
                                'dentals_by_month_year':dentals_by_month_year,
                                'sites':sites,
                                'current_site':current_site,
                                'unique_students':unique_students
                            })
    else:
      raise PermissionDenied
'''
*****************************************************************************
Student Dental Report
 - lists all student Dental visits
*****************************************************************************
'''
def student_dental_report(request,start_date=None,end_date=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        if request.method == 'POST':
          start_date = request.POST['start_date']
          end_date = request.POST['end_date']
          dentals = Health.objects.filter( Q( Q( Q(appointment_date__gte=start_date) & Q(appointment_date__lte=end_date) ) & Q(appointment_type='Dental')) )
        else:
          dentals = Health.objects.filter(appointment_type='Dental')

        return render(request, 'mande/studentdental.html',
                            {
                                'dentals':dentals,
                                'start_date':start_date,
                                'end_date' : end_date
                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
M&E summary Report
 -
*****************************************************************************
'''
@vary_on_cookie
def mande_summary_report(request,start_view_date=date.today().isoformat(),view_date=date.today().isoformat()):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        # Catch-up school report
        schools = School.objects.filter(active=True)
        exit_surveys = ExitSurvey.objects.filter(exit_date__lte=start_view_date).values_list('student_id',flat=True)
        students = IntakeSurvey.objects.exclude(student_id__in=exit_surveys).filter(date__lte=view_date)

        students_by_site_grade = {}
        students_enrolled_in_english_by_level = {}
        students_by_site= {}

        # catch up grade level
        grades_level = [item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]
        # english grade level
        english_level = set(Classroom.objects.filter(cohort__gt=50,cohort__lt=60).values_list('cohort',flat=True))

        # generate_list of students group by site and grade
        for school in schools:
           name = school.school_name
           students_by_site_grade[name] = OrderedDict()
           students_enrolled_in_english_by_level[name] = OrderedDict()
           students_by_site[name] = {'M':0,'F':0}

           for key,value in grades_level:
               students_by_site_grade[name][key] = {'M':{'data':0,'age_appropriate':0},'F':{'data':0,'age_appropriate':0}}
           students_by_site_grade[name]['total'] = {'M':{'data':0,'age_appropriate':0},'F':{'data':0,'age_appropriate':0}}

           for a in english_level:
               students_enrolled_in_english_by_level[name][a] = {'M':0,'F':0}
           students_enrolled_in_english_by_level[name]['total'] = {'M':0,'F':0}

        for student in students:
            grade = student.current_vdp_grade(view_date)
            site = student.site
            gender = student.gender

            # total student
            students_by_site[unicode(site)][gender] +=1

            # english student by level
            try:
                students_enrolled_in_english_by_level[unicode(site)][grade][gender] +=1
                students_enrolled_in_english_by_level[unicode(site)]['total'][gender] +=1
            except KeyError:
                pass

            try:
                students_by_site_grade[unicode(site)][grade][gender]['data'] +=1
                students_by_site_grade[unicode(site)]['total'][gender]['data'] +=1
                #student appropriate level
                if (student.age_appropriate_grade(view_date) - grade < 1):
                    students_by_site_grade[unicode(site)][grade][gender]['age_appropriate'] +=1
                    students_by_site_grade[unicode(site)]['total'][gender]['age_appropriate'] +=1
            except KeyError:
                pass

        return render(request, 'mande/mandesummaryreport.html',
                                {
                                    'view_date':view_date,
                                    'start_view_date':start_view_date,
                                    'schools':schools,
                                    'students_by_site_grade' : students_by_site_grade,
                                    'students_by_site' : students_by_site,
                                    'students_enrolled_in_english_by_level':students_enrolled_in_english_by_level,
                                    'grades_level':grades_level,
                                    'english_level':english_level,
                                })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Student Promoted Report
 - lists all student Promoted
*****************************************************************************
'''

def student_promoted_report(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      academics = Academic.objects.filter(promote = True)
      students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())
      schools = School.objects.all()
      promoted_years = []
      years = datetime.now().year-2012
      list_of_years = []
      # generate list of year
      for r in range(years):
        list_of_years.append(datetime.now().year-r)
      # generate list of student break down by site and year
      for school in schools:
        promoted_years.extend(
            [
                {
                'school':school,
                'total':[],
                'years':[{'year'+str(i):{'years':str(i),'students':[]}} for i in list_of_years],

                }
            ]
        )
      for student in students:
        academics = Academic.objects.filter(student_id=student,promote=True)
        for promoted_year in promoted_years:
                if promoted_year['school'] == student.site:
                    for each_year in  promoted_year['years']:
                        for i in range(years):
                                try:
                                    if len(academics) != 0:
                                        for academic in academics:
                                            # get academic by school year
                                            # the 2014 school year is 1 Aug 2014 - 31 July 2015
                                            # the 2015 school year is 1 Aug 2015 - 31 July 2016
                                            if each_year['year'+str(datetime.now().year-i)]['years'] == str(academic.test_date.year) or int(each_year['year'+str(datetime.now().year-i)]['years'])+1 == academic.test_date.year:
                                                beginning = str(each_year['year'+str(datetime.now().year-i)]['years'])+"-08-01"
                                                end = str(int(each_year['year'+str(datetime.now().year-i)]['years'])+1)+"-07-31"

                                                beginning_of_school_year = datetime.strptime(beginning, "%Y-%m-%d").date()
                                                end_of_school_year = datetime.strptime(end, "%Y-%m-%d").date()

                                                if academic.test_date >= beginning_of_school_year and academic.test_date <= end_of_school_year:
                                                    each_year['year'+str(datetime.now().year-i)]['students'].append(academic.student_id)
                                                    promoted_year['total'].append(academic.student_id)

                                except:
                                    pass
      return render(request, 'mande/student_promoted_report.html',
                            {
                                'promoted_years':promoted_years,
                                'list_of_years':list_of_years
                            })
    else:
      raise PermissionDenied


'''
*****************************************************************************
Students Promoted Times Report
 - lists all number of times student has been promoted
*****************************************************************************
'''
def students_promoted_times_report(request,filter_seach='CURRENT',year=0,school=0,grade=0,classroom=0):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      sites = School.objects.filter(active=True)
      academic_year_start = datetime.now().year-2013
      academic_years = []
      # generate list of year
      for r in range(academic_year_start):
        academic_years.append(datetime.now().year-r)

      if filter_seach=='ALL':
        students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())

      elif filter_seach != 'ALL':
        exit_surveys = ExitSurvey.objects.filter(exit_date__lte=date.today().isoformat()).values_list('student_id',flat=True)
        students = IntakeSurvey.objects.exclude(student_id__in=exit_surveys).filter(date__lte=date.today().isoformat())
        enrolled_catch_up = ClassroomEnrollment.objects.all().filter( Q( Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)) & Q(Q(Q(classroom_id__cohort__gte=1) & Q(classroom_id__cohort__lte=6)) | Q(Q(classroom_id__cohort__gte=-3) & Q(classroom_id__cohort__lte=-1))) ) ).values_list('student_id',flat=True)
        students = students.filter(student_id__in=enrolled_catch_up)

      if int(classroom) != 0:
        enrolled_class = ClassroomEnrollment.objects.filter(Q(Q(classroom_id=int(classroom)) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))).values_list('student_id',flat=True)
        students = students.filter(student_id__in=enrolled_class)
      elif int(school) !=0 and int(grade) !=0:
        enrolled_class = ClassroomEnrollment.objects.filter(Q(Q(classroom_id__school_id=int(school)) & Q(classroom_id__cohort=int(grade)) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))).values_list('student_id',flat=True)
        students = students.filter(student_id__in=enrolled_class)
      elif int(classroom) == 0 and int(school) != 0 and int(grade) == 0:
        students = students.filter(site=school)




      students_promoted = {}
      if int(year) != 0:
          school_year_start_date = str(year)+"-08-01"
          school_year_end_date = str(int(year)+1)+"-07-31"
          for student in students:
              current_grade = student.current_vdp_grade()
              #if the student has a valid current grade
              if current_grade < 50:
                    try:
                       intake_date = student.intakeinternal_set.all().filter().order_by(
                                    '-enrollment_date'
                                )[0].enrollment_date
                    except IndexError:
                        intake_date = None
                    students_promoted[student] = {
                        'promoted_times':len( Academic.objects.filter( Q(student_id=student) & Q(promote=True) & Q(Q(test_date__gte=school_year_start_date) & Q(test_date__lte=school_year_end_date)) ) ),
                        'lastest_promoted_date':Academic.objects.filter(student_id=student,promote=True).latest('test_date').test_date if len(Academic.objects.filter(student_id=student,promote=True)) > 0 else None,
                        'enrolled_date' : intake_date
                    }
      else:
          for student in students:
              current_grade = student.current_vdp_grade()
              #if the student current grade is in catch-up
              if current_grade < 50:
                    try:
                       intake_date = student.intakeinternal_set.all().filter().order_by(
                                    '-enrollment_date'
                                )[0].enrollment_date
                    except IndexError:
                        intake_date = None
                    students_promoted[student] = {
                        'promoted_times':len( Academic.objects.filter( Q(student_id=student) & Q(promote=True)) ),
                        'lastest_promoted_date':Academic.objects.filter(student_id=student,promote=True).latest('test_date').test_date if len(Academic.objects.filter(student_id=student,promote=True)) > 0 else None,
                        'enrolled_date' : intake_date
                    }
      return render(request, 'mande/students_promoted_times_report.html',
                            {
                                'students_promoted':students_promoted,
                                'filter_seach':filter_seach,
                                'sites':sites,
                                'grades':OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]),
                                'academic_years':academic_years,
                                'year':year,
                                'school':school,
                                'grade':grade,
                                'classroom':classroom

                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Students not enrolled in public school Report
 - lists all students not enrolled in public school
*****************************************************************************
'''
def public_school_report(request,academic_year=0,start_date='11-01',end_date='10-31',number=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      exit_surveys = ExitSurvey.objects.all().filter(
                      exit_date__lte=date.today().isoformat()
                      ).values_list('student_id',flat=True)
      active_surveys = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                               ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
      pschools = PublicSchoolHistory.objects.filter(student_id__student_id__in=active_surveys)
      months = [
        ('01', 'January'), ('02', 'February'), ('03', 'March'),
        ('04', 'April'), ('05', 'May'), ('06', 'June'),
        ('07', 'July'), ('08', 'August'), ('09', 'September'),
        ('10', 'October'), ('11', 'November'), ('12', 'December')
       ]

      if int(academic_year) != 0:
          academic_year_start = str(academic_year)+'-'+start_date
          academic_year_end = str(int(academic_year)+1)+'-'+end_date
          pschools = pschools.filter( Q(enroll_date__gte=academic_year_start) & Q(enroll_date__lte=academic_year_end) )

      if number:
          students_match_number = pschools.values('student_id_id').annotate(count=Count('student_id_id')).filter(count=number).values_list('student_id_id',flat=True)
          pschools = pschools.filter(student_id__student_id__in=students_match_number)

      academic_year_start = datetime.now().year-2013
      academic_years = []
      # generate list of year
      for r in range(academic_year_start):
        academic_years.append(datetime.now().year-r)

      grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
      grades[50]=_('English')
      grades[70]=_('Vietnamese')
      return render(request, 'mande/public_school_report.html',
                            {
                                'pschools' : pschools,
                                'grades' : grades,
                                'sites' : School.objects.filter(active=True),
                                'academic_years':academic_years,
                                'academic_year': academic_year,
                                'number' : number,
                                'start_date' : start_date,
                                'end_date' : end_date,
                                'months': months,
                                'days':range(1,32)
                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Students intergrated in public school Report
 - lists all students intergrated in public school
*****************************************************************************
'''
def students_intergrated_in_public_school(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())
      intergrated_students = []
      for student in students:
        if student.enrolled == 'N' :
            if student.getRecentFields()['enrolled'] == 'Y':
                intergrated_students.append(student)
      return render(request, 'mande/students_intergrated_in_public_school_report.html',
                            {
                                'intergrated_students' : intergrated_students
                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Students Lag Summary Report
 - Summary of student Lag
*****************************************************************************
'''
def students_lag_summary(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      enrolled_students = getEnrolledStudents()
      schools = School.objects.filter(active=True)

      students_lag_by_site = {}
      students_all_site = {
        'students_age_not_appropriate_grade_level' : {'M':0,'F':0},
        'students' : {'M':0,'F':0}
      }
      for school in schools:
        name = school.school_name
        students_lag_by_site[name] = {}
        students_lag_by_site[name]['students_age_not_appropriate_grade_level'] = {'M':0,'F':0}
        students_lag_by_site[name]['students'] = {'M':0,'F':0}

      for student in enrolled_students:
        age_appropriate_grade = student.age_appropriate_grade()
        current_grade = student.current_vdp_grade()
        site = student.site
        gender = student.gender
        if current_grade <= 6 and current_grade >= -3:
            students_lag_by_site[unicode(site)]['students'][gender] +=1
            students_all_site['students'][gender] +=1

            lag = age_appropriate_grade - current_grade
            if lag > 0:
                students_lag_by_site[unicode(site)]['students_age_not_appropriate_grade_level'][gender] +=1
                students_all_site['students_age_not_appropriate_grade_level'][gender] +=1
      return render(request, 'mande/students_lag_summary.html',
                            {
                                'students_lag_by_site' : students_lag_by_site,
                                'students_all_site' : students_all_site
                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Anamolous data report
 - get students have intake survey date in future
*****************************************************************************
'''
def anomalous_data(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      future_students = IntakeSurvey.objects.all().filter(date__gte=date.today().isoformat()).order_by('student_id')

      return render(request, 'mande/anomalous_data_report.html',
                            {
                                'future_students' : future_students,
                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
advance report
 - advance report
*****************************************************************************
'''

def advanced_report(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      all_intake_survey = IntakeSurvey.objects.all()
      all_intake_updated = IntakeUpdate.objects.all()

      data_guardian1_profession = list(all_intake_survey.values_list('guardian1_profession',flat=True).distinct())
      guardian1_profession_intake_update = list(all_intake_updated.values_list('guardian1_profession',flat=True).distinct())
      data_guardian1_profession.extend(guardian1_profession_intake_update)

      data_guardian2_profession = list(all_intake_survey.values_list('guardian2_profession',flat=True).distinct())
      guardian2_profession_intake_update = list(all_intake_updated.values_list('guardian2_profession',flat=True).distinct())
      data_guardian2_profession.extend(guardian2_profession_intake_update)

      data_minors_profession = list(all_intake_survey.values_list('minors_profession',flat=True).distinct())
      minors_profession_intake_update = list(all_intake_updated.values_list('minors_profession',flat=True).distinct())
      data_minors_profession.extend(minors_profession_intake_update)

      data_minors_training_type = list(all_intake_survey.values_list('minors_training_type',flat=True).distinct())
      minors_training_type_intake_update = list(all_intake_updated.values_list('minors_training_type',flat=True).distinct())
      data_minors_training_type.extend(minors_training_type_intake_update)

      data_reasons = list(PublicSchoolHistory.objects.all().values_list('reasons',flat=True).distinct())

      data_public_schools = list(PublicSchoolHistory.objects.all().values_list('school_name',flat=True).distinct())
      # sort khmer
      data_public_schools = [x.encode('utf-8').strip() for x in data_public_schools if x is not None]
      locale = Locale('km_KH')
      collator = Collator.createInstance(locale)
      data_public_schools = sorted(set(data_public_schools),key=collator.getSortKey)

      q=[]
      filter_query = Q()
      recent_field_list={}
      show_data = 'no'
      students = None

      schools = School.objects.all()
      classrooms = Classroom.objects.filter(active=True)
      genders = dict(GENDERS)
      grades = dict(GRADES)
      yns = dict(YN)
      employments = dict(EMPLOYMENT)
      relationships = dict(RELATIONSHIPS)

      #   ------------------
      #   convert object fields to human readable
      def convert_field_to_readable(string):
           s = string
           s1= re.sub(r'([^0-9])([0-9])', r'\1 \2',s)
           s2 =  re.sub(r'([$0-9])([a-z])', r'\1 \2',s1)
           s3 = s2.replace('_', ' ').title()
           return s3

      all_fields = IntakeSurvey._meta.fields
      list_of_fields = dict((field.name, convert_field_to_readable(field.name)) for field in all_fields if not field.primary_key)
      list_of_fields.update({
            "vdp_grade":"VDP Grade", "classroom": "Classroom","id":"ID",
            "enrolled":"Enrolled","grade_current":"Grade Current",
            "grade_last":"Grade Last","reasons":"Reasons",
            "public_school_name":"Public School Name"
            })
      if request.method == 'POST':

         student_id = request.POST['studnet_id']
         name = request.POST['name']
         school = request.POST['school']
         gender = request.POST['gender']
         intake_date = request.POST['intake_date']
         intake_date_year = request.POST['intake_date_year']
         dob = request.POST['dob']
         dob_year = request.POST['dob_year']

         address = request.POST['address']

         guardian1_name = request.POST['guardian1_name']
     	 guardian1_relationship = request.POST['guardian1_relationship']
     	 guardian1_phone = request.POST['guardian1_phone']
     	 guardian1_profession = request.POST['guardian1_profession']
     	 guardian1_employment = request.POST['guardian1_employment']

         guardian2_name = request.POST['guardian2_name']
     	 guardian2_relationship = request.POST['guardian2_relationship']
     	 guardian2_phone = request.POST['guardian2_phone']
     	 guardian2_profession = request.POST['guardian2_profession']
     	 guardian2_employment = request.POST['guardian2_employment']

          #Household Information
      	 minors = request.POST['minors']
      	 minors_in_public_school = request.POST['minors_in_public_school']
      	 minors_in_other_school = request.POST['minors_in_other_school']
      	 minors_working = request.POST['minors_working']
      	 minors_profession = request.POST['minors_profession']
      	 minors_encouraged = request.POST['minors_encouraged']
      	 minors_training = request.POST['minors_training']
      	 minors_training_type = request.POST['minors_training_type']

         enrolled = request.POST['enrolled']
         grade_current = request.POST['grade_current']
         grade_last = request.POST['grade_last']
         public_school_name = request.POST['public_school_name']
         reasons = request.POST['reasons']

         classroom = request.POST['classroom']
         vdp_grade = request.POST['vdp_grade']

        #  ----field that don't have in intakeupdate----
         if school != '':
             q.append(Q(site=school))
         if intake_date != '':
             q.append(Q(date=intake_date))
         if dob != '':
            q.append(Q(dob=dob))
         if gender != '':
             q.append(Q(gender=gender))
         if dob_year != '':
             q.append(Q(dob__year=int(dob_year)))
         if intake_date_year != '':
             q.append(Q(date__year=int(intake_date_year)))
         if len(q) > 0:
             filter_query = reduce(operator.and_, q)

         students = IntakeSurvey.objects.filter(
                     Q(student_id__contains=student_id) &
                     Q(name__contains=name)
                     ).filter(filter_query)
         #  ----------------------------------------


         # ----- for field that has in intakeupdate -----
         if address != '':
             recent_field_list['address'] = address

         #  guardian 1
         if guardian1_name != '':
             recent_field_list['guardian1_name'] = guardian1_name
         if guardian1_relationship != '':
             recent_field_list['guardian1_relationship'] = guardian1_relationship
         if guardian1_phone != '':
             recent_field_list['guardian1_phone'] = guardian1_phone
         if guardian1_profession != '':
             recent_field_list['guardian1_profession'] = guardian1_profession
         if guardian1_employment != '':
             recent_field_list['guardian1_employment'] = guardian1_employment

        #  guardian 2
         if guardian2_name != '':
             recent_field_list['guardian2_name'] = guardian2_name
         if guardian2_relationship != '':
             recent_field_list['guardian2_relationship'] = guardian2_relationship
         if guardian2_phone != '':
             recent_field_list['guardian2_phone'] = guardian2_phone
         if guardian2_profession != '':
             recent_field_list['guardian2_profession'] = guardian2_profession
         if guardian2_employment != '':
             recent_field_list['guardian2_employment'] = guardian2_employment

        # minors
         if minors != '':
            recent_field_list['minors'] = int(minors)
         if minors_in_public_school != '':
            recent_field_list['minors_in_public_school'] = int(minors_in_public_school)
         if minors_in_other_school != '':
            recent_field_list['minors_in_other_school'] = int(minors_in_other_school)
         if minors_working != '':
            recent_field_list['minors_working'] = int(minors_working)
         if minors_profession != '':
            recent_field_list['minors_profession'] = minors_profession
         if minors_encouraged != '':
            recent_field_list['minors_encouraged'] = minors_encouraged
         if minors_training != '':
            recent_field_list['minors_training'] = minors_training
         if minors_training_type != '':
            recent_field_list['minors_training_type'] = minors_training_type

         equal_value_list = ['guardian1_relationship','guardian2_relationship',
                             'guardian1_employment','guardian2_employment',
                             'minors','minors_in_public_school','minors_in_other_school',
                             'minors_working','minors_encouraged','minors_training',
                            ]
         match_intakeUpdate = []
         for key, value in recent_field_list.iteritems():
             match_intakeUpdate=[]
             for student in students:
                student_recent_data = student.getRecentFields()
                if key not in equal_value_list:
                    if value in student_recent_data[key]:
                        match_intakeUpdate.append(student.student_id)
                else:
                    if value == student_recent_data[key]:
                        match_intakeUpdate.append(student.student_id)
             students = students.filter(student_id__in=match_intakeUpdate)
         #  -----end filter field that have in intakeupdate----

        #-----filter classroom and grade-----
         if classroom != '':
             students = students.filter(Q(classroomenrollment__classroom_id=classroom) & Q(Q(classroomenrollment__drop_date__gt=date.today().isoformat()) | Q(classroomenrollment__drop_date=None)))
         if vdp_grade != '':
             student_filter_by_vdp_grade = []
             for student in students:
                 if(student.current_vdp_grade() == int(vdp_grade)):
                     student_filter_by_vdp_grade.append(student.student_id)
             students = students.filter(student_id__in=student_filter_by_vdp_grade)
         #  -----end filter classroom and grede-----
         #  ----- filter public school --------------
         if enrolled != '':
              if grade_current == '' and public_school_name == '' and grade_last == '' and reasons == '':
                 student_filter_by_enrolled = []
                 for student in students:
                      pschool = student.get_pschool()
                      if(pschool.status == enrolled):
                          student_filter_by_enrolled.append(student.student_id)
                 students = students.filter(student_id__in=student_filter_by_enrolled)
              elif grade_current != '' and public_school_name != '':
                   student_filter_by_enrolled = []
                   for student in students:
                        pschool = student.get_pschool()
                        if pschool.status == enrolled and pschool.grade == int(grade_current) and re.search(public_school_name,pschool.school_name, re.IGNORECASE):
                            student_filter_by_enrolled.append(student.student_id)
                   students = students.filter(student_id__in=student_filter_by_enrolled)
              elif grade_current != '':
                   student_filter_by_enrolled = []
                   for student in students:
                      pschool = student.get_pschool()
                      if pschool.status == enrolled and pschool.grade == int(grade_current):
                          student_filter_by_enrolled.append(student.student_id)
                   students = students.filter(student_id__in=student_filter_by_enrolled)
              elif public_school_name != '':
                   student_filter_by_enrolled = []
                   for student in students:
                        pschool = student.get_pschool()
                        if pschool.status == enrolled and re.search(public_school_name,pschool.school_name, re.IGNORECASE):
                            student_filter_by_enrolled.append(student.student_id)
                   students = students.filter(student_id__in=student_filter_by_enrolled)
              elif grade_last != '' and reasons != '':
                   student_filter_by_enrolled = []
                   for student in students:
                      pschool = student.get_pschool()
                      if pschool.status == enrolled and pschool.last_grade == int(grade_last) and re.search(reasons,pschool.reasons, re.IGNORECASE):
                          student_filter_by_enrolled.append(student.student_id)
                   students = students.filter(student_id__in=student_filter_by_enrolled)
              elif grade_last != '':
                   student_filter_by_enrolled = []
                   for student in students:
                       pschool = student.get_pschool()
                       if pschool.status == enrolled and pschool.last_grade == int(grade_last):
                           student_filter_by_enrolled.append(student.student_id)
                   students = students.filter(student_id__in=student_filter_by_enrolled)
              elif reasons != '':
                   student_filter_by_enrolled = []
                   for student in students:
                       pschool = student.get_pschool()
                       if pschool.status == enrolled and re.search(reasons,pschool.reasons, re.IGNORECASE):
                           student_filter_by_enrolled.append(student.student_id)
                   students = students.filter(student_id__in=student_filter_by_enrolled)

         show_data = 'yes'


      else :
          pass
        #   students = IntakeSurvey.objects.all()
      return render(request, 'mande/advancedreport.html',
                            {
                                'students':students,
                                'schools':schools,
                                'genders':genders,
                                'grades' : grades,
                                'relationships':relationships,
                                'yns':yns,
                                'employments':employments,
                                'classrooms':classrooms,
                                'show_data': show_data,
                                'data_guardian1_profession' :json.dumps(list(set(data_guardian1_profession))),
                                'data_guardian2_profession' :json.dumps(list(set(data_guardian2_profession))),
                                'data_minors_profession' : json.dumps(list(set(data_minors_profession))),
                                'data_minors_training_type' :json.dumps(list(set(data_minors_training_type))),
                                'data_reasons' : json.dumps(list(set(data_reasons))),
                                'data_public_schools':json.dumps(list(set(data_public_schools))),
                                'list_of_fields':sorted(list_of_fields.iteritems())
                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Unapproved Absence With No Comment Report
 - lists all student attendance that unapproved absence with no comment
*****************************************************************************
'''
def unapproved_absence_with_no_comment(request,school_year=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      thisyear = date.today().year
      school_year_list = [thisyear-i for i in range(thisyear-2013)]
      if school_year != None:
        school_year_start_date = str(school_year)+"-08-01"
        school_year_end_date = str(int(school_year)+1)+"-07-31"
        unapproved_absence_no_comments = Attendance.objects.all().filter(attendance__exact="UA").filter(Q(Q(notes=u"") |Q(notes=None)) & Q(Q(date__gte=school_year_start_date) & Q(date__lte=school_year_end_date))).order_by('-date')
      else:
          unapproved_absence_no_comments = Attendance.objects.all().filter(attendance__exact="UA").filter(Q(Q(notes=u"") |Q(notes=None))).order_by('-date')
      return render(request, 'mande/unapproved_absence_with_no_comment.html',
                            {
                                'unapproved_absence_no_comments':unapproved_absence_no_comments,
                                'school_year_list':school_year_list,
                                'school_year':school_year
                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Absence With Special Comment Report
 - lists all student absence attendance with special no comment
*****************************************************************************
'''
def absence_with_special_comment(request,school_year=0):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      enrolled_students = getEnrolledStudents()
      comment_list = [
        'ទាក់ទងមិនទាន់បាន',
        'បានផ្តល់ព៍មានដល់សហគមន',
        'បានបញ្ជូនឲ្យសហគមន៍ទាក់ទង',
        'អ្នកសហគមន៍នឹងចុះទៅផ្ទះនៅថ្ងៃស្អែក',
        'លេខទាក់ទងមិនកើតឲ្យគ្រូសហគមន៍ចុះសួរសុខទុក្ខ',
        'តេទៅមិនបានទទួលនឹងអោយគ្រូសហគម៍ចុះទៅស្អែក',
        'អ្នកសហគមន៏និងចុះទៅផ្ទះ',
        'អត់ដឹងដំណឹង ទាក់ទងមិនចូល',
        'ទាក់ទងមិនបាន',
        'កំពុងទាក់ទង',
        'អ្នកសហគមន៍ចុះទៅផ្ទះមិនឃើញមាននរណានៅផ្ទះ',
        'ទូសព្ទត្រូវបានបិត',
        'តេទៅលេខម៉ាក់ចូលអត់លើកខ្ញុុំនឹងប្រាប់អ្នកសហគមន៍',
        'សហគមន៏ទាក់ទង់',
        'ទាក់ទងមិនទាន់បាន',
        'គ្រូតេ',
        'អត់មានច្បាប់',
        'បានផ្តល់ព័ន៍មានដល់ខាងសហគមន៍',
        'ខលមិនចូល',
        'តេទៅគ្មានអ្នកទទួល',
        'តេទៅបិទទូរសព្ទ',
        'លេខទូរសព្ទត្រូវបានបិទ',
        'តេទៅគ្មានអ្នកទទួល',
        'គ្មានលេខទូរសព្ទ',
        'លេខទូរសព្ទទាក់ទងមិនបានហើយលោកគ្រូសហគមន៏កំពុងជប់រវល់follow upជាមួយអាណាព្យាបាលសិស្សមិនអាចទៅផ្ទះគាត់បាន',
        'តេទាក់ទងមិនបាន',
        'ទំនាក់ទំនងមិនបាន',
        'អ្នកសហគមន៍នឹងចុះទៅផ្ទះនៅថ្ងៃស្អែក',
        'តេចូលអត់អ្នកលើក',
        'លេខទូរសព្ទទាក់ទងអត់បាន',
        'ឈឺ',
        'ឈប់រៀន',
        'រវល់',
        'ឈប់មានច្បាប់​',
        'តេទៅគ្មានអ្នកលើក',
        'មិនទាន់មានលេខទំនាក់ទំនង',
        'អត់ដែលឃើញមករៀនទេ',
        'នៅផ្ទះ',
        'ទាក់ទងអត់បាន',
        'ខលចូលតែមិនលើក',
        'រវល់នៅផ្ទះគាត់',
        'លោកគ្រូសហគមន៍នឹងទៅដល់ផ្ទះ',
        'លេខតេមិនចូល',
        'សហគមន៍នឹងចុះទៅ',
        'ថ្ងៃស្អែកលោកគ្រូសហគមន៍ទាក់ទងទៅគាត់',
        'តេអត់លើក',
        'ទាក់ទងអត់ចូល',
        'គ្រូកំពុងទាក់ទង',
        'ទូរស័ព្ទគ្មានក្នុងប្រព័ន្ធ',
        'ទាក់ទងអត់បាន',
        'អត់មានលេខទំនាក់ទំនង',
        'សុំឈប់រៀន',
        'គ្រូទាក់ទងមិនចូល',
        'គ្រូសហគមន៍នឹងចុះទៅ',
        'គ្រូសហគមន៍ចុះទៅជួប',
        'សុំច្បាប់ឈឺ',
        'សូមលោកគ្រូសហគមន៍ចុះទៅ',
        'តេទំនាក់ទំនងឣត់ចូល',
        'សិស្សសុំច្បាប់ឈឺ'
      ]
      #school year = 1 (ALL), school year = 0 (Currret school year)
      # get current school year
      school_year = int(school_year)
      if school_year == 0:
          today = date.today()
          if (today < datetime.strptime(str(today.year)+"-08-01", "%Y-%m-%d").date()):
              school_year = today.year - 1
          else:
              school_year = today.year

      thisyear = date.today().year
      school_year_list = [thisyear-i for i in range(thisyear-2013)]

      if school_year != 1:
        school_year_start_date = str(school_year)+"-08-01"
        school_year_end_date = str(school_year+1)+"-07-31"
        absence_with_special_comment = Attendance.objects.all().filter(
              Q(
              Q(attendance__exact="UA") |Q(attendance__exact="AA")) &
              Q(notes__in=comment_list) &
              Q(Q(date__gte=school_year_start_date) & Q(date__lte=school_year_end_date)) &
              Q(student_id__in=enrolled_students)
              ).order_by('-date')
      else:

        absence_with_special_comment = Attendance.objects.all().filter(
              Q(Q(attendance__exact="UA") |Q(attendance__exact="AA")) &
              Q(notes__in=comment_list) &
              Q(student_id__in=enrolled_students)
              ).order_by('-date')



      return render(request, 'mande/absence_with_special_comment.html',
                            {
                                'absence_with_special_comment':absence_with_special_comment,
                                'school_year_list':school_year_list,
                                'school_year':school_year,
                                'schools': School.objects.filter(active=True),
                            })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Sync current enroll student to  CurrentStudentInfo table
*****************************************************************************
'''
def generate(request):
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
    #    delete all CurrentStudentInfo
       CurrentStudentInfo.objects.all().delete()
       ''' enrolled students are those who have:
             - completed an intake survey
             - have completed an internal intake
             AND
                 - do not have an exit survey
                 OR
                 - have an exit survey with an exit date after today

       '''
       exit_surveys = ExitSurvey.objects.all().filter(
                       exit_date__lte=date.today().isoformat()
                       ).values_list('student_id',flat=True)
       active_surveys = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                                ).exclude(student_id__in=exit_surveys)
       for survey in active_surveys:
            recent_survey = survey.getRecentFields()

            student = CurrentStudentInfo(
                student_id=recent_survey['student_id'],
                name = recent_survey['name'],
                site = recent_survey['site'],
                date = recent_survey['date'],
                dob = recent_survey['dob'],
                gender = recent_survey['gender'],
                age_appropriate_grade = survey.age_appropriate_grade(),
                in_public_school = True if survey.get_pschool().status=='Y' else False,
                at_grade_level = studentAtAgeAppropriateGradeLevel(recent_survey['student_id']),
                vdp_grade = survey.current_vdp_grade()
            )
            student.save()
    #    return HttpResponseRedirect(reverse('student_list'))
       return render(request, 'mande/generatestudentinfo.html',
                              {
                               'students':active_surveys.count()
                              })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Spiritual Survey Report
 - List of Spiritual activities surveys
*****************************************************************************
'''
def spiritual_survey_report(request,academic_year=0,number=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        exit_surveys = ExitSurvey.objects.all().filter(
                      exit_date__lte=date.today().isoformat()
                      ).values_list('student_id',flat=True)
        active_students = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                               ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
        spiritual_surveys = SpiritualActivitiesSurvey.objects.filter(student_id__student_id__in=active_students)

        if int(academic_year) != 0:
            academic_year_start = str(academic_year)+"-08-01"
            academic_year_end = str(int(academic_year)+1)+"-07-31"
            spiritual_surveys = spiritual_surveys.filter( Q(date__gte=academic_year_start) & Q(date__lte=academic_year_end) )

        if number:
            students_match_number = spiritual_surveys.values('student_id_id').annotate(count=Count('student_id_id')).filter(count=number).values_list('student_id_id',flat=True)
            spiritual_surveys = spiritual_surveys.filter(student_id__student_id__in=students_match_number)

        academic_year_start = datetime.now().year-2013
        academic_years = []
        # generate list of year
        for r in range(academic_year_start):
            academic_years.append(datetime.now().year-r)

        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]=_('English')
        grades[70]=_('Vietnamese')
        return render(request, 'mande/spiritual_survey_report.html',
                            {
                                'spiritual_surveys' : spiritual_surveys,
                                'grades' : grades,
                                'sites' : School.objects.filter(active=True),
                                'academic_years':academic_years,
                                'academic_year': academic_year,
                                'number' : number

                            })
    else:
        raise PermissionDenied

'''
*****************************************************************************
Achievement Test Report
 - lists all Achievement test and compare with previous test
*****************************************************************************
'''
def achievement_test(request,current_date=None,previous_date=None,previous_date2=None,site=0,grade=0):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        test_date_lists =  Academic.objects.all().exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                            ).values_list('test_date', flat=True).distinct().order_by('-test_date')
        if current_date is None and previous_date is None and previous_date2 is None:
           if len(test_date_lists) > 2:
               current_date = test_date_lists[0].isoformat()
               previous_date = test_date_lists[1].isoformat()
               previous_date2 = test_date_lists[2].isoformat()
           else:
                try:
                    current_date = test_date_lists[0].isoformat()
                except:
                    current_date = date.today().isoformat()
                try:
                    previous_date = test_date_lists[1].isoformat()
                except:
                    previous_date = date.today().isoformat()
                previous_date2 = date.today().isoformat()

        grade = int(grade)
        site = int(site)

        if site !=0 and grade == 0:
            achievement_tests =  Academic.objects.all().filter(test_date=current_date,student_id__site=site).exclude(
                                                      test_grade_math=None,
                                                      test_grade_khmer=None,
                                                    )
        elif site == 0 and grade !=0:
            achievement_tests =  Academic.objects.all().filter(test_date=current_date,test_level=grade).exclude(
                                                      test_grade_math=None,
                                                      test_grade_khmer=None,
                                                    )
        elif site != 0 and grade != 0:
            achievement_tests =  Academic.objects.all().filter(test_date=current_date,test_level=grade,student_id__site=site).exclude(
                                                      test_grade_math=None,
                                                      test_grade_khmer=None,
                                                    )
        else:
            achievement_tests =  Academic.objects.all().filter(test_date=current_date).exclude(
                                                      test_grade_math=None,
                                                      test_grade_khmer=None,
                                                )
        students = achievement_tests.count()
        passed = achievement_tests.filter(promote=True).count()

        student_achievements = {}
        for achievement_test in achievement_tests:
            student_achievements[achievement_test] = {}
            try:
                student_achievements[achievement_test]['previous'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date)
            except Exception as e:
                student_achievements[achievement_test]['previous'] = None

            try:
                student_achievements[achievement_test]['previous2'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date2)
            except Exception as e:
                student_achievements[achievement_test]['previous2'] = None

        return render(request, 'mande/achievementtestreport.html',
                                {
                                    'student_achievements':student_achievements,
                                    'students' : students,
                                    'passed' : passed,
                                    'current_date':current_date,
                                    'previous_date':previous_date,
                                    'previous_date2':previous_date2,
                                    'site':site,
                                    'grade':grade,
                                    'sites' : School.objects.filter(active=True),
                                    'grades' : OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]),
                                    'test_date_lists' : test_date_lists
                                })
    else:
      raise PermissionDenied

'''
*****************************************************************************
Disease Survey Report
 - List of Spiritual activities surveys
*****************************************************************************
'''
def student_disease_report(request,start_date=None,end_date=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        
     
        exit_surveys = ExitSurvey.objects.all().filter(
                      exit_date__lte=date.today().isoformat()
                      ).values_list('student_id',flat=True)
        active_students = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                               ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
        student_diseases = Diseases.objects.filter(student_id__student_id__in=active_students)
        
        
        if request.method == 'POST' and request.POST['start_date']!=None and request.POST['end_date']!=None:
           try:
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            student_diseases = Diseases.objects.filter(Q(student_id__student_id__in=active_students) & Q(Q(survey_date__gte=start_date) & Q(survey_date__lte=end_date)) )          
           except Exception as e:
            student_diseases = Diseases.objects.filter(student_id__student_id__in=active_students)
              
        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]=_('English')
        grades[70]=_('Vietnamese')
        #return HttpResponse('Hello!')
        return render(request, 'mande/student_disease_report.html',
                            {
                                'student_diseases' : student_diseases,
                                'grades' : grades,
                                'sites' : School.objects.filter(active=True),
                                'start_date':start_date,
                                'end_date': end_date,
                                #'number' : number

                            })
      
      
    else:
        raise PermissionDenied



'''
*****************************************************************************
Vaccine Survey Report
 - List of Spiritual activities surveys
*****************************************************************************
'''
def student_vaccine_report(request,start_date=None,end_date=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
        
     
        exit_surveys = ExitSurvey.objects.all().filter(
                      exit_date__lte=date.today().isoformat()
                      ).values_list('student_id',flat=True)
        active_students = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                               ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
        vaccine_surveys = Vaccines.objects.filter(student_id__student_id__in=active_students)
        
        
        if request.method == 'POST' and request.POST['start_date']!=None and request.POST['end_date']!=None:
           try:
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            vaccine_surveys = Vaccines.objects.filter(Q(student_id__student_id__in=active_students) & Q(Q(survey_date__gte=start_date) & Q(survey_date__lte=end_date)) )          
           except Exception as e:
            vaccine_surveys = Vaccines.objects.filter(student_id__student_id__in=active_students)
              
       
        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]=_('English')
        grades[70]=_('Vietnamese')
        
        return render(request, 'mande/student_vaccine_report.html',
                            {
                                'vaccine_surveys' : vaccine_surveys,
                                'grades' : grades,
                                'sites' : School.objects.filter(active=True),
                                'start_date':start_date,
                                'end_date': end_date,
                                #'number' : number

                            })
      
      
    else:
        raise PermissionDenied
def student_enrollment_report(request,start_date=None,end_date=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):        

        exit_surveys = ExitSurvey.objects.all().filter(exit_date__lte=date.today().isoformat()).values_list('student_id',flat=True)        
        surveys = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat()).order_by('student_id').exclude(student_id__in=exit_surveys).order_by('student_id')
        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]=_('English')
        grades[70]=_('Vietnamese')    

        return render(request, 'mande/student_enrollment_report.html',
                            {
                                'surveys' : surveys,
                                'grades' : grades,
                                'sites' : School.objects.filter(active=True),
                                'start_date':start_date,
                                'end_date': end_date,
                                #'number' : number

                            })
      
      
    else:
        raise PermissionDenied


def dictfetchall(cursor):
  # Get all the row data from the cursor and convert it into a dictionary 
  columns = [col[0] for col in cursor.description]
  return [
    dict(zip(columns, row))
    for row in cursor.fetchall()
  ]
  
'''
*****************************************************************************
chatchup_student_academic_history_report
    method_name = inspect.currentframe().f_code.co_name
 - List of students test
*****************************************************************************
'''
def catchup_academic_history_report(request,start_date=None,end_date=None):
    if request.is_ajax():
      cursor = connection.cursor()
      if request.GET['min'] !="" and request.GET['max'] != "":
        min = request.GET['min']
        max = request.GET['max']                
        cursor.execute("select mande_school.school_name, mande_intakesurvey.site_id, mande_academic.test_level,mande_academic.test_grade_math, mande_academic.test_grade_khmer,mande_academic.activity_test,mande_academic.quiz,mande_academic.monthly_test, mande_academic.middle_test,mande_academic.final_test, mande_academic.test_date, mande_intakesurvey.student_id,mande_intakesurvey.name, mande_intakesurvey.gender, mande_academic.promote from mande_intakesurvey, mande_academic, mande_school where mande_intakesurvey.student_id = mande_academic.student_id_id and mande_intakesurvey.site_id = mande_school.school_id and ( ((mande_academic.test_grade_math or mande_academic.test_grade_khmer) is not null) or (mande_academic.activity_test or mande_academic.quiz or mande_academic.monthly_test or mande_academic.middle_test or mande_academic.final_test ) is not null) and mande_academic.test_date >= %s and mande_academic.test_date <= %s  order by mande_intakesurvey.student_id",(min, max))        
        return JsonResponse(dictfetchall(cursor), safe=False) 
       
      cursor.execute("select mande_school.school_name, mande_intakesurvey.site_id, mande_academic.test_level,mande_academic.test_grade_math, mande_academic.test_grade_khmer,mande_academic.activity_test,mande_academic.quiz,mande_academic.monthly_test, mande_academic.middle_test,mande_academic.final_test, mande_academic.test_date, mande_intakesurvey.student_id,mande_intakesurvey.name, mande_intakesurvey.gender, mande_academic.promote from mande_intakesurvey, mande_academic, mande_school where mande_intakesurvey.student_id = mande_academic.student_id_id and mande_intakesurvey.site_id = mande_school.school_id and ( ((mande_academic.test_grade_math or mande_academic.test_grade_khmer) is not null) or (mande_academic.activity_test or mande_academic.quiz or mande_academic.monthly_test or mande_academic.middle_test or mande_academic.final_test ) is not null)")
      return JsonResponse(dictfetchall(cursor), safe=False)
      
          
    #get current method name
    method_name = inspect.currentframe().f_code.co_name        
    if user_permissions(method_name,request.user):            
        cursor = connection.cursor()              
        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]=_('English')
        grades[70]=_('Vietnamese')
        
        return render(request, 'mande/catchup_academic_history_report.html',
                            {
                                'grades' : grades,
                                'sites' : School.objects.filter(active=True),
                                'start_date':start_date,
                                'end_date': end_date,                                       
                                #'ac': ac,
                            })
      
      
    else:
        raise PermissionDenied


'''
*****************************************************************************
English_student_academic_history_report
    method_name = inspect.currentframe().f_code.co_name
 - List of students test
*****************************************************************************
'''
def english_academic_history_report(request,start_date=None,end_date=None):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name            
    if user_permissions(method_name,request.user):             
        cursor = connection.cursor()                       
        if request.method == 'POST' and request.POST['start_date']!=None and request.POST['end_date']!=None:
           try:           
            start_date = request.POST['start_date']
            end_date = request.POST['end_date']
            if start_date=="" or end_date=="":                                             
               cursor.execute("select mande_school.school_name, mande_intakesurvey.site_id, mande_englishtest.test_level, mande_englishtest.test_date,mande_intakesurvey.student_id,mande_intakesurvey.name, mande_intakesurvey.gender, mande_englishtest.promote from mande_intakesurvey, mande_englishtest, mande_school where mande_intakesurvey.student_id = mande_englishtest.student_id_id and mande_intakesurvey.site_id = mande_school.school_id and NOT ( mande_englishtest.middle_test IS NULL AND mande_englishtest.quiz IS NULL AND mande_englishtest.final_test IS NULL AND mande_englishtest.monthly_test IS NULL AND mande_englishtest.activity_test IS NULL ) and mande_intakesurvey.student_id not in (select student_id_id from mande_exitsurvey) order by mande_intakesurvey.student_id, mande_englishtest.test_date")
               ac = dictfetchall(cursor)                              
            else:
               cursor.execute("select mande_school.school_name, mande_intakesurvey.site_id, mande_englishtest.test_level, mande_englishtest.test_date,mande_intakesurvey.student_id,mande_intakesurvey.name, mande_intakesurvey.gender, mande_englishtest.promote from mande_intakesurvey, mande_englishtest, mande_school where mande_intakesurvey.student_id = mande_englishtest.student_id_id and mande_intakesurvey.site_id = mande_school.school_id and NOT ( mande_englishtest.middle_test IS NULL AND mande_englishtest.quiz IS NULL AND mande_englishtest.final_test IS NULL AND mande_englishtest.monthly_test IS NULL AND mande_englishtest.activity_test IS NULL ) and mande_intakesurvey.student_id not in (select student_id_id from mande_exitsurvey)  and mande_englishtest.test_date >= %s and mande_englishtest.test_date <= %s  order by mande_intakesurvey.student_id",(start_date, end_date))
               ac = dictfetchall(cursor)               
           except Exception as e:                      
            cursor.execute("select mande_school.school_name, mande_intakesurvey.site_id, mande_englishtest.test_level, mande_englishtest.test_date,mande_intakesurvey.student_id,mande_intakesurvey.name, mande_intakesurvey.gender, mande_englishtest.promote from mande_intakesurvey, mande_englishtest, mande_school where mande_intakesurvey.student_id = mande_englishtest.student_id_id and mande_intakesurvey.site_id = mande_school.school_id and NOT ( mande_englishtest.middle_test IS NULL AND mande_englishtest.quiz IS NULL AND mande_englishtest.final_test IS NULL AND mande_englishtest.monthly_test IS NULL AND mande_englishtest.activity_test IS NULL ) and mande_intakesurvey.student_id not in (select student_id_id from mande_exitsurvey) order by mande_intakesurvey.student_id, mande_englishtest.test_date")
            ac = dictfetchall(cursor)            
        else:            
            cursor.execute("select mande_school.school_name, mande_intakesurvey.site_id, mande_englishtest.test_level, mande_englishtest.test_date,mande_intakesurvey.student_id,mande_intakesurvey.name, mande_intakesurvey.gender, mande_englishtest.promote from mande_intakesurvey, mande_englishtest, mande_school where mande_intakesurvey.student_id = mande_englishtest.student_id_id and mande_intakesurvey.site_id = mande_school.school_id and NOT ( mande_englishtest.middle_test IS NULL AND mande_englishtest.quiz IS NULL AND mande_englishtest.final_test IS NULL AND mande_englishtest.monthly_test IS NULL AND mande_englishtest.activity_test IS NULL ) and mande_intakesurvey.student_id not in (select student_id_id from mande_exitsurvey) order by mande_intakesurvey.student_id, mande_englishtest.test_date")
            ac = dictfetchall(cursor)             
                                     
        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]=_('English')
        grades[70]=_('Vietnamese')
        
        return render(request, 'mande/english_academic_history_report.html',
                            {
                                'grades' : grades,
                                'sites' : School.objects.filter(active=True),
                                'start_date':start_date,
                                'end_date': end_date,                                       
                                'ac': ac,
                            })
      
      
    else:
        raise PermissionDenied
'''
*****************************************************************************
Students over come catchup with public school
 - lists all students has been over come catchup 
*****************************************************************************
'''
def followup_vdp_ps_studentgrade_report(request,filter_seach='CURRENT',year=0,school=0,grade=0,classroom=0):
    #get current method name    
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      sites = School.objects.filter(active=True)
      academic_year_start = datetime.now().year-2013
      academic_years = []
      # generate list of year
      for r in range(academic_year_start):
        academic_years.append(datetime.now().year-r)

      if filter_seach=='ALL':
        students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())

      elif filter_seach != 'ALL':
        exit_surveys = ExitSurvey.objects.filter(exit_date__lte=date.today().isoformat()).values_list('student_id',flat=True)
        students = IntakeSurvey.objects.exclude(student_id__in=exit_surveys).filter(date__lte=date.today().isoformat())
        enrolled_catch_up = ClassroomEnrollment.objects.all().filter( Q( Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)) & Q(Q(Q(classroom_id__cohort__gte=1) & Q(classroom_id__cohort__lte=6)) | Q(Q(classroom_id__cohort__gte=-3) & Q(classroom_id__cohort__lte=-1))) ) ).values_list('student_id',flat=True)
        students = students.filter(student_id__in=enrolled_catch_up)

      if int(classroom) != 0:
        enrolled_class = ClassroomEnrollment.objects.filter(Q(Q(classroom_id=int(classroom)) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))).values_list('student_id',flat=True)
        students = students.filter(student_id__in=enrolled_class)
      elif int(school) !=0 and int(grade) !=0:
        enrolled_class = ClassroomEnrollment.objects.filter(Q(Q(classroom_id__school_id=int(school)) & Q(classroom_id__cohort=int(grade)) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))).values_list('student_id',flat=True)
        students = students.filter(student_id__in=enrolled_class)
      elif int(classroom) == 0 and int(school) != 0 and int(grade) == 0:
        students = students.filter(site=school)


      #students_promoted = {}
      students_followup_ps = {}
      if int(year) != 0:
          school_year_start_date = str(year)+"-08-01"
          school_year_end_date = str(int(year)+1)+"-07-31"
          for student in students:
              current_grade = student.current_vdp_grade()
              #if the student has a valid current grade
              if current_grade < 50:
                    try:
                       intake_date = student.intakeinternal_set.all().filter().order_by(
                                    '-enrollment_date'
                                )[0].enrollment_date
                    except IndexError:
                        intake_date = None
                    #if PublicSchoolHistory.objects.filter(student_id=student,grade=current_grade).latest('enroll_date'):
                    try:
                      current_public_grade=PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').grade
                      if current_grade==current_public_grade and current_public_grade!=None and current_public_grade!='N/A':
                        students_followup_ps[student] = {                                                               
                        'followup_ps_status': 'MeetPS',
                        'ps_grade': PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').grade  ,
                        'enrolled_date' : intake_date ,
                        'ps_enrolled_date' : PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').enroll_date
                      }  
                      
                      elif  current_grade>current_public_grade and current_public_grade!=None and current_public_grade!='N/A':
                          
                        students_followup_ps[student] = {                                                               
                        'followup_ps_status': 'OverPS',
                        'ps_grade': PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').grade  ,
                        'enrolled_date' : intake_date ,
                        'ps_enrolled_date' : PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').enroll_date
                      }  
                      else:
                        students_followup_ps[student] = {                                                               
                        'promoted_times': 'NoCatchup',
                        'ps_grade': PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').grade  ,
                        'enrolled_date' : intake_date ,
                        'ps_enrolled_date' : PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').enroll_date
                        }                                 
                    except ObjectDoesNotExist:
                      #students_promoted[student] = None
                      #pass
                        students_followup_ps[student] = {                                                               
                        'promoted_times': 'NoPS',
                        'ps_grade': None,
                        'enrolled_date' : intake_date ,
                        'ps_enrolled_date' : None
                        }  
                    
      else:
          for student in students:
              current_grade = student.current_vdp_grade()
              #if the student current grade is in catch-up
              if current_grade < 50:
                    try:
                       intake_date = student.intakeinternal_set.all().filter().order_by(
                                    '-enrollment_date'
                                )[0].enrollment_date
                    except IndexError:
                        intake_date = None
                    
                    try:
                      current_public_grade=PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').grade
                      if current_grade==current_public_grade and current_public_grade!=None and current_public_grade!='N/A':
                        students_followup_ps[student] = {                                                               
                        'followup_ps_status': 'MeetPS',
                        'ps_grade': PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').grade  ,
                        'enrolled_date' : intake_date ,
                        'ps_enrolled_date' : PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').enroll_date
                        }
                      elif current_grade>current_public_grade and current_public_grade!=None and current_public_grade!='N/A':
                       
                        students_followup_ps[student] = {                                                               
                        'followup_ps_status': 'OverPS',
                        'ps_grade': PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').grade  ,
                        'enrolled_date' : intake_date ,
                        'ps_enrolled_date' : PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').enroll_date
                        }
                      
                      else:
                        students_followup_ps[student] = {                                                               
                        'followup_ps_status': 'Catchup',
                        'ps_grade': PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').grade  ,
                        'enrolled_date' : intake_date ,
                        'ps_enrolled_date' : PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').enroll_date
                        }
                    except ObjectDoesNotExist:
                      #students_promoted[student] = None
                      #pass
                      students_followup_ps[student] = {                                                               
                        'followup_ps_status': 'NoPS',
                        'ps_grade': None,
                        'enrolled_date' : intake_date ,
                        'ps_enrolled_date' : None
                      }                                           
                       
      return render(request, 'mande/followup_vdp_ps_studentgrade_report.html',
                            {
                                'students_followup_ps':students_followup_ps,
                                'filter_seach':filter_seach,
                                'sites':sites,
                                'grades':OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]),
                                'academic_years':academic_years,
                                'year':year,
                                'school':school,
                                'grade':grade,
                                'classroom':classroom

                            })
    else:
      raise PermissionDenied


'''
*****************************************************************************
Students meet and over public school Report
 - Summary of student meet and over catchup 
*****************************************************************************
'''
def vdp_catchup_right_level_summary(request):
    #get current method name
    method_name = inspect.currentframe().f_code.co_name
    if user_permissions(method_name,request.user):
      enrolled_students = getEnrolledStudents()      
      schools = School.objects.filter(active=True)
     
      students_meet_psgrade_by_site = {}
      students_all_site = {
        'student_vpdgrade_meet_or_over_psgrade' : {'M':0,'F':0},            
        'students' : {'M':0,'F':0}
      }
      
      all_student_catchup=0
      for school in schools:
        name = school.school_name        
        students_meet_psgrade_by_site[name] = {}
        students_meet_psgrade_by_site[name]['student_vpdgrade_meet_or_over_psgrade'] = {'M':0,'F':0}        
        students_meet_psgrade_by_site[name]['students'] = {'M':0,'F':0}
      
      for student in enrolled_students:
        age_appropriate_grade = student.age_appropriate_grade()
        current_grade = student.current_vdp_grade()
        site = student.site
        gender = student.gender
  
        if current_grade <= 6 and current_grade >= -3:                     
            students_meet_psgrade_by_site[unicode(site)]['students'][gender] +=1                        
            students_all_site['students'][gender] +=1          
            try:
                current_public_grade=PublicSchoolHistory.objects.filter(student_id=student).latest('enroll_date').grade
                if current_grade>=current_public_grade and current_public_grade!=None and current_public_grade!='N/A':
                    students_meet_psgrade_by_site[unicode(site)]['student_vpdgrade_meet_or_over_psgrade'][gender] +=1
                    students_all_site['student_vpdgrade_meet_or_over_psgrade'][gender] +=1
            except ObjectDoesNotExist:
              pass
      
      return render(request, 'mande/vdp_catchup_right_level_summary.html',
                            {
                                
                                'students_meet_psgrade_by_site' : students_meet_psgrade_by_site,
                                'students_all_site' : students_all_site
                            })
    else:
      raise PermissionDenied



