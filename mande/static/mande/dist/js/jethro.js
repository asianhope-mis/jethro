/*!
 * Custom Jethro CSS
 */

//custom sorting for program titles
 $.fn.dataTable.ext.type.order['jethro-pre'] = function ( d ) {
     switch ( d ) {
         case 'Grade 1':     return 1;
         case 'Grade 2':     return 2;
         case 'Grade 3':     return 3;
         case 'Grade 4':     return 4;
         case 'Grade 5':     return 5;
         case 'Grade 6':     return 6;
         case 'Grade 7':     return 7;
         case 'Grade 8':     return 8;
         case 'Grade 9':     return 9;
         case 'Grade 10':    return 10;
         case 'Grade 11':    return 11;
         case 'Grade 12':    return 12;
         case 'English':     return 50;
         case 'Computers':  return 60;
         case 'Vietnamese':  return 70;
     }
     return 0;
 };
 //  popup window
 //----- OPEN
 var $image = $("#photo")
 // student_id or username (50000 or username)

 var studentid_or_username = ''
 var profile_type = ''
function open_popup(img_url,id,type){
    // img_url for user = /student_photos/user_photos/username.jpg
    // img_url for student = /student_photos/50000.jpg
    studentid_or_username = id
    profile_type = type
    $('#popup').fadeIn(350);
    $(".messages_upload").hide();
    var aspectRatio = 2/3
    if (profile_type == 'user'){
        aspectRatio = 2/2
    }
   //  adding webcam when start popup
    Webcam.set({
     width: 320,
     height: 240,
    });
    Webcam.attach('#my_camera');

    //  set image with new image
    var d = new Date();
    $image.attr('src',img_url+'?'+d.getTime());
    //  crop image
    $image.cropper({
     aspectRatio: aspectRatio,
     cropBoxResizable: false,
     crop: function(e) {
       // get cropped image data url, and set src to preview image
       var croppedCanvas = $(this).cropper('getCroppedCanvas', {
             width: 340, // resize the cropped area
             height: 510
           });
       var preview_data_url = croppedCanvas.toDataURL(); // Get the 340 * 510 image.
       $("#preview").attr('src',preview_data_url)
     },
   });
}

 //----- CLOSE popup
function close_popup(){
     $('#popup').fadeOut(350);
     $image.cropper("destroy");
     $("#file").val("");
}
function hide_crop_area(){
   $(".crop_area").hide();
}
//  take photo with Webcam and preview
function take_photo() {
      $(".crop_area").show();
      Webcam.snap( function(data_uri) {
       //  src = data_uri
        $image.cropper("replace", data_uri)
      });
}
// preview upload image before save
function readURL(input) {
     $(".crop_area").show();
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
        //  replace cropper with new upload image
         $image.cropper("replace", e.target.result)
        };
        reader.readAsDataURL(input.files[0]);
    }
}
//  save upload or cropped photo
function post(path, params, method) {
      method = method || "post"; // Set method to post by default if not specified.
      // The rest of this code assumes you are not using a library.
      // It can be made less wordy if you use one.
      var form = document.createElement("form");
      form.setAttribute("method", method);
      form.setAttribute("action", path);

      for(var key in params) {
          if(params.hasOwnProperty(key)) {
              var hiddenField = document.createElement("input");
              hiddenField.setAttribute("type", "hidden");
              hiddenField.setAttribute("name", key);
              hiddenField.setAttribute("value", params[key]);

              form.appendChild(hiddenField);
           }
      }

      document.body.appendChild(form);
      form.submit();
}
function save_photo(url,csrf_token,request_path){
    var img_url = $("#preview").attr("src")
    post(url, {
        'img_url': img_url,
        'student_id':studentid_or_username,
        'csrfmiddlewaretoken':csrf_token,
        'request_path':request_path,
        'profile_type':profile_type
    });
}
// rotate cropping image
function rotate(){
    $image.cropper("rotate",90)
}
function zoom_in(){
    $image.cropper("zoom",0.1)
}
function zoom_out(){
    $image.cropper("zoom",-0.1)
}
