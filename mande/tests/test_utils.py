from django.test import TestCase
from django.test import Client
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import activate, get_language
from django.core.urlresolvers import resolve, reverse
from django.contrib.auth.models import User

from datetime import date

from mande.utils import *
from mande.permissions import perms_required

from mande.models import FREQUENCY
from mande.models import YN
from mande.models import GRADES
from mande.models import RELATIONSHIPS
from mande.models import COHORTS
from mande.models import STATUS

from mande.models import IntakeSurvey
from mande.models import School
from mande.models import ExitSurvey
from mande.models import IntakeInternal
from mande.models import IntakeUpdate


class MandeUtilsTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','intakesurveys.json','exitsurveys.json',
        'intakeinternals.json','intakeupdates.json',
    ]

    def setUp(self):
        self.client = Client()

    def test_get_enrolled_students_grade_id_none(self):
        exit_surveys = ExitSurvey.objects.all().filter(exit_date__lte=date.today().isoformat()).values_list('student_id',flat=True)
        surveys = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat()).order_by('student_id').exclude(student_id__in=exit_surveys)

        enrolled_students = IntakeInternal.objects.all().values_list('student_id',flat=True)
        not_enrolled = surveys.exclude(student_id__in=enrolled_students).values_list('student_id',flat=True)
        enrolled = surveys.exclude(student_id__in=not_enrolled)
        data = enrolled
        self.assertEqual(list(getEnrolledStudents()),list(data))

    def test_get_enrolled_students_grade_id_not_none(self):
        grade_id = 1

        exit_surveys = ExitSurvey.objects.all().filter(exit_date__lte=date.today().isoformat()).values_list('student_id',flat=True)
        surveys = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat()).order_by('student_id').exclude(student_id__in=exit_surveys)

        enrolled_students = IntakeInternal.objects.all().values_list('student_id',flat=True)
        not_enrolled = surveys.exclude(student_id__in=enrolled_students).values_list('student_id',flat=True)
        enrolled = surveys.exclude(student_id__in=not_enrolled)

        in_grade_id = []
        for student in enrolled:
            if getStudentGradebyID(student.student_id) == grade_id:
                in_grade_id.append(student)

        enrolled = in_grade_id
        data = enrolled
        self.assertEqual(list(getEnrolledStudents(grade_id)),list(data))

    def test_get_student_grade_by_id_no_intakeinternal(self):
        intake = IntakeSurvey.objects.get(pk=9)
        self.assertEqual(getStudentGradebyID(intake.student_id),0)


    def test_get_student_grade_by_id_no_intakeupdate(self):
        intake = IntakeSurvey.objects.get(pk=2)
        self.assertEqual(getStudentGradebyID(intake.student_id),1)

    def test_get_student_grade_by_id_with_intakeudpate(self):
        intake = IntakeSurvey.objects.get(pk=2)
        self.assertEqual(getStudentGradebyID(intake.student_id),1)
        current_grade = 2
        intakeupdate = IntakeUpdate.objects.create(
         student_id = IntakeSurvey.objects.get(pk=2),
         current_grade = current_grade,
         date=date.today().isoformat()
        )
        self.assertEqual(getStudentGradebyID(intake.student_id),current_grade)

    def test_get_student_age_appropriate_grade_level_no_intake_survey(self):
        student_id = 5000
        self.assertEqual(getStudentAgeAppropriateGradeLevel(student_id),False)

    def test_get_student_age_appropriate_grade_level_greater_than_12(self):
        student_id =1
        data = 'N/A'
        self.assertEqual(getStudentAgeAppropriateGradeLevel(student_id),data)

    def test_get_student_age_appropriate_grade_level_dob_not_none(self):
        student_id = 2
        survey = IntakeSurvey.objects.get(pk=student_id)

        approximate_age = date.today().year - survey.dob.year
        if date.today().month < 8:
            age_appropriate_grade = approximate_age - 6
        else:
            age_appropriate_grade = approximate_age - 5

        data = age_appropriate_grade
        self.assertEqual(getStudentAgeAppropriateGradeLevel(student_id),data)

    def test_student_at_age_appropriate_grade_level_no_intake_survey(self):
        student_id = 5000
        self.assertEqual(studentAtAgeAppropriateGradeLevel(student_id),False)

    def test_student_at_age_appropriate_grade_level_dob_not_none(self):
        student_id = 2
        survey = IntakeSurvey.objects.get(pk=student_id)

        current_grade = getStudentGradebyID(student_id)
        age_appropriate_grade = getStudentAgeAppropriateGradeLevel(student_id)
        if (current_grade >= age_appropriate_grade) and current_grade is not 0:
            data = True
        else:
            data = False
        self.assertEqual(studentAtAgeAppropriateGradeLevel(student_id),data)

    def test_user_permissions(self):
        method_name = 'dashboard'
        user = User.objects.get(pk=1)
        user_perms = user.get_all_permissions()
        allow_access = True
        if perms_required[method_name] not in user_perms:
           allow_access = False
        data = allow_access
        self.assertEqual(user_permissions(method_name,user),data)

    def test_get_current_academic_year(self):
        start_date="-08-01"
        today = date.today()
        if (today < datetime.strptime(str(today.year)+start_date, "%Y-%m-%d").date()):
            school_year = today.year - 1
        else:
            school_year = today.year
        self.assertEqual(getCurrentAcademicYear(),school_year)
