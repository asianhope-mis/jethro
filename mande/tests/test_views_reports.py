from django.test import TestCase
from django.test import Client

from mande.models import *
from mande.forms import *

from datetime import date,datetime
from django.core.urlresolvers import reverse
from django.utils.translation import activate
from django.db.models import Q,Count

import json
from datetime import timedelta
import re
import operator
from collections import OrderedDict

from mande.utils import studentAtAgeAppropriateGradeLevel
from mande.utils import getEnrolledStudents

activate('en')
class DailyAttendanceReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'attendancelogs.json','attendancedayofferings.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        today = date.today().isoformat()
        attendance_date = today
        url = reverse('daily_attendance_report',kwargs={'attendance_date':attendance_date})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/attendancereport.html')
        classrooms = Classroom.objects.all().filter(active=True)
        classrooms_who_take_attendance = []
        for classroom in classrooms:
            if classroom.getAttendanceDayOfferings(attendance_date):
                classrooms_who_take_attendance.append(classroom)
        classroomattendance = {}
        for classroom in classrooms_who_take_attendance:
          try:
              classroomattendance[classroom] = AttendanceLog.objects.get(
                                                             classroom=classroom,
                                                             date=attendance_date)
          except ObjectDoesNotExist:
              classroomattendance[classroom] = None
        self.assertEqual(list(resp.context['classroomattendance']),list(classroomattendance))
        self.assertEqual(resp.context['attendance_date'],attendance_date)

class StudentAttendanceDetailViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'attendances.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        today = date.today().isoformat()
        student_id = 1
        url = reverse('student_attendance_detail',kwargs={'student_id':student_id})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/student_attendance_detail.html')
        attendances = Attendance.objects.all().filter(student_id=student_id).order_by('-date')
        self.assertEqual(list(resp.context['attendances']),list(attendances))
        self.assertEqual(resp.context['student_id'],str(student_id))
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)

    def test_context_post(self):
        today = date.today().isoformat()
        start_date = today
        end_date = today
        data = {
            'start_date' : start_date,
            'end_date' : end_date
        }
        student_id = 1
        url = reverse('student_attendance_detail',kwargs={'student_id':student_id})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/student_attendance_detail.html')
        attendances = Attendance.objects.all().filter(Q(student_id=student_id) & Q(Q(date__gte=start_date) & Q(date__lte=end_date))).order_by('-date')
        self.assertEqual(list(resp.context['attendances']),list(attendances))
        self.assertEqual(resp.context['student_id'],str(student_id))
        self.assertEqual(resp.context['start_date'],start_date)
        self.assertEqual(resp.context['end_date'],end_date)

class AttendanceSummaryReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'attendances.json','exitsurveys.json','classroomenrollment.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        self.intake1 = IntakeSurvey.objects.get(pk=1)
        ClassroomEnrollment.objects.create(
            classroom_id = Classroom.objects.get(pk=2),
            student_id = self.intake1 ,
            enrollment_date = "2014-01-01",
            drop_date = date.today().isoformat()
        )

    def test_context_id_none(self):
        url = reverse('attendance_summary_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/attendance_summary_report.html')
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(list(resp.context['schools']),list(School.objects.filter(active=True)))
        self.assertEqual(list(resp.context['classrooms']),list(Classroom.objects.filter(active=True)))
        self.assertEqual(resp.context['current_selected'],None)
        self.assertEqual(resp.context['select_type'],None)
        self.assertEqual(resp.context['id'],None)
        exit_surveys = ExitSurvey.objects.filter(exit_date__lte=date.today().isoformat()).values_list('student_id',flat=True)
        students = IntakeSurvey.objects.exclude(student_id__in=exit_surveys).filter(date__lte=date.today().isoformat())
        studentattendance = {}
        for stu in students:
            student = stu
            try:
                studentattendance[student] = {
                    'classroom' : ClassroomEnrollment.objects.filter(Q(student_id=student) & Q(Q(drop_date=None) | Q(drop_date__gt=date.today().isoformat()))),
                    'present' : Attendance.objects.filter(student_id=student,attendance='P').count(),
                    'unapproved' : Attendance.objects.filter(student_id=student,attendance='UA').count(),
                    'approved' : Attendance.objects.filter(student_id=student,attendance='AA').count(),
                    'total' : Attendance.objects.filter(student_id=student,attendance='P').count()
                            +Attendance.objects.filter(student_id=student,attendance='UA').count()
                            +Attendance.objects.filter(student_id=student,attendance='AA').count()
                     }
            except ObjectDoesNotExist:
               studentattendance[student] = None
        self.assertItemsEqual(resp.context['studentattendance'],studentattendance)
        self.assertItemsEqual(resp.context['studentattendance'][self.intake1]['classroom'], studentattendance[self.intake1]['classroom'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['present'], studentattendance[self.intake1]['present'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['unapproved'], studentattendance[self.intake1]['unapproved'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['approved'], studentattendance[self.intake1]['approved'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['total'], studentattendance[self.intake1]['total'])

    def test_context_id_not_none(self):
        id = 1
        url = reverse('attendance_summary_report',kwargs={'id':id,'select_type':'other'})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/attendance_summary_report.html')
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(list(resp.context['schools']),list(School.objects.filter(active=True)))
        self.assertEqual(list(resp.context['classrooms']),list(Classroom.objects.filter(active=True)))
        self.assertEqual(resp.context['current_selected'],None)
        self.assertEqual(resp.context['select_type'],'other')
        self.assertEqual(resp.context['id'],str(id))
        exit_surveys = ExitSurvey.objects.filter(exit_date__lte=date.today().isoformat()).values_list('student_id',flat=True)
        students = IntakeSurvey.objects.exclude(student_id__in=exit_surveys).filter(date__lte=date.today().isoformat())
        studentattendance = {}
        for stu in students:
            student = stu.student_id
            try:
                studentattendance[student] = {
                    'classroom' : ClassroomEnrollment.objects.filter(Q(student_id=student) & Q(Q(drop_date=None) | Q(drop_date__gt=date.today().isoformat()))),
                    'present' : Attendance.objects.filter(student_id=student,attendance='P').count(),
                    'unapproved' : Attendance.objects.filter(student_id=student,attendance='UA').count(),
                    'approved' : Attendance.objects.filter(student_id=student,attendance='AA').count(),
                    'total' : Attendance.objects.filter(student_id=student,attendance='P').count()
                            +Attendance.objects.filter(student_id=student,attendance='UA').count()
                            +Attendance.objects.filter(student_id=student,attendance='AA').count()
                     }
            except ObjectDoesNotExist:
               studentattendance[student] = None
        self.assertItemsEqual(resp.context['studentattendance'],studentattendance)
        self.assertItemsEqual(resp.context['studentattendance'][self.intake1.student_id]['classroom'], studentattendance[self.intake1.student_id]['classroom'])
        self.assertEqual(resp.context['studentattendance'][self.intake1.student_id]['present'], studentattendance[self.intake1.student_id]['present'])
        self.assertEqual(resp.context['studentattendance'][self.intake1.student_id]['unapproved'], studentattendance[self.intake1.student_id]['unapproved'])
        self.assertEqual(resp.context['studentattendance'][self.intake1.student_id]['approved'], studentattendance[self.intake1.student_id]['approved'])
        self.assertEqual(resp.context['studentattendance'][self.intake1.student_id]['total'], studentattendance[self.intake1.student_id]['total'])

    def test_context_id_not_none_and_select_type_is_site(self):
        id = 1
        url = reverse('attendance_summary_report',kwargs={'id':id,'select_type':'site'})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/attendance_summary_report.html')
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(list(resp.context['schools']),list(School.objects.filter(active=True)))
        self.assertEqual(list(resp.context['classrooms']),list(Classroom.objects.filter(active=True)))
        self.assertEqual(resp.context['current_selected'],School.objects.get(school_id=id))
        self.assertEqual(resp.context['select_type'],'site')
        self.assertEqual(resp.context['id'],str(id))
        students = ClassroomEnrollment.objects.all().filter(Q(classroom_id__school_id=id) & Q(student_id__date__lte=date.today().isoformat()) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))

        studentattendance = {}
        for stu in students:
            student = stu.student_id
            try:
                studentattendance[student] = {
                    'classroom' : ClassroomEnrollment.objects.filter(Q(student_id=student) & Q(Q(drop_date=None) | Q(drop_date__gt=date.today().isoformat()))),
                    'present' : Attendance.objects.filter(student_id=student,attendance='P').count(),
                    'unapproved' : Attendance.objects.filter(student_id=student,attendance='UA').count(),
                    'approved' : Attendance.objects.filter(student_id=student,attendance='AA').count(),
                    'total' : Attendance.objects.filter(student_id=student,attendance='P').count()
                            +Attendance.objects.filter(student_id=student,attendance='UA').count()
                            +Attendance.objects.filter(student_id=student,attendance='AA').count()
                     }
            except ObjectDoesNotExist:
               studentattendance[student] = None
        self.assertItemsEqual(resp.context['studentattendance'],studentattendance)
        self.assertItemsEqual(resp.context['studentattendance'][self.intake1]['classroom'], studentattendance[self.intake1]['classroom'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['present'], studentattendance[self.intake1]['present'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['unapproved'], studentattendance[self.intake1]['unapproved'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['approved'], studentattendance[self.intake1]['approved'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['total'], studentattendance[self.intake1]['total'])

    def test_context_id_not_none_and_select_type_is_classroom(self):
        id = 1
        url = reverse('attendance_summary_report',kwargs={'id':id,'select_type':'classroom'})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/attendance_summary_report.html')
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(list(resp.context['schools']),list(School.objects.filter(active=True)))
        self.assertEqual(list(resp.context['classrooms']),list(Classroom.objects.filter(active=True)))
        self.assertEqual(resp.context['current_selected'],Classroom.objects.get(classroom_id=id))
        self.assertEqual(resp.context['select_type'],'classroom')
        self.assertEqual(resp.context['id'],str(id))
        students = ClassroomEnrollment.objects.all().filter(Q(classroom_id__classroom_id=id) & Q(student_id__date__lte=date.today().isoformat()) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))

        studentattendance = {}
        for stu in students:
            student = stu.student_id
            try:
                studentattendance[student] = {
                    'classroom' : ClassroomEnrollment.objects.filter(Q(student_id=student) & Q(Q(drop_date=None) | Q(drop_date__gt=date.today().isoformat()))),
                    'present' : Attendance.objects.filter(student_id=student,attendance='P').count(),
                    'unapproved' : Attendance.objects.filter(student_id=student,attendance='UA').count(),
                    'approved' : Attendance.objects.filter(student_id=student,attendance='AA').count(),
                    'total' : Attendance.objects.filter(student_id=student,attendance='P').count()
                            +Attendance.objects.filter(student_id=student,attendance='UA').count()
                            +Attendance.objects.filter(student_id=student,attendance='AA').count()
                     }
            except ObjectDoesNotExist:
               studentattendance[student] = None
        self.assertItemsEqual(resp.context['studentattendance'],studentattendance)
        self.assertItemsEqual(resp.context['studentattendance'][self.intake1]['classroom'], studentattendance[self.intake1]['classroom'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['present'], studentattendance[self.intake1]['present'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['approved'], studentattendance[self.intake1]['approved'])
        self.assertEqual(resp.context['studentattendance'][self.intake1]['total'], studentattendance[self.intake1]['total'])


class DailyAbsenceReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'attendances.json','attendancedayofferings.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context_no_attendance_date(self):
        today = date.today().isoformat()
        attendance_date = today
        attendance_end_date = today
        url = reverse('daily_absence_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/absencereport.html')
        self.assertEqual(resp.context['attendance_date'],today)
        self.assertEqual(resp.context['attendance_end_date'],today)
        self.assertEqual(list(resp.context['schools']),list(School.objects.filter(active=True)))
        self.assertEqual(list(resp.context['classroom_list']),list(Classroom.objects.filter(active=True)))
        self.assertEqual(resp.context['current_site'],None)
        self.assertEqual(resp.context['current_classroom'],None)
        # if site and classrooom is none
        classrooms = Classroom.objects.all().filter(active=True)
        classroomattendance = {}
        for classroom in classrooms:
           try:
               #only displays unexcused absences
               classroomattendance[classroom] = Attendance.objects.filter(
                                                              Q(Q(date__gte=attendance_date) & Q(date__lte=attendance_end_date))
                                                              & Q(classroom=classroom)
                                                           #    & Q(attendance='UA')
                                                              )
           except ObjectDoesNotExist:
               classroomattendance[classroom] = None
        self.assertEqual(list(resp.context['classroomattendance']),list(classroomattendance))

    def test_context_attendance_date_not_none(self):
        today = date.today().isoformat()
        attendance_date = '2017-01-01'
        attendance_end_date = today
        url = reverse('daily_absence_report',kwargs={'attendance_date':attendance_date})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/absencereport.html')
        self.assertEqual(resp.context['attendance_date'],attendance_date)
        self.assertEqual(resp.context['attendance_end_date'],attendance_end_date)
        self.assertEqual(list(resp.context['schools']),list(School.objects.filter(active=True)))
        self.assertEqual(list(resp.context['classroom_list']),list(Classroom.objects.filter(active=True)))
        self.assertEqual(resp.context['current_site'],None)
        self.assertEqual(resp.context['current_classroom'],None)
        # if site and classrooom is none
        classrooms = Classroom.objects.all().filter(active=True)
        classroomattendance = {}
        for classroom in classrooms:
           try:
               #only displays unexcused absences
               classroomattendance[classroom] = Attendance.objects.filter(
                                                              Q(Q(date__gte=attendance_date) & Q(date__lte=attendance_end_date))
                                                              & Q(classroom=classroom)
                                                           #    & Q(attendance='UA')
                                                              )
           except ObjectDoesNotExist:
               classroomattendance[classroom] = None
        self.assertEqual(list(resp.context['classroomattendance']),list(classroomattendance))

    def test_context_attendance_side_not_none_and_classroom_is_none(self):
        today = date.today().isoformat()
        attendance_date = today
        attendance_end_date = today
        classroom_id = None
        site = 1
        data = {
            'classroom':classroom_id,
            'site':site,
            'attendance_date':today,
            'attendance_end_date':attendance_end_date
        }
        url = reverse('daily_absence_report',kwargs={'attendance_date':attendance_date})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/absencereport.html')
        self.assertEqual(resp.context['attendance_date'],attendance_date)
        self.assertEqual(resp.context['attendance_end_date'],attendance_end_date)
        self.assertEqual(list(resp.context['schools']),list(School.objects.filter(active=True)))
        self.assertEqual(list(resp.context['classroom_list']),list(Classroom.objects.filter(active=True)))
        self.assertEqual(resp.context['current_site'], School.objects.get(school_id=site))
        self.assertEqual(resp.context['current_classroom'],None)
        # if site and classrooom is none
        classrooms = Classroom.objects.all().filter(active=True)
        classrooms = classrooms.filter(school_id=site)
        classroomattendance = {}
        for classroom in classrooms:
           try:
               #only displays unexcused absences
               classroomattendance[classroom] = Attendance.objects.filter(
                                                              Q(Q(date__gte=attendance_date) & Q(date__lte=attendance_end_date))
                                                              & Q(classroom=classroom)
                                                           #    & Q(attendance='UA')
                                                              )
           except ObjectDoesNotExist:
               classroomattendance[classroom] = None
        self.assertEqual(list(resp.context['classroomattendance']),list(classroomattendance))

    def test_context_attendance_classroom_id_not_none_and_site_is_none(self):
        today = date.today().isoformat()
        attendance_date = today
        attendance_end_date = today
        classroom_id = 1
        site = None
        data = {
            'classroom':classroom_id,
            'site':site,
            'attendance_date':today,
            'attendance_end_date':attendance_end_date
        }
        url = reverse('daily_absence_report',kwargs={'attendance_date':attendance_date})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/absencereport.html')
        self.assertEqual(resp.context['attendance_date'],attendance_date)
        self.assertEqual(resp.context['attendance_end_date'],attendance_end_date)
        self.assertEqual(list(resp.context['schools']),list(School.objects.filter(active=True)))
        self.assertEqual(list(resp.context['classroom_list']),list(Classroom.objects.filter(active=True)))
        self.assertEqual(resp.context['current_site'],None)
        self.assertEqual(resp.context['current_classroom'],Classroom.objects.get(classroom_id=classroom_id))
        # if site and classrooom is none
        classrooms = Classroom.objects.all().filter(active=True)
        classrooms = classrooms.filter(classroom_id=classroom_id)
        classroomattendance = {}
        for classroom in classrooms:
           try:
               #only displays unexcused absences
               classroomattendance[classroom] = Attendance.objects.filter(
                                                              Q(Q(date__gte=attendance_date) & Q(date__lte=attendance_end_date))
                                                              & Q(classroom=classroom)
                                                           #    & Q(attendance='UA')
                                                              )
           except ObjectDoesNotExist:
               classroomattendance[classroom] = None
        self.assertEqual(list(resp.context['classroomattendance']),list(classroomattendance))

class DataAuditViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'intakeupdates.json','exitsurveys.json','intakeinternals.json',
        'publicschoolhistorys.json','classroomenrollment.json','attendancedayofferings.json',
        'attendances.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        # Incorrect DOB
        self.dob = '1989-01-01'
        intake = IntakeSurvey.objects.get(pk=1)
        intake.dob = self.dob
        intake.save()

        today = date.today()
      #   today = datetime.strptime("2014-08-01", "%Y-%m-%d").date()
        if (today < datetime.strptime(str(today.year)+"-08-01", "%Y-%m-%d").date()):
             school_year = today.year - 1
        else:
             school_year = today.year
        # Has never attended class,Unapproved absence with no comment
        self.attendance_date = str(school_year)+"-08-01"
        self.classroom = Classroom.objects.get(pk=1)
        Attendance.objects.create(
            student_id=IntakeSurvey.objects.get(pk=2),
            classroom=self.classroom,
            attendance='UA',
    		date=self.attendance_date
        )
        # Unapproved abscence with no comment - missing class id
        Attendance.objects.create(
            student_id=IntakeSurvey.objects.get(pk=3),
            classroom=None,
            attendance='UA',
    		date=self.attendance_date
        )

    def test_context(self):
        url = reverse('data_audit',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/data_audit.html')
        self.assertIn('No Public School History',resp.context['filters'])
        self.assertIn('Not enrolled in any classes',resp.context['filters'])
        self.assertIn('Incorrect DOB',resp.context['filters'])
        self.assertIn('Unapproved absence with no comment',resp.context['filters'])
        self.assertIn('Unapproved abscence with no comment - missing class id',resp.context['filters'])
        students = resp.context['students']
        student1 = IntakeSurvey.objects.get(pk=1)
        student2 = IntakeSurvey.objects.get(pk=2)
        student3 = IntakeSurvey.objects.get(pk=3)
        student4 = IntakeSurvey.objects.get(pk=4)
        student5 = IntakeSurvey.objects.get(pk=5)
        student6 = IntakeSurvey.objects.get(pk=6)
        student7 = IntakeSurvey.objects.get(pk=7)
        student8 = IntakeSurvey.objects.get(pk=8)

        text = 'Incorrect DOB'
        age = '(~'+unicode(datetime.now().year-datetime.strptime(self.dob, '%Y-%m-%d').year)+' years old)'
        self.assertIn(
            {'text': text+age, 'resolution': reverse('intake_survey',kwargs={'student_id':student1.student_id}), 'limit': 'dob'},
            students[student1]
        )
        self.assertIn(
            {'text': 'Not enrolled in any classes', 'resolution': reverse('classroomenrollment_form'), 'limit': None}
            ,students[student2]
        )
        self.assertIn(
            {'text': 'Has never attended class', 'resolution': reverse('student_detail',kwargs={'student_id':student2.student_id}), 'limit': None}
            ,students[student2]
        )
        self.assertIn(
            {'text': 'No Public School History', 'resolution': reverse('student_detail',kwargs={'student_id':student2.student_id}), 'limit': None}
            ,students[student2]
        )
        self.assertIn(
            {'text': 'Unapproved absence with no comment', 'resolution': reverse('take_class_attendance',kwargs={'attendance_date':self.attendance_date, 'classroom_id':self.classroom.classroom_id}), 'limit': None}
            ,students[student2]
        )
        self.assertIn(
            {'text': 'Not enrolled in any classes', 'resolution': reverse('classroomenrollment_form'), 'limit': None}
            ,students[student3]
        )
        self.assertIn(
            {'text': 'Has never attended class', 'resolution': reverse('student_detail',kwargs={'student_id':student3.student_id}), 'limit': None}
            ,students[student3]
        )
        self.assertIn(
            {'text': 'No Public School History', 'resolution': reverse('student_detail',kwargs={'student_id':student3.student_id}), 'limit': None}
            ,students[student3]
        )
        self.assertIn(
            {'text': 'Unapproved abscence with no comment - missing class id', 'resolution': '', 'limit': None}
            ,students[student3]
        )
        self.assertIn(
            {'text': 'No Public School History', 'resolution': reverse('student_detail',kwargs={'student_id':student4.student_id}), 'limit': None}
            ,students[student4]
        )
        self.assertIn(
            {'text': 'Not enrolled in any classes', 'resolution': reverse('classroomenrollment_form'), 'limit': None}
            ,students[student4]
        )
        self.assertIn(
            {'text': 'No Public School History', 'resolution': reverse('student_detail',kwargs={'student_id':student5.student_id}), 'limit': None}
            ,students[student5]
        )
        self.assertIn(
            {'text': 'Not enrolled in any classes', 'resolution': reverse('classroomenrollment_form'), 'limit': None}
            ,students[student5]
        )
        self.assertIn(
            {'text': 'No Public School History', 'resolution': reverse('student_detail',kwargs={'student_id':student6.student_id}), 'limit': None}
            ,students[student6]
        )
        self.assertIn(
            {'text': 'Not enrolled in any classes', 'resolution': reverse('classroomenrollment_form'), 'limit': None}
            ,students[student6]
        )
        self.assertIn(
            {'text': 'No Public School History', 'resolution': reverse('student_detail',kwargs={'student_id':student7.student_id}), 'limit': None}
            ,students[student7]
        )
        self.assertIn(
            {'text': 'Not enrolled in any classes', 'resolution': reverse('classroomenrollment_form'), 'limit': None}
            ,students[student7]
        )
        self.assertIn(
            {'text': 'No Public School History', 'resolution': reverse('student_detail',kwargs={'student_id':student8.student_id}), 'limit': None}
            ,students[student8]
        )
        self.assertIn(
            {'text': 'Not enrolled in any classes', 'resolution': reverse('classroomenrollment_form'), 'limit': None}
            ,students[student8]
        )

class ClassListViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'classroomteachers.json','classroomenrollment.json','teachers.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        self.classroom2 = Classroom.objects.get(pk=2)
        ClassroomEnrollment.objects.create(
            classroom_id = self.classroom2,
            student_id = IntakeSurvey.objects.get(pk=1) ,
            enrollment_date = "2014-01-01",
            drop_date = date.today().isoformat()
        )
        today_current = date.today().year
        ClassroomTeacher.objects.create(
            classroom_id=self.classroom2,
            teacher_id=Teacher.objects.get(pk=1),
            year=today_current-1
        )
        ClassroomTeacher.objects.create(
            classroom_id=self.classroom2,
            teacher_id=Teacher.objects.get(pk=1),
            year=today_current
        )
    def test_context(self):
        url = reverse('class_list',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/class_list.html')
        academic_year = getCurrentAcademicYear()
        class_list={}
        classrooms = Classroom.objects.filter(active=True)
        for classroom in classrooms:
          instance = Classroom.objects.get(classroom_id=classroom.pk)
          class_list[classroom]={
              'site':classroom.school_id,
              'target_grade':classroom.cohort,
              'classroom_number':classroom.classroom_number,
              'teacher': 'Not assigned',
              'students': 0,
              'female': 0,
          }
          try:
              class_list[classroom]['teacher'] = ClassroomTeacher.objects.filter(classroom_id=classroom.pk,year=academic_year)
          except ObjectDoesNotExist:
              pass
          try:
              enrolled_students =  instance.classroomenrollment_set.all().filter(Q(student_id__date__lte=date.today().isoformat()) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))
              female_students = 0
              for student in enrolled_students:
                  if student.student_id.gender == 'F':
                      female_students +=1
              class_list[classroom]['students'] = len(enrolled_students)
              class_list[classroom]['female'] = female_students
          except ObjectDoesNotExist:
              pass
        self.assertEqual(list(resp.context['class_list']),list(class_list))
        self.assertEqual(resp.context['class_list'][self.classroom2]['students'],class_list[self.classroom2]['students'])
        self.assertEqual(resp.context['class_list'][self.classroom2]['female'],class_list[self.classroom2]['female'])
        self.assertEqual(list(resp.context['class_list'][self.classroom2]['teacher']),list(class_list[self.classroom2]['teacher']))
        self.assertEqual(resp.context['academic_year'],academic_year)

class ExitSurveysListViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'exitsurveys.json','postexitsurveys.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        url = reverse('exit_surveys_list',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/exitsurveylist.html')
        from_exit_date=(datetime.now()- timedelta(days=2 * 365/12)).strftime("%Y-%m-%d")
        to_exit_date=(datetime.now()).strftime("%Y-%m-%d")
        exit_surveys = ExitSurvey.objects.all().filter(exit_date__gte=from_exit_date , exit_date__lte=to_exit_date)
        post_exit_surveys = PostExitSurvey.objects.all()
        self.assertEqual(resp.context['from_exit_date'],from_exit_date)
        self.assertEqual(resp.context['to_exit_date'],to_exit_date)
        self.assertEqual(list(resp.context['exit_surveys']),list(exit_surveys))
        self.assertEqual(list(resp.context['post_exit_surveys']),list(post_exit_surveys))

    def test_context_post(self):
        url = reverse('exit_surveys_list',kwargs={})
        from_exit_date=date.today().isoformat()
        to_exit_date=date.today().isoformat()
        data = {
            'from_exit_date':from_exit_date,
            'to_exit_date':to_exit_date
        }
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/exitsurveylist.html')

        exit_surveys = ExitSurvey.objects.all().filter(exit_date__gte=from_exit_date , exit_date__lte=to_exit_date)
        post_exit_surveys = PostExitSurvey.objects.all()
        self.assertEqual(resp.context['from_exit_date'],from_exit_date)
        self.assertEqual(resp.context['to_exit_date'],to_exit_date)
        self.assertEqual(list(resp.context['exit_surveys']),list(exit_surveys))
        self.assertEqual(list(resp.context['post_exit_surveys']),list(post_exit_surveys))

class StudentAbsenceReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'attendances.json','exitsurveys.json','intakeinternals.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        url = reverse('student_absence_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/student_absence_report.html')
        attendances = Attendance.objects.all()

        #set up dict of attendance codes with zero values
        default_attendance ={}
        attendancecodes = dict(ATTENDANCE_CODES)
        for key,code in attendancecodes.iteritems():
          default_attendance[key]=0

        #default out all current students
        attendance_by_sid = {}
        currently_enrolled_students = getEnrolledStudents()
        for student in currently_enrolled_students:
          attendance_by_sid[student]=dict(default_attendance)


        for attendance in attendances:
          try:
              attendance_by_sid[attendance.student_id][attendance.attendance] +=1
          except KeyError:
              pass; #students no longer in attendance that have attendance
        self.assertEqual(resp.context['attendance_by_sid'],attendance_by_sid)
        self.assertEqual(resp.context['attendancecodes'],attendancecodes)

class StudentLagReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'attendances.json','exitsurveys.json','intakeinternals.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        url = reverse('student_lag_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/student_lag_report.html')
        view_date=date.today().strftime("%Y-%m-%d")
        enrolled_students = getEnrolledStudents()
        students_lag = {}
        for student in enrolled_students:
          current_vdp_grade = student.current_vdp_grade(view_date)
          #only students in the scope of grade levels
          if current_vdp_grade < 12 and current_vdp_grade >= -3:
              age_appropriate_grade = student.age_appropriate_grade(view_date)
              lag = age_appropriate_grade - current_vdp_grade

              if age_appropriate_grade < 0 and current_vdp_grade > 0:
                  lag = lag + 1
                  print student.student_id
              elif current_vdp_grade < 0 and age_appropriate_grade > 0:
                  lag = lag - 1
              students_lag[student] = {
                      'lag':lag,
                      'appropriate_grade':age_appropriate_grade,
                      'vdp_grade':current_vdp_grade,
                      'date_achieved_age_appropriate_level': student.date_enrolled_grade(current_vdp_grade) if current_vdp_grade - age_appropriate_grade<=0 else student.date_enrolled_grade(current_vdp_grade-(current_vdp_grade - age_appropriate_grade))
              }
        self.assertEqual(resp.context['view_date'],view_date)
        self.assertEqual(resp.context['students_lag'],students_lag)

    def test_context_post(self):
        view_date = date.today().isoformat()
        data = {
            'view_date': view_date
        }
        url = reverse('student_lag_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/student_lag_report.html')
        enrolled_students = getEnrolledStudents()
        students_lag = {}
        for student in enrolled_students:
          current_vdp_grade = student.current_vdp_grade(view_date)
          #only students in the scope of grade levels
          if current_vdp_grade < 12 and current_vdp_grade >= -3:
              age_appropriate_grade = student.age_appropriate_grade(view_date)
              lag = age_appropriate_grade - current_vdp_grade

              if age_appropriate_grade < 0 and current_vdp_grade > 0:
                  lag = lag + 1
                  print student.student_id
              elif current_vdp_grade < 0 and age_appropriate_grade > 0:
                  lag = lag - 1
              students_lag[student] = {
                      'lag':lag,
                      'appropriate_grade':age_appropriate_grade,
                      'vdp_grade':current_vdp_grade,
                      'date_achieved_age_appropriate_level': student.date_enrolled_grade(current_vdp_grade) if current_vdp_grade - age_appropriate_grade<=0 else student.date_enrolled_grade(current_vdp_grade-(current_vdp_grade - age_appropriate_grade))
              }
        self.assertEqual(resp.context['view_date'],view_date)
        self.assertEqual(resp.context['students_lag'],students_lag)

class StudentEvaluationReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'studentevaluations.json','classroomenrollment.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        StudentEvaluation.objects.create(
            student_id = IntakeSurvey.objects.get(pk=1),
            date = date.today().isoformat(),
            faith_score = 100,personal_score = 100,study_score = 100,
            hygiene_score = 100,academic_score = 100,comments = 'good'
        )
        self.classroom2 = Classroom.objects.get(pk=2)
        ClassroomEnrollment.objects.create(
            classroom_id = self.classroom2,
            student_id = IntakeSurvey.objects.get(pk=1) ,
            enrollment_date = "2014-01-01",
            drop_date = date.today().isoformat()
        )

    def test_context_grade_id_none(self):
        url = reverse('student_evaluation_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentevaluationreport.html')
        evaluations = StudentEvaluation.objects.all().exclude(  academic_score=None,
                                                              study_score=None,
                                                              personal_score=None,
                                                              hygiene_score=None,
                                                              faith_score=None)

        self.assertEqual(list(resp.context['evaluations']),list(evaluations))
        self.assertEqual(resp.context['grade_id'],0)
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))

    def test_context_grade_id_not_none(self):
        grade_id = self.classroom2.classroom_id
        url = reverse('student_evaluation_report',kwargs={'grade_id':grade_id})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentevaluationreport.html')

        evaluations = StudentEvaluation.objects.all().exclude(  academic_score=None,
                                                              study_score=None,
                                                              personal_score=None,
                                                              hygiene_score=None,
                                                              faith_score=None)
        enrolled_students = ClassroomEnrollment.objects.all().filter(
                            Q(
                                Q(student_id__date__lte=date.today().isoformat()) &
                                Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                            )
                            &
                            Q(classroom_id__cohort=grade_id)
                        ).values_list('student_id',flat=True)

        evaluations = evaluations.filter(student_id__in=enrolled_students)
        self.assertEqual(list(resp.context['evaluations']),list(evaluations))
        self.assertEqual(resp.context['grade_id'],str(grade_id))
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))

    def test_context_post_grade_id_not_none(self):
        grade_id = self.classroom2.classroom_id
        start_date = date.today().isoformat()
        end_date = date.today().isoformat()
        data = {
            'grade_id':grade_id,
            'start_date':start_date,
            'end_date':end_date
        }
        url = reverse('student_evaluation_report',kwargs=data)
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentevaluationreport.html')
        evaluations = StudentEvaluation.objects.all().filter(
                                    Q(date__gte=start_date) & Q(date__lte=end_date)
                                ).exclude(
                                    academic_score=None,
                                    study_score=None,
                                    personal_score=None,
                                    hygiene_score=None,
                                    faith_score=None
                                )
        enrolled_students = ClassroomEnrollment.objects.all().filter(
                            Q(
                                Q(student_id__date__lte=date.today().isoformat()) &
                                Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                            )
                            &
                            Q(classroom_id__cohort=grade_id)
                        ).values_list('student_id',flat=True)

        evaluations = evaluations.filter(student_id__in=enrolled_students)
        self.assertEqual(list(resp.context['evaluations']),list(evaluations))
        self.assertEqual(resp.context['grade_id'],str(grade_id))
        self.assertEqual(resp.context['start_date'],start_date)
        self.assertEqual(resp.context['end_date'],end_date)
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))

class StudentAchievementTestReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'academic.json','classroomenrollment.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        ClassroomEnrollment.objects.create(
            student_id = IntakeSurvey.objects.get(pk=2),
            classroom_id = Classroom.objects.get(pk=1),
            enrollment_date = date.today().isoformat(),
            drop_date = None
        )
        Academic.objects.create(
            student_id = IntakeSurvey.objects.get(pk=2),
            test_date = date.today().isoformat(),test_level=1,
            test_grade_math = 10,test_grade_khmer=10,
            promote=True
        )
        # for test drop_date not equal today
        ClassroomEnrollment.objects.filter(student_id=IntakeSurvey.objects.get(pk=1)).update(drop_date=date.today().isoformat())

    def test_context_grade_id_none(self):
        url = reverse('student_achievement_test_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentachievementtestreport.html')
        achievement_tests = Academic.objects.all().exclude(
                                                       test_grade_math=None,
                                                       test_grade_khmer=None,
                                                       )
        self.assertEqual(list(resp.context['achievement_tests']),list(achievement_tests))
        self.assertEqual(resp.context['grade_id'],0)
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))

    def test_context_grade_id_not_none(self):
        grade_id = 1
        url = reverse('student_achievement_test_report',kwargs={'grade_id':grade_id})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentachievementtestreport.html')
        achievement_tests = Academic.objects.all().exclude(
                                                       test_grade_math=None,
                                                       test_grade_khmer=None,
                                                       )
        enrolled_students = ClassroomEnrollment.objects.all().filter(
                            Q(
                                Q(student_id__date__lte=date.today().isoformat()) &
                                Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                            )
                            &
                            Q(classroom_id__cohort=grade_id)
                        ).values_list('student_id',flat=True)
        achievement_tests = achievement_tests.filter(student_id__in=enrolled_students)
        self.assertEqual(list(resp.context['achievement_tests']),list(achievement_tests))
        self.assertEqual(resp.context['grade_id'],str(grade_id))
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))

    def test_context_post_grade_id_not_none(self):
        grade_id = 1
        start_date = date.today().isoformat()
        end_date = date.today().isoformat()
        data = {
            'grade_id':grade_id,
            'start_date':start_date,
            'end_date':end_date
        }
        url = reverse('student_achievement_test_report',kwargs=data)
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentachievementtestreport.html')
        achievement_tests = Academic.objects.all().filter(Q(test_date__gte=start_date) & Q(test_date__lte=end_date)).exclude(
                                                        test_grade_math=None,
                                                        test_grade_khmer=None,
                                                        )
        enrolled_students = ClassroomEnrollment.objects.all().filter(
                            Q(
                                Q(student_id__date__lte=date.today().isoformat()) &
                                Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                            )
                            &
                            Q(classroom_id__cohort=grade_id)
                        ).values_list('student_id',flat=True)
        achievement_tests = achievement_tests.filter(student_id__in=enrolled_students)
        self.assertEqual(list(resp.context['achievement_tests']),list(achievement_tests))
        self.assertEqual(resp.context['grade_id'],str(grade_id))
        self.assertEqual(resp.context['start_date'],start_date)
        self.assertEqual(resp.context['end_date'],end_date)
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))

class StudentEnglishTestViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'englishtests.json','classroomenrollment.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        classroom = Classroom.objects.create(
            active = True,
            cohort = 51,
            school_id = School.objects.get(pk=1),
            classroom_number = 'English level 1'
        )
        ClassroomEnrollment.objects.create(
            student_id = IntakeSurvey.objects.get(pk=1),
            classroom_id = classroom,
            enrollment_date = date.today().isoformat(),
            drop_date = None
        )

        EnglishTest.objects.create(
            student_id = IntakeSurvey.objects.get(pk=1),
            test_date = date.today().isoformat(),test_level=51,
            activity_test = 10, quiz=10, monthly_test=10, middle_test=10, final_test=10,
            promote=True
        )

    def test_context_grade_id_none(self):
        url = reverse('student_englishtest',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentenglishtestreport.html')
        english_tests = EnglishTest.objects.all().exclude(
                                                       activity_test=None,
                                                       quiz=None,
                                                       monthly_test=None,
                                                       middle_test=None,
                                                       final_test=None
                                                       )
        self.assertEqual(list(resp.context['english_tests']),list(english_tests))
        self.assertEqual(resp.context['grade_id'],0)
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(resp.context['grades'],OrderedDict(ENGLISH_GRADES))

    def test_context_grade_id_not_none(self):
        grade_id = 51
        url = reverse('student_englishtest',kwargs={'grade_id':grade_id})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentenglishtestreport.html')
        english_tests = EnglishTest.objects.all().exclude(
                                                       activity_test=None,
                                                       quiz=None,
                                                       monthly_test=None,
                                                       middle_test=None,
                                                       final_test=None
                                                       )
        enrolled_students = ClassroomEnrollment.objects.all().filter(
                            Q(
                                Q(student_id__date__lte=date.today().isoformat()) &
                                Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                            )
                            &
                            Q(classroom_id__cohort=grade_id)
                        ).values_list('student_id',flat=True)
        english_tests = english_tests.filter(student_id__in=enrolled_students)
        self.assertEqual(list(resp.context['english_tests']),list(english_tests))
        self.assertEqual(resp.context['grade_id'],str(grade_id))
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)
        self.assertEqual(resp.context['grades'],OrderedDict(ENGLISH_GRADES))

    def test_context_post_grade_id_not_none(self):
        grade_id = 51
        start_date = date.today().isoformat()
        end_date = date.today().isoformat()
        data = {
            'grade_id':grade_id,
            'start_date':start_date,
            'end_date':end_date
        }
        url = reverse('student_englishtest',kwargs=data)
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentenglishtestreport.html')
        english_tests = EnglishTest.objects.all().filter(Q(test_date__gte=start_date) & Q(test_date__lte=end_date)).exclude(
                                                        activity_test=None,
                                                        quiz=None,
                                                        monthly_test=None,
                                                        middle_test=None,
                                                        final_test=None
                                                        )
        enrolled_students = ClassroomEnrollment.objects.all().filter(
                            Q(
                                Q(student_id__date__lte=date.today().isoformat()) &
                                Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None))
                            )
                            &
                            Q(classroom_id__cohort=grade_id)
                        ).values_list('student_id',flat=True)
        english_tests = english_tests.filter(student_id__in=enrolled_students)
        self.assertEqual(list(resp.context['english_tests']),list(english_tests))
        self.assertEqual(resp.context['grade_id'],str(grade_id))
        self.assertEqual(resp.context['start_date'],start_date)
        self.assertEqual(resp.context['end_date'],end_date)
        self.assertEqual(resp.context['grades'],OrderedDict(ENGLISH_GRADES))

class StudentMedicalReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'exitsurveys.json','healths.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        url = reverse('student_medical_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentmedicalreport.html')
        enrolled_students = getEnrolledStudents()
        visits = {}
        for student in enrolled_students:
          try:
              visits[student] = len(Health.objects.all().filter(student_id=student))
          except ObjectDoesNotExist:
              pass
        self.assertEqual(list(resp.context['visits']),list(visits))

class StudenDentalSummaryReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'healths.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context_site_is_none(self):
        url = reverse('student_dental_summary_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentdentalreport.html')
        dentals= Health.objects.all().filter(appointment_type='Dental')
        unique_students = dentals.values('student_id').annotate(dcount=Count('student_id')).count()
        year = datetime.now().year-2013
        dentals_by_month_year=[]
        for x in range(year):
          dentals_by_month_year.extend([{'group_by_date':str(datetime.now().year-x)+'-'+format(i+1, '02d'),'dentals':[], 'unique':0} for i in range(12)])

        for dental in dentals:
          for dental_by_month_year in dentals_by_month_year:
              generate_to_date=datetime.strptime(dental_by_month_year['group_by_date'], '%Y-%m')
              if(generate_to_date.year==dental.appointment_date.year and generate_to_date.month==dental.appointment_date.month):
                  dental_by_month_year['dentals'].append(dental)
                  unique_students_by_month = dentals.filter(appointment_date__year=generate_to_date.year, appointment_date__month=generate_to_date.month).values('student_id').annotate(dcount=Count('student_id')).count()
                  dental_by_month_year['unique'] = unique_students_by_month
        self.assertEqual(list(resp.context['dentals_by_month_year']),list(dentals_by_month_year))
        self.assertEqual(resp.context['unique_students'],unique_students)
        self.assertEqual(resp.context['current_site'],"All Site")
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))

    def test_context_site_not_none(self):
        site_id = 1
        url = reverse('student_dental_summary_report',kwargs={'site_id':site_id})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentdentalreport.html')
        dentals= Health.objects.all().filter(appointment_type='Dental',student_id__site=site_id)
        unique_students = dentals.values('student_id').annotate(dcount=Count('student_id')).count()
        year = datetime.now().year-2013
        dentals_by_month_year=[]
        for x in range(year):
          dentals_by_month_year.extend([{'group_by_date':str(datetime.now().year-x)+'-'+format(i+1, '02d'),'dentals':[], 'unique':0} for i in range(12)])

        for dental in dentals:
          for dental_by_month_year in dentals_by_month_year:
              generate_to_date=datetime.strptime(dental_by_month_year['group_by_date'], '%Y-%m')
              if(generate_to_date.year==dental.appointment_date.year and generate_to_date.month==dental.appointment_date.month):
                  dental_by_month_year['dentals'].append(dental)
                  unique_students_by_month = dentals.filter(appointment_date__year=generate_to_date.year, appointment_date__month=generate_to_date.month).values('student_id').annotate(dcount=Count('student_id')).count()
                  dental_by_month_year['unique'] = unique_students_by_month
        self.assertEqual(list(resp.context['dentals_by_month_year']),list(dentals_by_month_year))
        self.assertEqual(resp.context['unique_students'],unique_students)
        self.assertEqual(resp.context['current_site'],School.objects.get(school_id=site_id))
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))

class StudenDentalReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'healths.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        url = reverse('student_dental_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentdental.html')
        dentals = Health.objects.filter(appointment_type='Dental')
        self.assertEqual(list(resp.context['dentals']),list(dentals))
        self.assertEqual(resp.context['start_date'],None)
        self.assertEqual(resp.context['end_date'],None)

    def test_context_post(self):
        start_date = date.today().isoformat()
        end_date = date.today().isoformat()
        data = {
            'start_date':start_date,
            'end_date':end_date
        }
        url = reverse('student_dental_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/studentdental.html')
        dentals = Health.objects.filter( Q( Q( Q(appointment_date__gte=start_date) & Q(appointment_date__lte=end_date) ) & Q(appointment_type='Dental')) )
        self.assertEqual(list(resp.context['dentals']),list(dentals))
        self.assertEqual(resp.context['start_date'],start_date)
        self.assertEqual(resp.context['end_date'],end_date)

class MandeSummaryReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'exitsurveys.json','classroomenrollment.json','intakeinternals.json',
        'intakeupdates.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        # catch-up + vietnamese
        classroom = Classroom.objects.create(
            active=True,cohort = 71,
            school_id = School.objects.get(pk=3),
            classroom_number = "Vietnamese 1"
        )
        ClassroomEnrollment.objects.create(
            student_id = IntakeSurvey.objects.get(pk=2),
            classroom_id = classroom,
            enrollment_date = date.today().isoformat()
        )
        ClassroomEnrollment.objects.create(
            student_id = IntakeSurvey.objects.get(pk=2),
            classroom_id = Classroom.objects.get(pk=1),
            enrollment_date = date.today().isoformat()
        )

        # test classroom enrollment drop_date not today
        #grade 2 + vietnamese 1
        IntakeUpdate.objects.create(
         student_id = IntakeSurvey.objects.get(pk=3),
         current_grade = 2,
         date=date.today().isoformat()
        )
        ClassroomEnrollment.objects.create(
            student_id = IntakeSurvey.objects.get(pk=3),
            classroom_id = Classroom.objects.get(pk=2),
            enrollment_date = date.today().isoformat(),
        )
        classroom_vietname = Classroom.objects.create(
            active=True,cohort = 71,
            school_id = School.objects.get(pk=2),
            classroom_number = "Vietnamese 1"
        )
        ClassroomEnrollment.objects.create(
            student_id = IntakeSurvey.objects.get(pk=3),
            classroom_id = classroom_vietname,
            enrollment_date = date.today().isoformat(),
            drop_date = date.today().isoformat()
        )

        # vietnamese only
        IntakeUpdate.objects.create(
         student_id = IntakeSurvey.objects.get(pk=1),
         current_grade = 71,
         date=date.today().isoformat()
        )

    def test_context(self):
        url = reverse('mande_summary_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/mandesummaryreport.html')
        view_date = date.today().isoformat()
        start_view_date = date.today().isoformat()

        schools = School.objects.filter(active=True)
        exit_surveys = ExitSurvey.objects.filter(exit_date__lte=start_view_date).values_list('student_id',flat=True)
        students = IntakeSurvey.objects.exclude(student_id__in=exit_surveys).filter(date__lte=view_date)

        students_by_site_grade = {}
        students_enrolled_in_english_by_level = {}
        students_by_site= {}

        # catch up grade level
        grades_level = [item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]
        # english grade level
        english_level = set(Classroom.objects.filter(cohort__gt=50,cohort__lt=60).values_list('cohort',flat=True))

        # generate dict
        for school in schools:
           name = school.school_name
           students_by_site_grade[name] = OrderedDict()
           students_enrolled_in_english_by_level[name] = OrderedDict()
           students_by_site[name] = {'M':0,'F':0}

           for key,value in grades_level:
               students_by_site_grade[name][key] = {'M':{'data':0,'age_appropriate':0},'F':{'data':0,'age_appropriate':0}}
           students_by_site_grade[name]['total'] = {'M':{'data':0,'age_appropriate':0},'F':{'data':0,'age_appropriate':0}}

           for a in english_level:
               students_enrolled_in_english_by_level[name][a] = {'M':0,'F':0}
           students_enrolled_in_english_by_level[name]['total'] = {'M':0,'F':0}

        for student in students:
            grade = student.current_vdp_grade(view_date)
            site = student.site
            gender = student.gender

            # total student
            students_by_site[unicode(site)][gender] +=1

            # english student by level
            try:
                students_enrolled_in_english_by_level[unicode(site)][grade][gender] +=1
                students_enrolled_in_english_by_level[unicode(site)]['total'][gender] +=1
            except KeyError:
                pass

            try:
                students_by_site_grade[unicode(site)][grade][gender]['data'] +=1
                students_by_site_grade[unicode(site)]['total'][gender]['data'] +=1
                #student appropriate level
                if (student.age_appropriate_grade(view_date) - grade < 1):
                    students_by_site_grade[unicode(site)][grade][gender]['age_appropriate'] +=1
                    students_by_site_grade[unicode(site)]['total'][gender]['age_appropriate'] +=1
            except KeyError:
                pass


        self.assertEqual(list(resp.context['schools']),list(schools))
        self.assertEqual(resp.context['students_by_site_grade'],students_by_site_grade)
        self.assertEqual(resp.context['students_by_site'],students_by_site)
        self.assertEqual(resp.context['students_enrolled_in_english_by_level'],students_enrolled_in_english_by_level)
        self.assertEqual(resp.context['grades_level'],grades_level)
        self.assertEqual(resp.context['english_level'],english_level)
        self.assertEqual(resp.context['view_date'],date.today().isoformat())
        self.assertEqual(resp.context['start_view_date'],date.today().isoformat())

    def test_context_view_date_not_none(self):
        view_date = date.today().isoformat()
        start_view_date = date.today().isoformat()
        url = reverse('mande_summary_report',kwargs={'view_date':view_date,'start_view_date':start_view_date})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/mandesummaryreport.html')
        self.assertEqual(resp.context['view_date'],view_date)
        self.assertEqual(resp.context['start_view_date'],start_view_date)

class StudentPromotedReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'academic.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        url = reverse('student_promoted_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/student_promoted_report.html')
        academics = Academic.objects.filter(promote = True)
        students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())
        schools = School.objects.all()
        promoted_years = []
        years = datetime.now().year-2012
        list_of_years = []
        # generate list of year
        for r in range(years):
          list_of_years.append(datetime.now().year-r)
        # generate list of student break down by site and year
        for school in schools:
          promoted_years.extend(
              [
                  {
                  'school':school,
                  'total':[],
                  'years':[{'year'+str(i):{'years':str(i),'students':[]}} for i in list_of_years],

                  }
              ]
          )
        for student in students:
          academics = Academic.objects.filter(student_id=student,promote=True)
          for promoted_year in promoted_years:
                  if promoted_year['school'] == student.site:
                      for each_year in  promoted_year['years']:
                          for i in range(years):
                                  try:
                                      if len(academics) != 0:
                                          for academic in academics:
                                              # get academic by school year
                                              # the 2014 school year is 1 Aug 2014 - 31 July 2015
                                              # the 2015 school year is 1 Aug 2015 - 31 July 2016
                                              if each_year['year'+str(datetime.now().year-i)]['years'] == str(academic.test_date.year) or int(each_year['year'+str(datetime.now().year-i)]['years'])+1 == academic.test_date.year:
                                                  beginning = str(each_year['year'+str(datetime.now().year-i)]['years'])+"-08-01"
                                                  end = str(int(each_year['year'+str(datetime.now().year-i)]['years'])+1)+"-07-31"

                                                  beginning_of_school_year = datetime.strptime(beginning, "%Y-%m-%d").date()
                                                  end_of_school_year = datetime.strptime(end, "%Y-%m-%d").date()

                                                  if academic.test_date >= beginning_of_school_year and academic.test_date <= end_of_school_year:
                                                      each_year['year'+str(datetime.now().year-i)]['students'].append(academic.student_id)
                                                      promoted_year['total'].append(academic.student_id)

                                  except:
                                      pass
        self.assertEqual(resp.context['promoted_years'],promoted_years)
        self.assertEqual(resp.context['list_of_years'],list_of_years)

class StudentPromotedTimesReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'exitsurveys.json','academic.json','classroomenrollment.json',
        'intakeinternals.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        academic_year_start = datetime.now().year-2013
        self.academic_years = []
        # generate list of year
        for r in range(academic_year_start):
           self.academic_years.append(datetime.now().year-r)
        # test classroom enrollment drop date not today
        ClassroomEnrollment.objects.create(
            student_id = IntakeSurvey.objects.get(pk=2),
            classroom_id = Classroom.objects.get(pk=1),
            enrollment_date = '2014-06-01',
            drop_date = date.today().isoformat()
        )
        Academic.objects.create(
            student_id = IntakeSurvey.objects.get(pk=2),
            test_date = '2014-06-01',
            test_level = 1,
            test_grade_math = 100,
            test_grade_khmer = 100,
            promote = True
        )

    def test_context(self):
        # filter_seach is CURRENT
        url = reverse('students_promoted_times_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/students_promoted_times_report.html')
        exit_surveys = ExitSurvey.objects.filter(exit_date__lte=date.today().isoformat()).values_list('student_id',flat=True)
        students = IntakeSurvey.objects.exclude(student_id__in=exit_surveys).filter(date__lte=date.today().isoformat())
        enrolled_catch_up = ClassroomEnrollment.objects.all().filter( Q( Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)) & Q(Q(Q(classroom_id__cohort__gte=1) & Q(classroom_id__cohort__lte=6)) | Q(Q(classroom_id__cohort__gte=-3) & Q(classroom_id__cohort__lte=-1))) ) ).values_list('student_id',flat=True)
        students = students.filter(student_id__in=enrolled_catch_up)

        students_promoted = {}
        for student in students:
          current_grade = student.current_vdp_grade()
          #if the student has a valid current grade
          if current_grade < 50:
              try:
                 intake_date = student.intakeinternal_set.all().filter().order_by(
                              '-enrollment_date'
                          )[0].enrollment_date
              except IndexError:
                  intake_date = None
              students_promoted[student] = {
                  'promoted_times':len(Academic.objects.filter(student_id=student,promote=True)),
                  'lastest_promoted_date':Academic.objects.filter(student_id=student,promote=True).latest('test_date').test_date if len(Academic.objects.filter(student_id=student,promote=True)) > 0 else None,
                  'enrolled_date' : intake_date
                                            }

        self.assertEqual(resp.context['students_promoted'],students_promoted)
        self.assertEqual(resp.context['filter_seach'],'CURRENT')
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(resp.context['academic_years'],self.academic_years)
        self.assertEqual(resp.context['year'],0)
        self.assertEqual(resp.context['school'],0)
        self.assertEqual(resp.context['grade'],0)
        self.assertEqual(resp.context['classroom'],0)

    def test_context_filter_search_ALL(self):
        filter_seach = 'ALL'
        url = reverse('students_promoted_times_report',kwargs={'filter_seach':filter_seach})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/students_promoted_times_report.html')
        students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())
        students_promoted = {}
        for student in students:
          current_grade = student.current_vdp_grade()

          #if the student has a valid current grade
          if current_grade < 50:
              try:
                 intake_date = student.intakeinternal_set.all().filter().order_by(
                              '-enrollment_date'
                          )[0].enrollment_date
              except IndexError:
                  intake_date = None
              students_promoted[student] = {
                  'promoted_times':len(Academic.objects.filter(student_id=student,promote=True)),
                  'lastest_promoted_date':Academic.objects.filter(student_id=student,promote=True).latest('test_date').test_date if len(Academic.objects.filter(student_id=student,promote=True)) > 0 else None,
                  'enrolled_date' : intake_date
                                            }

        self.assertEqual(resp.context['students_promoted'],students_promoted)
        self.assertEqual(resp.context['filter_seach'],filter_seach)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(resp.context['academic_years'],self.academic_years)
        self.assertEqual(resp.context['year'],0)
        self.assertEqual(resp.context['school'],0)
        self.assertEqual(resp.context['grade'],0)
        self.assertEqual(resp.context['classroom'],0)

    def test_context_year_not_none(self):
        filter_seach = 'ALL'
        year = 2014
        url = reverse('students_promoted_times_report',kwargs={'filter_seach':filter_seach,'year':year})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/students_promoted_times_report.html')
        students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())
        students_promoted = {}
        school_year_start_date = str(year)+"-08-01"
        school_year_end_date = str(int(year)+1)+"-07-31"
        for student in students:
          current_grade = student.current_vdp_grade()

          #if the student has a valid current grade
          if current_grade < 50:
              try:
                 intake_date = student.intakeinternal_set.all().filter().order_by(
                              '-enrollment_date'
                          )[0].enrollment_date
              except IndexError:
                  intake_date = None
              students_promoted[student] = {
                  'promoted_times':len( Academic.objects.filter( Q(student_id=student) & Q(promote=True) & Q(Q(test_date__gte=school_year_start_date) & Q(test_date__lte=school_year_end_date)) ) ),
                  'lastest_promoted_date':Academic.objects.filter(student_id=student,promote=True).latest('test_date').test_date if len(Academic.objects.filter(student_id=student,promote=True)) > 0 else None,
                  'enrolled_date' : intake_date
                                            }

        self.assertEqual(resp.context['students_promoted'],students_promoted)
        self.assertEqual(resp.context['filter_seach'],filter_seach)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(resp.context['academic_years'],self.academic_years)
        self.assertEqual(resp.context['year'],str(year))
        self.assertEqual(resp.context['school'],0)
        self.assertEqual(resp.context['grade'],0)
        self.assertEqual(resp.context['classroom'],0)

    def test_context_school_not_none(self):
        filter_seach = 'ALL'
        year = 2014
        school = 1
        url = reverse('students_promoted_times_report',
                    kwargs={'filter_seach':filter_seach,'year':year,'school':school})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/students_promoted_times_report.html')
        students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())
        students = students.filter(site=school)
        students_promoted = {}
        school_year_start_date = str(year)+"-08-01"
        school_year_end_date = str(int(year)+1)+"-07-31"
        for student in students:
          current_grade = student.current_vdp_grade()

          #if the student has a valid current grade
          if current_grade < 50:
              try:
                 intake_date = student.intakeinternal_set.all().filter().order_by(
                              '-enrollment_date'
                          )[0].enrollment_date
              except IndexError:
                  intake_date = None
              students_promoted[student] = {
                  'promoted_times':len( Academic.objects.filter( Q(student_id=student) & Q(promote=True) & Q(Q(test_date__gte=school_year_start_date) & Q(test_date__lte=school_year_end_date)) ) ),
                  'lastest_promoted_date':Academic.objects.filter(student_id=student,promote=True).latest('test_date').test_date if len(Academic.objects.filter(student_id=student,promote=True)) > 0 else None,
                  'enrolled_date' : intake_date
                                            }

        self.assertEqual(resp.context['students_promoted'],students_promoted)
        self.assertEqual(resp.context['filter_seach'],filter_seach)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(resp.context['academic_years'],self.academic_years)
        self.assertEqual(resp.context['year'],str(year))
        self.assertEqual(resp.context['school'],str(school))
        self.assertEqual(resp.context['grade'],0)
        self.assertEqual(resp.context['classroom'],0)

    def test_context_school_and_grade_not_none(self):
        filter_seach = 'ALL'
        year = 2014
        school = 1
        grade = 1
        url = reverse('students_promoted_times_report',
                    kwargs={
                        'filter_seach':filter_seach,'year':year,
                        'school':school,
                        'grade':grade
                           })
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/students_promoted_times_report.html')
        students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())
        enrolled_class = ClassroomEnrollment.objects.filter(Q(Q(classroom_id__school_id=school) & Q(classroom_id__cohort=grade) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))).values_list('student_id',flat=True)
        students = students.filter(student_id__in=enrolled_class)

        students_promoted = {}
        school_year_start_date = str(year)+"-08-01"
        school_year_end_date = str(int(year)+1)+"-07-31"
        for student in students:
          current_grade = student.current_vdp_grade()

          #if the student has a valid current grade
          if current_grade < 50:
              try:
                 intake_date = student.intakeinternal_set.all().filter().order_by(
                              '-enrollment_date'
                          )[0].enrollment_date
              except IndexError:
                  intake_date = None
              students_promoted[student] = {
                  'promoted_times':len( Academic.objects.filter( Q(student_id=student) & Q(promote=True) & Q(Q(test_date__gte=school_year_start_date) & Q(test_date__lte=school_year_end_date)) ) ),
                  'lastest_promoted_date':Academic.objects.filter(student_id=student,promote=True).latest('test_date').test_date if len(Academic.objects.filter(student_id=student,promote=True)) > 0 else None,
                  'enrolled_date' : intake_date
                                            }

        self.assertEqual(resp.context['students_promoted'],students_promoted)
        self.assertEqual(resp.context['filter_seach'],filter_seach)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(resp.context['academic_years'],self.academic_years)
        self.assertEqual(resp.context['year'],str(year))
        self.assertEqual(resp.context['school'],str(school))
        self.assertEqual(resp.context['grade'],str(grade))
        self.assertEqual(resp.context['classroom'],0)

    def test_context_school_and_grade_and_classroom_not_none(self):
        filter_seach = 'ALL'
        year = 2014
        school = 1
        grade = 1
        classroom = 1
        url = reverse('students_promoted_times_report',
                    kwargs={
                        'filter_seach':filter_seach,'year':year,
                        'school':school,
                        'grade':grade,
                        'classroom':classroom
                         })
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/students_promoted_times_report.html')
        students = IntakeSurvey.objects.all().filter(date__lte=date.today().isoformat())

        enrolled_class = ClassroomEnrollment.objects.filter(Q(Q(classroom_id=classroom) & Q(Q(drop_date__gt=date.today().isoformat()) | Q(drop_date=None)))).values_list('student_id',flat=True)
        students = students.filter(student_id__in=enrolled_class)

        students_promoted = {}
        school_year_start_date = str(year)+"-08-01"
        school_year_end_date = str(int(year)+1)+"-07-31"
        for student in students:
          current_grade = student.current_vdp_grade()

          #if the student has a valid current grade
          if current_grade < 50:
              try:
                 intake_date = student.intakeinternal_set.all().filter().order_by(
                              '-enrollment_date'
                          )[0].enrollment_date
              except IndexError:
                  intake_date = None
              students_promoted[student] = {
                  'promoted_times':len( Academic.objects.filter( Q(student_id=student) & Q(promote=True) & Q(Q(test_date__gte=school_year_start_date) & Q(test_date__lte=school_year_end_date)) ) ),
                  'lastest_promoted_date':Academic.objects.filter(student_id=student,promote=True).latest('test_date').test_date if len(Academic.objects.filter(student_id=student,promote=True)) > 0 else None,
                  'enrolled_date' : intake_date
                                            }

        self.assertEqual(resp.context['students_promoted'],students_promoted)
        self.assertEqual(resp.context['filter_seach'],filter_seach)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(resp.context['academic_years'],self.academic_years)
        self.assertEqual(resp.context['year'],str(year))
        self.assertEqual(resp.context['school'],str(school))
        self.assertEqual(resp.context['grade'],str(grade))
        self.assertEqual(resp.context['classroom'],str(classroom))

class PublicSchoolReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'exitsurveys.json','intakeinternals.json','intakeupdates.json',
        'publicschoolhistorys.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        PublicSchoolHistory.objects.create(
            student_id=IntakeSurvey.objects.get(pk=10),
            status = 'Y',
            enroll_date='2016-11-01',
            grade = 1
        );
        PublicSchoolHistory.objects.create(
            student_id=IntakeSurvey.objects.get(pk=2),
            status = 'Y',
            enroll_date='2016-10-01',
            grade = 1
        );

    def test_context_academic_year_none_start_date_none_end_date_none_number_none(self):
        url = reverse('public_school_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/public_school_report.html')

        months = [
          ('01', 'January'), ('02', 'February'), ('03', 'March'),
          ('04', 'April'), ('05', 'May'), ('06', 'June'),
          ('07', 'July'), ('08', 'August'), ('09', 'September'),
          ('10', 'October'), ('11', 'November'), ('12', 'December')
         ]

        exit_surveys = ExitSurvey.objects.all().filter(
                        exit_date__lte=date.today().isoformat()
                        ).values_list('student_id',flat=True)
        active_surveys = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                                 ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
        pschools = PublicSchoolHistory.objects.filter(student_id__student_id__in=active_surveys)

        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]='English'
        grades[70]='Vietnamese'

        academic_year_start = datetime.now().year-2013
        academic_years = []
        # generate list of year
        for r in range(academic_year_start):
          academic_years.append(datetime.now().year-r)

        self.assertEqual(resp.context['grades'],grades)
        self.assertEqual(list(resp.context['pschools']),list(pschools))
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['academic_years'],academic_years)
        self.assertEqual(resp.context['academic_year'],0)
        self.assertEqual(resp.context['number'],None)
        self.assertEqual(resp.context['days'],range(1,32))
        self.assertEqual(resp.context['months'],months)
        self.assertEqual(resp.context['start_date'],'11-01')
        self.assertEqual(resp.context['end_date'],'10-31')

    def test_context_academic_year_not_none_start_date_not_none_end_date_not_none_number_none(self):
        academic_year = 2016
        start_date = '10-01'
        end_date = '10-31'
        url = reverse('public_school_report',kwargs={
                                    'academic_year':academic_year,
                                    'start_date' : start_date,
                                    'end_date' : end_date
                                })
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/public_school_report.html')

        months = [
          ('01', 'January'), ('02', 'February'), ('03', 'March'),
          ('04', 'April'), ('05', 'May'), ('06', 'June'),
          ('07', 'July'), ('08', 'August'), ('09', 'September'),
          ('10', 'October'), ('11', 'November'), ('12', 'December')
         ]
        academic_year_start = str(academic_year)+'-'+start_date
        academic_year_end = str(int(academic_year)+1)+'-'+end_date
        exit_surveys = ExitSurvey.objects.all().filter(
                        exit_date__lte=date.today().isoformat()
                        ).values_list('student_id',flat=True)
        active_surveys = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                                 ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
        pschools = PublicSchoolHistory.objects.filter(student_id__student_id__in=active_surveys)
        pschools = pschools.filter( Q(enroll_date__gte=academic_year_start) & Q(enroll_date__lte=academic_year_end) )

        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]='English'
        grades[70]='Vietnamese'

        academic_year_start = datetime.now().year-2013
        academic_years = []
        # generate list of year
        for r in range(academic_year_start):
          academic_years.append(datetime.now().year-r)

        self.assertEqual(resp.context['grades'],grades)
        self.assertEqual(list(resp.context['pschools']),list(pschools))
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['academic_years'],academic_years)
        self.assertEqual(resp.context['academic_year'],str(academic_year))
        self.assertEqual(resp.context['number'],None)
        self.assertEqual(resp.context['days'],range(1,32))
        self.assertEqual(resp.context['months'],months)
        self.assertEqual(resp.context['start_date'],start_date)
        self.assertEqual(resp.context['end_date'],end_date)

    def test_context_academic_year_not_none_start_date_not_none_end_date_not_none_number_not_none(self):
        academic_year = 2016
        start_date = '10-01'
        end_date = '10-31'
        number = 1
        url = reverse('public_school_report',kwargs={
                                    'academic_year':academic_year,
                                    'start_date' : start_date,
                                    'end_date' : end_date,
                                    'number' : number
                                })
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/public_school_report.html')

        months = [
          ('01', 'January'), ('02', 'February'), ('03', 'March'),
          ('04', 'April'), ('05', 'May'), ('06', 'June'),
          ('07', 'July'), ('08', 'August'), ('09', 'September'),
          ('10', 'October'), ('11', 'November'), ('12', 'December')
         ]

        academic_year_start = str(academic_year)+'-'+start_date
        academic_year_end = str(int(academic_year)+1)+'-'+end_date

        exit_surveys = ExitSurvey.objects.all().filter(
                        exit_date__lte=date.today().isoformat()
                        ).values_list('student_id',flat=True)
        active_surveys = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                                 ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
        pschools = PublicSchoolHistory.objects.filter(student_id__student_id__in=active_surveys)

        pschools = pschools.filter( Q(enroll_date__gte=academic_year_start) & Q(enroll_date__lte=academic_year_end) )

        students_match_number = pschools.values('student_id_id').annotate(count=Count('student_id_id')).filter(count=number).values_list('student_id_id',flat=True)
        pschools = pschools.filter(student_id__student_id__in=students_match_number)

        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]='English'
        grades[70]='Vietnamese'

        academic_year_start = datetime.now().year-2013
        academic_years = []
        # generate list of year
        for r in range(academic_year_start):
          academic_years.append(datetime.now().year-r)

        self.assertEqual(resp.context['grades'],grades)
        self.assertEqual(list(resp.context['pschools']),list(pschools))
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['academic_years'],academic_years)
        self.assertEqual(resp.context['academic_year'],str(academic_year))
        self.assertEqual(resp.context['number'],str(number))
        self.assertEqual(resp.context['days'],range(1,32))
        self.assertEqual(resp.context['months'],months)
        self.assertEqual(resp.context['start_date'],start_date)
        self.assertEqual(resp.context['end_date'],end_date)

class StudentsLagSummaryViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'intakeupdates.json','academic.json','intakeinternals.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        url = reverse('students_lag_summary',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/students_lag_summary.html')
        enrolled_students = getEnrolledStudents()
        schools = School.objects.filter(active=True)

        students_lag_by_site = {}
        students_all_site = {
          'students_age_not_appropriate_grade_level' : {'M':0,'F':0},
          'students' : {'M':0,'F':0}
        }
        for school in schools:
          name = school.school_name
          students_lag_by_site[name] = {}
          students_lag_by_site[name]['students_age_not_appropriate_grade_level'] = {'M':0,'F':0}
          students_lag_by_site[name]['students'] = {'M':0,'F':0}

        for student in enrolled_students:
          age_appropriate_grade = student.age_appropriate_grade()
          current_grade = student.current_vdp_grade()
          site = student.site
          gender = student.gender
          if current_grade <= 6 and current_grade >= -3:
              students_lag_by_site[unicode(site)]['students'][gender] +=1
              students_all_site['students'][gender] +=1

              lag = age_appropriate_grade - current_grade
              if lag > 0:
                  students_lag_by_site[unicode(site)]['students_age_not_appropriate_grade_level'][gender] +=1
                  students_all_site['students_age_not_appropriate_grade_level'][gender] +=1
        self.assertEqual(resp.context['students_lag_by_site'],students_lag_by_site)
        self.assertEqual(resp.context['students_all_site'],students_all_site)

class AnomalousDataViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        url = reverse('anomalous_data',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/anomalous_data_report.html')
        future_students = IntakeSurvey.objects.all().filter(date__gte=date.today().isoformat()).order_by('student_id')
        self.assertEqual(list(resp.context['future_students']),list(future_students))

class AdvancedReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'intakeupdates.json','publicschoolhistorys.json','spiritualactivities.json',
        'classroomenrollment.json','academic.json','intakeinternals.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

        ClassroomEnrollment.objects.create(
            classroom_id = Classroom.objects.get(pk=1),
            student_id = IntakeSurvey.objects.get(pk=2) ,
            enrollment_date = "2014-01-01",
            drop_date = date.today().isoformat()
        )

    def test_context(self):
        url = reverse('advanced_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')

        self.assertEqual(resp.context['students'],None)
        self.assertEqual(list(resp.context['schools']),list(School.objects.all()))
        self.assertEqual(resp.context['genders'],dict(GENDERS))
        self.assertEqual(resp.context['grades'],dict(GRADES))
        self.assertEqual(resp.context['relationships'],dict(RELATIONSHIPS))
        self.assertEqual(resp.context['yns'],dict(YN))
        self.assertEqual(resp.context['employments'],dict(EMPLOYMENT))
        self.assertEqual(list(resp.context['classrooms']),list(Classroom.objects.filter(active=True)))
        self.assertEqual(resp.context['show_data'],'no')

        raised = True
        try:
                json.loads(resp.context['data_guardian1_profession'])
        except:
                raised = False
        self.assertTrue(raised)
        self.assertEqual(resp.context['data_guardian1_profession'],'["123", "456"]')

        raised = True
        try:
                json.loads(resp.context['data_guardian2_profession'])
        except:
                raised = False
        self.assertTrue(raised)
        self.assertEqual(resp.context['data_guardian2_profession'],'["NA"]')

        raised = True
        try:
                json.loads(resp.context['data_minors_profession'])
        except:
                raised = False
        self.assertTrue(raised)
        self.assertEqual(resp.context['data_minors_profession'],'["", "NA"]')

        raised = True
        try:
                json.loads(resp.context['data_minors_training_type'])
        except:
                raised = False
        self.assertTrue(raised)
        self.assertEqual(resp.context['data_minors_training_type'],'["", "NA"]')

        raised = True
        try:
                json.loads(resp.context['data_reasons'])
        except:
                raised = False
        self.assertTrue(raised)
        self.assertEqual(resp.context['data_reasons'],'["test"]')

        raised = True
        try:
                json.loads(resp.context['data_public_schools'])
        except:
                raised = False
        self.assertTrue(raised)
        self.assertEqual(resp.context['data_public_schools'],'["test"]')

        def convert_field_to_readable(string):
             s = string
             s1= re.sub(r'([^0-9])([0-9])', r'\1 \2',s)
             s2 =  re.sub(r'([$0-9])([a-z])', r'\1 \2',s1)
             s3 = s2.replace('_', ' ').title()
             return s3

        all_fields = IntakeSurvey._meta.fields
        list_of_fields = dict((field.name, convert_field_to_readable(field.name)) for field in all_fields if not field.primary_key)
        list_of_fields.update({
               "vdp_grade":"VDP Grade", "classroom": "Classroom","id":"ID",
               "enrolled":"Enrolled","grade_current":"Grade Current",
               "grade_last":"Grade Last","reasons":"Reasons",
               "public_school_name":"Public School Name"
               })
        self.assertEqual(resp.context['list_of_fields'],sorted(list_of_fields.iteritems()))

    def test_context_post(self):
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_student_id(self):
        student_id = 1
        data = {
            'studnet_id':student_id,
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains=student_id) &
                        Q(name__contains='')
                        ).filter(filter_query)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_name(self):
        name = 'test'
        data = {
            'studnet_id':'',
            'name':name,
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains=name)
                        ).filter(filter_query)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_school(self):
        school = 1
        data = {
            'studnet_id':'',
            'name':'',
            'school':school,
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        q=[]
        filter_query = Q()
        q.append(Q(site=school))
        if len(q) > 0:
            filter_query = reduce(operator.and_, q)

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_intake_date(self):
        intake_date = '2015-07-01'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':intake_date,
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        q=[]
        filter_query = Q()
        q.append(Q(date=intake_date))
        if len(q) > 0:
            filter_query = reduce(operator.and_, q)

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_dob(self):
        dob = '2005-07-01'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':dob,
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        q=[]
        filter_query = Q()
        q.append(Q(dob=dob))
        if len(q) > 0:
            filter_query = reduce(operator.and_, q)

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_gender(self):
        gender = 'F'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':gender,
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        q=[]
        filter_query = Q()
        q.append(Q(gender=gender))
        if len(q) > 0:
            filter_query = reduce(operator.and_, q)

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_dob_year(self):
        dob_year = '2005'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':dob_year,
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        q=[]
        filter_query = Q()
        q.append(Q(dob__year=int(dob_year)))
        if len(q) > 0:
            filter_query = reduce(operator.and_, q)

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_intake_date_year(self):
        intake_date_year = '2015'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':intake_date_year,
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        q=[]
        filter_query = Q()
        q.append(Q(date__year=int(intake_date_year)))
        if len(q) > 0:
            filter_query = reduce(operator.and_, q)

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_address(self):
        address = '123'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':address,

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        recent_field_list={}
        recent_field_list['address'] = address

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        equal_value_list = ['guardian1_relationship','guardian2_relationship',
                            'guardian1_employment','guardian2_employment',
                            'minors','minors_in_public_school','minors_in_other_school',
                            'minors_working','minors_encouraged','minors_training',
                           ]
        match_intakeUpdate = []
        for key, value in recent_field_list.iteritems():
            match_intakeUpdate=[]
            for student in students:
               student_recent_data = student.getRecentFields()
               if key not in equal_value_list:
                   if value in student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
               else:
                   if value == student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
            students = students.filter(student_id__in=match_intakeUpdate)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian1_name(self):
        guardian1_name = '123'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':guardian1_name,
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        recent_field_list={}
        recent_field_list['guardian1_name'] = guardian1_name

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        equal_value_list = ['guardian1_relationship','guardian2_relationship',
                            'guardian1_employment','guardian2_employment',
                            'minors','minors_in_public_school','minors_in_other_school',
                            'minors_working','minors_encouraged','minors_training',
                           ]
        match_intakeUpdate = []
        for key, value in recent_field_list.iteritems():
            match_intakeUpdate=[]
            for student in students:
               student_recent_data = student.getRecentFields()
               if key not in equal_value_list:
                   if value in student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
               else:
                   if value == student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
            students = students.filter(student_id__in=match_intakeUpdate)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian1_relationship(self):
        guardian1_relationship = 'FATHER'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':guardian1_relationship,
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        recent_field_list={}
        recent_field_list['guardian1_relationship'] = guardian1_relationship

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        equal_value_list = ['guardian1_relationship','guardian2_relationship',
                            'guardian1_employment','guardian2_employment',
                            'minors','minors_in_public_school','minors_in_other_school',
                            'minors_working','minors_encouraged','minors_training',
                           ]
        match_intakeUpdate = []
        for key, value in recent_field_list.iteritems():
            match_intakeUpdate=[]
            for student in students:
               student_recent_data = student.getRecentFields()
               if key not in equal_value_list:
                   if value in student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
               else:
                   if value == student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
            students = students.filter(student_id__in=match_intakeUpdate)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian1_phone(self):
        guardian1_phone = '123'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':guardian1_phone,
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        recent_field_list={}
        recent_field_list['guardian1_phone'] = guardian1_phone

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        equal_value_list = ['guardian1_relationship','guardian2_relationship',
                            'guardian1_employment','guardian2_employment',
                            'minors','minors_in_public_school','minors_in_other_school',
                            'minors_working','minors_encouraged','minors_training',
                           ]
        match_intakeUpdate = []
        for key, value in recent_field_list.iteritems():
            match_intakeUpdate=[]
            for student in students:
               student_recent_data = student.getRecentFields()
               if key not in equal_value_list:
                   if value in student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
               else:
                   if value == student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
            students = students.filter(student_id__in=match_intakeUpdate)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian1_profession(self):
        guardian1_profession = '123'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':guardian1_profession,
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        recent_field_list={}
        recent_field_list['guardian1_profession'] = guardian1_profession

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        equal_value_list = ['guardian1_relationship','guardian2_relationship',
                            'guardian1_employment','guardian2_employment',
                            'minors','minors_in_public_school','minors_in_other_school',
                            'minors_working','minors_encouraged','minors_training',
                           ]
        match_intakeUpdate = []
        for key, value in recent_field_list.iteritems():
            match_intakeUpdate=[]
            for student in students:
               student_recent_data = student.getRecentFields()
               if key not in equal_value_list:
                   if value in student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
               else:
                   if value == student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
            students = students.filter(student_id__in=match_intakeUpdate)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian1_employment(self):
        guardian1_employment = '1'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':guardian1_employment,

            'guardian2_name':'',
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        recent_field_list={}
        recent_field_list['guardian1_employment'] = guardian1_employment

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        equal_value_list = ['guardian1_relationship','guardian2_relationship',
                            'guardian1_employment','guardian2_employment',
                            'minors','minors_in_public_school','minors_in_other_school',
                            'minors_working','minors_encouraged','minors_training',
                           ]
        match_intakeUpdate = []
        for key, value in recent_field_list.iteritems():
            match_intakeUpdate=[]
            for student in students:
               student_recent_data = student.getRecentFields()
               if key not in equal_value_list:
                   if value in student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
               else:
                   if value == student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
            students = students.filter(student_id__in=match_intakeUpdate)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian2_name(self):
        guardian2_name = ''
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':guardian2_name,
            'guardian2_relationship':'',
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        recent_field_list={}
        recent_field_list['guardian2_name'] = guardian2_name

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        equal_value_list = ['guardian1_relationship','guardian2_relationship',
                            'guardian1_employment','guardian2_employment',
                            'minors','minors_in_public_school','minors_in_other_school',
                            'minors_working','minors_encouraged','minors_training',
                           ]
        match_intakeUpdate = []
        for key, value in recent_field_list.iteritems():
            match_intakeUpdate=[]
            for student in students:
               student_recent_data = student.getRecentFields()
               if key not in equal_value_list:
                   if value in student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
               else:
                   if value == student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
            students = students.filter(student_id__in=match_intakeUpdate)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian2_relationship(self):
        guardian2_relationship = 'MOTHER'
        data = {
            'studnet_id':'',
            'name':'',
            'school':'',
            'gender':'',
            'intake_date':'',
            'intake_date_year':'',
            'dob':'',
            'dob_year':'',
            'address':'',

            'guardian1_name':'',
            'guardian1_relationship':'',
            'guardian1_phone':'',
            'guardian1_profession':'',
            'guardian1_employment':'',

            'guardian2_name':'',
            'guardian2_relationship':guardian2_relationship,
            'guardian2_phone':'',
            'guardian2_profession':'',
            'guardian2_employment':'',

             #Household Information
            'minors':'',
            'minors_in_public_school':'',
            'minors_in_other_school':'',
            'minors_working':'',
            'minors_profession':'',
            'minors_encouraged':'',
            'minors_training':'',
            'minors_training_type':'',

            'enrolled':'',
            'grade_current':'',
            'grade_last':'',
            'public_school_name':'',
            'reasons':'',

            'classroom':'',
            'vdp_grade':'',
        }
        url = reverse('advanced_report',kwargs={})
        resp = self.client.post(url,data)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/advancedreport.html')
        filter_query = Q()
        recent_field_list={}
        recent_field_list['guardian2_relationship'] = guardian2_relationship

        students = IntakeSurvey.objects.filter(
                        Q(student_id__contains='') &
                        Q(name__contains='')
                        ).filter(filter_query)
        equal_value_list = ['guardian1_relationship','guardian2_relationship',
                            'guardian1_employment','guardian2_employment',
                            'minors','minors_in_public_school','minors_in_other_school',
                            'minors_working','minors_encouraged','minors_training',
                           ]
        match_intakeUpdate = []
        for key, value in recent_field_list.iteritems():
            match_intakeUpdate=[]
            for student in students:
               student_recent_data = student.getRecentFields()
               if key not in equal_value_list:
                   if value in student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
               else:
                   if value == student_recent_data[key]:
                       match_intakeUpdate.append(student.student_id)
            students = students.filter(student_id__in=match_intakeUpdate)
        self.assertEqual(list(resp.context['students']),list(students))
        self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian2_phone(self):
       guardian2_phone = ''
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':guardian2_phone,
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['guardian2_phone'] = guardian2_phone

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian2_profession(self):
       guardian2_profession = 'NA'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':guardian2_profession,
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['guardian2_profession'] = guardian2_profession

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_guardian2_employment(self):
       guardian2_employment = '1'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':guardian2_employment,

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['guardian2_employment'] = guardian2_employment

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_minors(self):
       minors = '0'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':minors,
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['minors'] = int(minors)

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_minors_in_public_school(self):
       minors_in_public_school = '0'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':minors_in_public_school,
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['minors_in_public_school'] = int(minors_in_public_school)

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_minors_in_other_school(self):
       minors_in_other_school = '0'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':minors_in_other_school,
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['minors_in_other_school'] = int(minors_in_other_school)

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_minors_working(self):
       minors_working = '0'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':minors_working,
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['minors_working'] = int(minors_working)

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_minors_profession(self):
       minors_profession = ''
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':minors_profession,
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['minors_profession'] = minors_profession

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_minors_encouraged(self):
       minors_encouraged = 'NA'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':minors_encouraged,
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['minors_encouraged'] = minors_encouraged

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_minors_training(self):
       minors_training = 'NA'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':minors_training,
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       recent_field_list={}
       recent_field_list['minors_training'] = minors_training

       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       equal_value_list = ['guardian1_relationship','guardian2_relationship',
                           'guardian1_employment','guardian2_employment',
                           'minors','minors_in_public_school','minors_in_other_school',
                           'minors_working','minors_encouraged','minors_training',
                          ]
       match_intakeUpdate = []
       for key, value in recent_field_list.iteritems():
           match_intakeUpdate=[]
           for student in students:
              student_recent_data = student.getRecentFields()
              if key not in equal_value_list:
                  if value in student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
              else:
                  if value == student_recent_data[key]:
                      match_intakeUpdate.append(student.student_id)
           students = students.filter(student_id__in=match_intakeUpdate)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_classroom(self):
       classroom = 1
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':classroom,
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)
       students = students.filter(Q(classroomenrollment__classroom_id=classroom) & Q(Q(classroomenrollment__drop_date__gt=date.today().isoformat()) | Q(classroomenrollment__drop_date=None)))
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_vdp_grade(self):
       vdp_grade = 1
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':'',
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',
           'reasons':'',

           'classroom':'',
           'vdp_grade':vdp_grade,
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)

       student_filter_by_vdp_grade = []
       for student in students:
           if(student.current_vdp_grade() == int(vdp_grade)):
               student_filter_by_vdp_grade.append(student.student_id)
       students = students.filter(student_id__in=student_filter_by_vdp_grade)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_enrolled_grade_current_none(self):
       enrolled = 'Y'
       grade_current = ''
       public_school_name = ''
       reasons = ''
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':enrolled,
           'grade_current':grade_current,
           'grade_last':'',
           'public_school_name':public_school_name,
           'reasons':reasons,

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)

       student_filter_by_enrolled = []
       for student in students:
            pschool = student.get_pschool()
            if(pschool.status == enrolled):
                student_filter_by_enrolled.append(student.student_id)
       students = students.filter(student_id__in=student_filter_by_enrolled)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_enrolled_grade_current_and_public_school_name(self):
       enrolled = 'Y'
       grade_current = '2'
       public_school_name = 'test'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':enrolled,
           'grade_current':grade_current,
           'grade_last':'',
           'public_school_name':public_school_name,
           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)

       student_filter_by_enrolled = []
       for student in students:
            pschool = student.get_pschool()
            if pschool.status == enrolled and pschool.grade == int(grade_current) and re.search(public_school_name,pschool.school_name, re.IGNORECASE):
                student_filter_by_enrolled.append(student.student_id)
       students = students.filter(student_id__in=student_filter_by_enrolled)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_enrolled_grade_current(self):
       enrolled = 'Y'
       grade_current = '2'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':enrolled,
           'grade_current':grade_current,
           'grade_last':'',
           'public_school_name':'',

           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)

       student_filter_by_enrolled = []
       for student in students:
          pschool = student.get_pschool()
          if pschool.status == enrolled and pschool.grade == int(grade_current):
              student_filter_by_enrolled.append(student.student_id)
       students = students.filter(student_id__in=student_filter_by_enrolled)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_enrolled_public_school_name(self):
       enrolled = 'Y'
       public_school_name = 'test'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':enrolled,
           'grade_current':'',
           'grade_last':'',
           'public_school_name':public_school_name,

           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)

       student_filter_by_enrolled = []
       for student in students:
            pschool = student.get_pschool()
            if pschool.status == enrolled and re.search(public_school_name,pschool.school_name, re.IGNORECASE):
                student_filter_by_enrolled.append(student.student_id)
       students = students.filter(student_id__in=student_filter_by_enrolled)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_enrolled_no_grade_last_and_reason(self):
       enrolled = 'N'
       grade_last = '1'
       reasons = 'test'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':enrolled,
           'grade_current':'',
           'grade_last':grade_last,
           'public_school_name':'',

           'reasons':reasons,

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)

       student_filter_by_enrolled = []
       for student in students:
          pschool = student.get_pschool()
          if pschool.status == enrolled and pschool.last_grade == int(grade_last) and re.search(reasons,pschool.reasons, re.IGNORECASE):
              student_filter_by_enrolled.append(student.student_id)
       students = students.filter(student_id__in=student_filter_by_enrolled)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_enrolled_no_grade_last(self):
       enrolled = 'N'
       grade_last = '1'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':enrolled,
           'grade_current':'',
           'grade_last':grade_last,
           'public_school_name':'',

           'reasons':'',

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)

       student_filter_by_enrolled = []
       for student in students:
           pschool = student.get_pschool()
           if pschool.status == enrolled and pschool.last_grade == int(grade_last):
               student_filter_by_enrolled.append(student.student_id)
       students = students.filter(student_id__in=student_filter_by_enrolled)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

    def test_context_post_enrolled_no_reason(self):
       enrolled = 'N'
       reasons = 'test'
       data = {
           'studnet_id':'',
           'name':'',
           'school':'',
           'gender':'',
           'intake_date':'',
           'intake_date_year':'',
           'dob':'',
           'dob_year':'',
           'address':'',

           'guardian1_name':'',
           'guardian1_relationship':'',
           'guardian1_phone':'',
           'guardian1_profession':'',
           'guardian1_employment':'',

           'guardian2_name':'',
           'guardian2_relationship':'',
           'guardian2_phone':'',
           'guardian2_profession':'',
           'guardian2_employment':'',

            #Household Information
           'minors':'',
           'minors_in_public_school':'',
           'minors_in_other_school':'',
           'minors_working':'',
           'minors_profession':'',
           'minors_encouraged':'',
           'minors_training':'',
           'minors_training_type':'',

           'enrolled':enrolled,
           'grade_current':'',
           'grade_last':'',
           'public_school_name':'',

           'reasons':reasons,

           'classroom':'',
           'vdp_grade':'',
       }
       url = reverse('advanced_report',kwargs={})
       resp = self.client.post(url,data)
       self.assertEqual(resp.status_code, 200)
       self.assertTemplateUsed(resp,'mande/advancedreport.html')
       filter_query = Q()
       students = IntakeSurvey.objects.filter(
                       Q(student_id__contains='') &
                       Q(name__contains='')
                       ).filter(filter_query)

       student_filter_by_enrolled = []
       for student in students:
          pschool = student.get_pschool()
          if pschool.status == enrolled and re.search(reasons,pschool.reasons, re.IGNORECASE):
              student_filter_by_enrolled.append(student.student_id)
       students = students.filter(student_id__in=student_filter_by_enrolled)
       self.assertEqual(list(resp.context['students']),list(students))
       self.assertEqual(resp.context['show_data'],'yes')

class UnapprovedAbsenceWithNoCommentViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'attendances.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        url = reverse('unapproved_absence_with_no_comment',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/unapproved_absence_with_no_comment.html')
        thisyear = date.today().year
        school_year_list = [thisyear-i for i in range(thisyear-2013)]
        unapproved_absence_no_comments = Attendance.objects.all().filter(attendance__exact="UA").filter(Q(Q(notes=u"") |Q(notes=None))).order_by('-date')

        self.assertEqual(list(resp.context['unapproved_absence_no_comments']),list(unapproved_absence_no_comments))
        self.assertEqual(resp.context['school_year_list'],school_year_list)
        self.assertEqual(resp.context['school_year'],None)

    def test_context_school_year_not_none(self):
        school_year = 2015
        url = reverse('unapproved_absence_with_no_comment',kwargs={'school_year':school_year})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/unapproved_absence_with_no_comment.html')
        thisyear = date.today().year
        school_year_list = [thisyear-i for i in range(thisyear-2013)]

        school_year_start_date = str(school_year)+"-08-01"
        school_year_end_date = str(int(school_year)+1)+"-07-31"
        unapproved_absence_no_comments = Attendance.objects.all().filter(attendance__exact="UA").filter(Q(Q(notes=u"") |Q(notes=None)) & Q(Q(date__gte=school_year_start_date) & Q(date__lte=school_year_end_date))).order_by('-date')

        self.assertEqual(list(resp.context['unapproved_absence_no_comments']),list(unapproved_absence_no_comments))
        self.assertEqual(resp.context['school_year_list'],school_year_list)
        self.assertEqual(resp.context['school_year'],str(school_year))

class GenerateViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'exitsurveys.json','intakeupdates.json','currentstudentInfos.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))

    def test_context(self):
        IntakeSurvey.objects.create(
            date=date.today().isoformat(),
            site=School.objects.get(pk=1),
            name='test',dob='2013-01-01'
            )
        self.assertEqual(CurrentStudentInfo.objects.all().count(),9)

        url = reverse('generate',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/generatestudentinfo.html')
        exit_surveys = ExitSurvey.objects.all().filter(
                         exit_date__lte=date.today().isoformat()
                         ).values_list('student_id',flat=True)
        active_surveys = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                                  ).exclude(student_id__in=exit_surveys)
        for survey in active_surveys:
            recent_survey = survey.getRecentFields()
            self.assertTrue(
                CurrentStudentInfo.objects.filter(
                student_id=recent_survey['student_id'], name=recent_survey['name'],site=recent_survey['site'],
                date=recent_survey['date'],dob = recent_survey['dob'],gender = recent_survey['gender'],
                age_appropriate_grade = survey.age_appropriate_grade(),
                in_public_school = True if survey.get_pschool().status=='Y' else False,
                at_grade_level = studentAtAgeAppropriateGradeLevel(recent_survey['student_id']),
                vdp_grade = survey.current_vdp_grade(),
                refresh = date.today().isoformat()
                ).exists()
            )
        self.assertEqual(resp.context['students'],active_surveys.count())
        self.assertEqual(CurrentStudentInfo.objects.all().count(),10)

class SpiritualSurveyReportViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','classrooms.json','intakesurveys.json',
        'exitsurveys.json','intakeinternals.json','intakeupdates.json',
        'spiritualactivities.json'
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        SpiritualActivitiesSurvey.objects.create(
            student_id=IntakeSurvey.objects.get(pk=10),
            date='2016-08-01',
            church_name = 'test'
        );
        SpiritualActivitiesSurvey.objects.create(
            student_id=IntakeSurvey.objects.get(pk=2),
            date='2016-08-01',
            church_name = 'test'
        );

    def test_context_academic_year_none_year_none(self):
        url = reverse('spiritual_survey_report',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/spiritual_survey_report.html')

        exit_surveys = ExitSurvey.objects.all().filter(
                        exit_date__lte=date.today().isoformat()
                        ).values_list('student_id',flat=True)
        active_surveys = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                                 ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
        spiritual_surveys = SpiritualActivitiesSurvey.objects.filter(student_id__student_id__in=active_surveys)

        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]='English'
        grades[70]='Vietnamese'

        academic_year_start = datetime.now().year-2013
        academic_years = []
        # generate list of year
        for r in range(academic_year_start):
          academic_years.append(datetime.now().year-r)
        self.assertEqual(resp.context['grades'],grades)
        self.assertEqual(list(resp.context['spiritual_surveys']),list(spiritual_surveys))
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['academic_years'],academic_years)
        self.assertEqual(resp.context['academic_year'],0)
        self.assertEqual(resp.context['number'],None)

    def test_context_academic_year_not_none_year_none(self):
        academic_year = 2016
        url = reverse('spiritual_survey_report',kwargs={
                                    'academic_year':academic_year
                                })
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/spiritual_survey_report.html')

        academic_year_start = str(academic_year)+"-08-01"
        academic_year_end = str(int(academic_year)+1)+"-07-31"
        exit_surveys = ExitSurvey.objects.all().filter(
                        exit_date__lte=date.today().isoformat()
                        ).values_list('student_id',flat=True)
        active_surveys = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                                 ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
        spiritual_surveys = SpiritualActivitiesSurvey.objects.filter(student_id__student_id__in=active_surveys)
        spiritual_surveys = spiritual_surveys.filter( Q(date__gte=academic_year_start) & Q(date__lte=academic_year_end) )


        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]='English'
        grades[70]='Vietnamese'

        academic_year_start = datetime.now().year-2013
        academic_years = []
        # generate list of year
        for r in range(academic_year_start):
          academic_years.append(datetime.now().year-r)

        self.assertEqual(resp.context['grades'],grades)
        self.assertEqual(list(resp.context['spiritual_surveys']),list(spiritual_surveys))
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['academic_years'],academic_years)
        self.assertEqual(resp.context['academic_year'],str(academic_year))
        self.assertEqual(resp.context['number'],None)

    def test_context_academic_year_not_none_year_not_none(self):
        academic_year = 2016
        number = 1
        url = reverse('spiritual_survey_report',kwargs={
                                    'academic_year':academic_year,
                                    'number' : number
                                })
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/spiritual_survey_report.html')

        academic_year_start = str(academic_year)+"-08-01"
        academic_year_end = str(int(academic_year)+1)+"-07-31"

        exit_surveys = ExitSurvey.objects.all().filter(
                        exit_date__lte=date.today().isoformat()
                        ).values_list('student_id',flat=True)
        active_surveys = IntakeSurvey.objects.filter(date__lte=date.today().isoformat()).order_by('student_id'
                                 ).exclude(student_id__in=exit_surveys).values_list('student_id',flat=True)
        spiritual_surveys = SpiritualActivitiesSurvey.objects.filter(student_id__student_id__in=active_surveys)

        spiritual_surveys = spiritual_surveys.filter( Q(date__gte=academic_year_start) & Q(date__lte=academic_year_end) )

        students_match_number = spiritual_surveys.values('student_id_id').annotate(count=Count('student_id_id')).filter(count=number).values_list('student_id_id',flat=True)
        spiritual_surveys = spiritual_surveys.filter(student_id__student_id__in=students_match_number)

        grades = OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)])
        grades[50]='English'
        grades[70]='Vietnamese'

        academic_year_start = datetime.now().year-2013
        academic_years = []
        # generate list of year
        for r in range(academic_year_start):
          academic_years.append(datetime.now().year-r)

        self.assertEqual(resp.context['grades'],grades)
        self.assertEqual(list(resp.context['spiritual_surveys']),list(spiritual_surveys))
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['academic_years'],academic_years)
        self.assertEqual(resp.context['academic_year'],str(academic_year))
        self.assertEqual(resp.context['number'],str(number))


class AchievementTestViewTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','intakesurveys.json',
        'academic.json',
    ]
    def setUp(self):
        self.client = Client()
        self.client.force_login(User.objects.get(username='admin'))
        self.academic = Academic.objects.create(
            student_id = IntakeSurvey.objects.get(pk=2),
            test_date = date.today().isoformat(),
            test_level = 1,
            test_grade_khmer = 90,
            test_grade_math = 90,
            promote = True
        )
        Academic.objects.create(
            student_id = IntakeSurvey.objects.get(pk=2),
            test_date = "2014-06-01",
            test_level = 1,
            test_grade_khmer = 10,
            test_grade_math = 10,
            promote = False
        )

    def test_context(self):
        url = reverse('achievement_test',kwargs={})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/achievementtestreport.html')
        test_date_lists =  Academic.objects.all().exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                            ).values_list('test_date', flat=True).distinct().order_by('-test_date')

        if len(test_date_lists) > 2:
            current_date = test_date_lists[0].isoformat()
            previous_date = test_date_lists[1].isoformat()
            previous_date2 = test_date_lists[2].isoformat()
        else:
             try:
                 current_date = test_date_lists[0].isoformat()
             except:
                 current_date = date.today().isoformat()
             try:
                 previous_date = test_date_lists[1].isoformat()
             except:
                 previous_date = date.today().isoformat()
             previous_date2 = date.today().isoformat()
        grade = 0
        site = 0
        achievement_tests =  Academic.objects.all().filter(test_date=current_date).exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                            )
        students = achievement_tests.count()
        passed = achievement_tests.filter(promote=True).count()

        student_achievements = {}
        for achievement_test in achievement_tests:
            student_achievements[achievement_test] = {}
            try:
                student_achievements[achievement_test]['previous'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date)
            except Exception as e:
                student_achievements[achievement_test]['previous'] = None

            try:
                student_achievements[achievement_test]['previous2'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date2)
            except Exception as e:
                student_achievements[achievement_test]['previous2'] = None

        self.assertEqual(resp.context['student_achievements'],student_achievements)
        self.assertEqual(resp.context['student_achievements'][self.academic],student_achievements[self.academic])
        self.assertEqual(resp.context['students'],students)
        self.assertEqual(resp.context['passed'],passed)
        self.assertEqual(resp.context['current_date'],current_date)
        self.assertEqual(resp.context['previous_date'],previous_date)
        self.assertEqual(resp.context['previous_date2'],previous_date2)
        self.assertEqual(resp.context['site'],site)
        self.assertEqual(resp.context['grade'],grade)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(list(resp.context['test_date_lists']),list(test_date_lists))


    def test_context_date_not_none_site_none_grade_none(self):
        current_date = date.today().isoformat()
        previous_date = "2014-06-01"
        previous_date2 = "2014-01-01"
        site = 0
        grade = 0
        url = reverse('achievement_test',kwargs={'current_date':current_date, 'previous_date': previous_date,'previous_date2' : previous_date2})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/achievementtestreport.html')

        test_date_lists =  Academic.objects.all().exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                            ).values_list('test_date', flat=True).distinct().order_by('-test_date')
        achievement_tests =  Academic.objects.all().filter(test_date=current_date).exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                                )
        students = achievement_tests.count()
        passed = achievement_tests.filter(promote=True).count()

        student_achievements = {}
        for achievement_test in achievement_tests:
            student_achievements[achievement_test] = {}
            try:
                student_achievements[achievement_test]['previous'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date)
            except Exception as e:
                student_achievements[achievement_test]['previous'] = None

            try:
                student_achievements[achievement_test]['previous2'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date2)
            except Exception as e:
                student_achievements[achievement_test]['previous2'] = None
        self.assertEqual(resp.context['student_achievements'],student_achievements)
        self.assertEqual(resp.context['student_achievements'][self.academic],student_achievements[self.academic])
        self.assertEqual(resp.context['students'],students)
        self.assertEqual(resp.context['passed'],passed)
        self.assertEqual(resp.context['current_date'],current_date)
        self.assertEqual(resp.context['previous_date'],previous_date)
        self.assertEqual(resp.context['previous_date2'],previous_date2)
        self.assertEqual(resp.context['site'],site)
        self.assertEqual(resp.context['grade'],grade)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(list(resp.context['test_date_lists']),list(test_date_lists))

    def test_context_date_not_none_site_not_none_grade_none(self):
        current_date = date.today().isoformat()
        previous_date = "2014-06-01"
        previous_date2 = "2014-01-01"
        site = 1
        grade = 0
        url = reverse('achievement_test',kwargs={'current_date':current_date, 'previous_date': previous_date, 'previous_date2':previous_date2, 'site': site})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/achievementtestreport.html')
        test_date_lists =  Academic.objects.all().exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                            ).values_list('test_date', flat=True).distinct().order_by('-test_date')

        achievement_tests =  Academic.objects.all().filter(test_date=current_date,student_id__site=site).exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                                )
        students = achievement_tests.count()
        passed = achievement_tests.filter(promote=True).count()

        student_achievements = {}
        for achievement_test in achievement_tests:
            student_achievements[achievement_test] = {}
            try:
                student_achievements[achievement_test]['previous'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date)
            except Exception as e:
                student_achievements[achievement_test]['previous'] = None

            try:
                student_achievements[achievement_test]['previous2'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date2)
            except Exception as e:
                student_achievements[achievement_test]['previous2'] = None
        self.assertEqual(resp.context['student_achievements'],student_achievements)
        self.assertEqual(resp.context['student_achievements'][self.academic],student_achievements[self.academic])
        self.assertEqual(resp.context['students'],students)
        self.assertEqual(resp.context['passed'],passed)
        self.assertEqual(resp.context['current_date'],current_date)
        self.assertEqual(resp.context['previous_date'],previous_date)
        self.assertEqual(resp.context['previous_date2'],previous_date2)
        self.assertEqual(resp.context['site'],site)
        self.assertEqual(resp.context['grade'],grade)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(list(resp.context['test_date_lists']),list(test_date_lists))

    def test_context_date_not_none_site_none_grade_not_none(self):
        current_date = date.today().isoformat()
        previous_date = "2014-06-01"
        previous_date2 = "2014-01-01"
        site = 0
        grade = 1
        url = reverse('achievement_test',kwargs={'current_date':current_date, 'previous_date': previous_date, 'previous_date2':previous_date2, 'site': site, 'grade': grade})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/achievementtestreport.html')
        test_date_lists =  Academic.objects.all().exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                            ).values_list('test_date', flat=True).distinct().order_by('-test_date')
        achievement_tests =  Academic.objects.all().filter(test_date=current_date,test_level=grade).exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                                )
        students = achievement_tests.count()
        passed = achievement_tests.filter(promote=True).count()

        student_achievements = {}
        for achievement_test in achievement_tests:
            student_achievements[achievement_test] = {}
            try:
                student_achievements[achievement_test]['previous'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date)
            except Exception as e:
                student_achievements[achievement_test]['previous'] = None

            try:
                student_achievements[achievement_test]['previous2'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date2)
            except Exception as e:
                student_achievements[achievement_test]['previous2'] = None

        self.assertEqual(resp.context['student_achievements'],student_achievements)
        self.assertEqual(resp.context['student_achievements'][self.academic],student_achievements[self.academic])
        self.assertEqual(resp.context['students'],students)
        self.assertEqual(resp.context['passed'],passed)
        self.assertEqual(resp.context['current_date'],current_date)
        self.assertEqual(resp.context['previous_date'],previous_date)
        self.assertEqual(resp.context['previous_date2'],previous_date2)
        self.assertEqual(resp.context['site'],site)
        self.assertEqual(resp.context['grade'],grade)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(list(resp.context['test_date_lists']),list(test_date_lists))

    def test_context_date_not_none_site_not_none_grade_not_none(self):
        current_date = date.today().isoformat()
        previous_date = "2014-06-01"
        previous_date2 = "2014-01-01"
        site = 1
        grade = 1
        url = reverse('achievement_test',kwargs={'current_date':current_date, 'previous_date': previous_date, 'previous_date2': previous_date2, 'site': site, 'grade': grade})
        resp = self.client.get(url,follow=True)
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp,'mande/achievementtestreport.html')
        test_date_lists =  Academic.objects.all().exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                            ).values_list('test_date', flat=True).distinct().order_by('-test_date')
        achievement_tests =  Academic.objects.all().filter(test_date=current_date,test_level=grade,student_id__site=site).exclude(
                                                  test_grade_math=None,
                                                  test_grade_khmer=None,
                                                )
        students = achievement_tests.count()
        passed = achievement_tests.filter(promote=True).count()

        student_achievements = {}
        for achievement_test in achievement_tests:
            student_achievements[achievement_test] = {}
            try:
                student_achievements[achievement_test]['previous'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date)
            except Exception as e:
                student_achievements[achievement_test]['previous'] = None

            try:
                student_achievements[achievement_test]['previous2'] = achievement_test.student_id.academic_set.all().exclude(
                        test_grade_math=None,
                        test_grade_khmer=None,
                    ).get(test_date=previous_date2)
            except Exception as e:
                student_achievements[achievement_test]['previous2'] = None

        self.assertEqual(resp.context['student_achievements'],student_achievements)
        self.assertEqual(resp.context['student_achievements'][self.academic],student_achievements[self.academic])
        self.assertEqual(resp.context['students'],students)
        self.assertEqual(resp.context['passed'],passed)
        self.assertEqual(resp.context['current_date'],current_date)
        self.assertEqual(resp.context['previous_date'],previous_date)
        self.assertEqual(resp.context['previous_date2'],previous_date2)
        self.assertEqual(resp.context['site'],site)
        self.assertEqual(resp.context['grade'],grade)
        self.assertEqual(list(resp.context['sites']),list(School.objects.filter(active=True)))
        self.assertEqual(resp.context['grades'],OrderedDict([item for item in GRADES if (item[0]>=1 and item[0]<=6) or (item[0]>=-3 and item[0]<=-1)]))
        self.assertEqual(list(resp.context['test_date_lists']),list(test_date_lists))
