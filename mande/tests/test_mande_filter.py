from django.test import TestCase
from django.test import Client

from django.utils.translation import ugettext_lazy as _
from django.utils.translation import activate, get_language
from django.core.urlresolvers import resolve, reverse
from datetime import date,datetime
import dateutil.parser

from mande.templatetags.mandefilters import *

from mande.models import FREQUENCY
from mande.models import YN
from mande.models import GRADES
from mande.models import RELATIONSHIPS
from mande.models import COHORTS
from mande.models import STATUS
from mande.models import PUBLIC_SCHOOL_GRADES

from mande.models import IntakeSurvey
from mande.models import School
from mande.models import Classroom
from mande.models import Health
from mande.models import PostExitSurvey
from mande.models import ExitSurvey
from mande.models import ClassroomEnrollment
from mande.models import User



class MandeFiltersTestCase(TestCase):
    fixtures = [
        'users.json','schools.json','intakesurveys.json','classrooms.json',
        'healths.json','postexitsurveys.json','exitsurveys.json',
        'classroomenrollment.json','intakeupdates.json','intakeinternals.json'
    ]

    def setUp(self):
        self.client = Client()

    def test_concate(self):
        value = 1
        arg = 2
        data = str(value)+str(arg)
        self.assertEqual(concate(value,arg),data)

    def test_trim(self):
        value = "test "
        data = value.strip()
        self.assertEqual(trim(value),data)

    def test_frequency_display_value_none(self):
        value = None
        data = 'Unknown'
        self.assertEqual(frequency_display(value),data)

    def test_frequency_display_value_not_none(self):
        value = 'EVERY_WEEK'
        rel_dict = dict(FREQUENCY)
        data = rel_dict.get(value, None)
        self.assertEqual(frequency_display(value),data)

    def test_yn_display_value_none(self):
        value = None
        data = 'Unknown'
        self.assertEqual(yn_display(value),data)

    def test_yn_display_value_not_none(self):
        value = 'Y'
        rel_dict = dict(YN)
        data = rel_dict.get(value, None)
        self.assertEqual(yn_display(value),data)

    def test_name_by_sid_value_none(self):
        value = None
        data = 'Not sure who this is!'
        self.assertEqual(name_by_sid(value),data)

    def test_name_by_sid_value_not_none(self):
        value = 1
        data = IntakeSurvey.objects.get(pk=value).name
        self.assertEqual(name_by_sid(value),data)

    def tes_grade_by_id_value_none(self):
        value = None
        data = 'Unknown'
        self.assertEqual(grade_by_id(value),data)

    def test_grade_by_id_value_not_none(self):
        value = 1
        grade_dict = dict(GRADES)
        grade_dict[50] = _('English')
        grade_dict[70] = _('Vietnamese')
        data = grade_dict.get(int(value), None)
        self.assertEqual(grade_by_id(value),data)

    def tes_site_by_id_value_none(self):
        value = 0
        data = 'Unknown'
        self.assertEqual(site_by_id(value),data)

    def test_site_by_id_value_not_none(self):
        value = 1
        data = School.objects.get(pk=value)
        self.assertEqual(site_by_id(value),data)

    def tes_classroom_by_id_value_none(self):
        value = 0
        data = 'Unknown'
        self.assertEqual(classroom_by_id(value),data)

    def test_classroom_by_id_value_not_none(self):
        value = 1
        data = Classroom.objects.get(pk=value)
        self.assertEqual(classroom_by_id(value),data)

    def test_relationship_display_value_none(self):
        value = None
        data = 'Unknown'
        self.assertEqual(relationship_display(value),data)

    def test_relationship_display_value_not_none(self):
        value = 'MOTHER'
        rel_dict = dict(RELATIONSHIPS)
        data = rel_dict.get(value, None)
        self.assertEqual(relationship_display(value),data)

    def test_getStudentGradebyID(self):
        student_id = 1
        grade_dict = dict(GRADES)
        current_grade = 0
        try:
            student = IntakeSurvey.objects.get(pk=student_id)
        except ObjectDoesNotExist:
            return grade_dict.get(current_grade, None)
        try:
            updates = student.intakeupdate_set.exclude(current_grade=None).latest('date')
            current_grade = updates.current_grade
        except ObjectDoesNotExist:
            try:
                intake = student.intakeinternal_set.all().filter().latest('enrollment_date')
                current_grade = intake.starting_grade
            except ObjectDoesNotExist:
                pass
        data = grade_dict.get(current_grade, None)
        self.assertEqual(getStudentGradebyID(student_id),data)


    def test_get_subtotal_of_dental(self):
        dental = dentals= Health.objects.all().filter(appointment_type='Dental')
        arg = 'extractions'
        subtotal = 0
        for student in dental:
            if getattr(student, arg) is not None:
                subtotal +=getattr(student, arg)
        data = subtotal
        self.assertEqual(get_subtotal_of_dental(dental,arg),data)

    def test_get_total_of_dental_arg_none(self):
        dental = dentals= Health.objects.all().filter(appointment_type='Dental')
        dentals = [{'dentals': dental, 'unique': 0, 'group_by_date': '2017-01'}]
        arg = 'None'
        total = 0
        for dental in dentals:
            total+=len(dental['dentals'])
        data = total
        self.assertEqual(get_total_of_dental(dentals,arg),data)

    def test_get_total_of_dental_arg_not_none(self):
        dental = dentals= Health.objects.all().filter(appointment_type='Dental')
        dentals = [{'dentals': dental, 'unique': 0, 'group_by_date': '2017-01'}]
        arg = 'extractions'
        total = 0
        for dental in dentals:
            for student in dental['dentals']:
                if getattr(student, arg) is not None:
                    total +=getattr(student, arg)
        data = total
        self.assertEqual(get_total_of_dental(dentals,arg),data)

    def test_check_if_already_perform_post_exit(self):
        exit_survey_student_id = IntakeSurvey.objects.get(pk=1)
        arg = PostExitSurvey.objects.all()
        post_exit_surveys = arg
        check_already_perform = False
        for post_exit_survey in post_exit_surveys:
            if(post_exit_survey.student_id.student_id==exit_survey_student_id.student_id):
                check_already_perform = True
                break
            else:
                check_already_perform = False
        data = check_already_perform
        self.assertEqual(check_if_already_perform_post_exit(exit_survey_student_id,arg),data)

    def test_get_students_length_by_gender(self):
        students = IntakeSurvey.objects.all()
        arg = 'F'
        gender = arg
        students_by_gender=[]
        for student in students:
            if student.getRecentFields()['gender'] == gender:
                students_by_gender.append(student)
        data = len(students_by_gender)
        self.assertEqual(get_students_length_by_gender(students,arg),data)

    def test_add_year(self):
        year = 2017
        data = year+1
        self.assertEqual(add_year(year),data)

    def test_get_item(self):
        dictionary = dict(GRADES)
        key = '1'
        data = dictionary.get(key)
        self.assertEqual(get_item(dictionary,key),data)

    def test_student_classroom(self):
        ClassroomEnrollment.objects.create(
            classroom_id = Classroom.objects.get(pk=2),
            student_id = IntakeSurvey.objects.get(pk=1),
            enrollment_date = "2014-01-01",
            drop_date = date.today().isoformat()
        )
        student = IntakeSurvey.objects.get(pk=1)
        data =  ClassroomEnrollment.objects.filter(Q(student_id=student) & Q(Q(drop_date=None) | Q(drop_date__gt=date.today().isoformat())))
        self.assertEqual(list(student_classroom(student)),list(data))

    def test_get_academic_year_value_none(self):
        value = None
        data = 'Unknown'
        self.assertEqual(get_academic_year(value),data)

    def test_get_academic_year_value_not_none(self):
        value = 2017
        rel_dict = dict(COHORTS)
        data = rel_dict.get(value, None)
        self.assertEqual(get_academic_year(value),data)

    def test_get_status_value_none(self):
        value = None
        data = 'Unknown'
        self.assertEqual(get_status(value),data)

    def test_get_status_value_not_none(self):
        value = 'COMPLETED'
        rel_dict = dict(STATUS)
        data = rel_dict.get(value, None)
        self.assertEqual(get_status(value),data)

    def test_change_lang_None(self):
        self.client.force_login(User.objects.get(username='admin'))
        url = '/'
        resp = self.client.get(url,follow=True)
        context =  resp.context
        data =  url + get_language() + '/'
        self.assertEqual(change_lang(context),data)

    def test_change_lang_kh(self):
        self.client.force_login(User.objects.get(username='admin'))
        url = '/'
        lang = 'kh'
        resp = self.client.get(url,follow=True)
        context =  resp.context
        data =  url + lang + '/'
        self.assertEqual(change_lang(context,lang),data)

    def test_change_lang_en(self):
        self.client.force_login(User.objects.get(username='admin'))
        url = '/'
        lang = 'en'
        resp = self.client.get(url,follow=True)
        context =  resp.context
        data =  url + lang + '/'
        self.assertEqual(change_lang(context,lang),data)

    def test_get_version(self):
        data = subprocess.check_output(["git","describe"])
        self.assertEqual(get_version(),data)

    def test_public_school_grade_display_value_none(self):
        value = None
        data = 'Unknown'
        self.assertEqual(public_school_grade_display(value),data)

    def test_public_school_grade_display_value_not_none(self):
        value = 'MOTHER'
        rel_dict = dict(PUBLIC_SCHOOL_GRADES)
        data =  rel_dict.get(value, None)
        self.assertEqual(public_school_grade_display(value),data)

    def test_iso_to_date(self):
        value = date.today().isoformat()
        data =  dateutil.parser.parse(value)
        self.assertEqual(iso_to_date(value),data)

    def test_exit_reason_display_value_none(self):
        value = None
        data = 'Unknown'
        self.assertEqual(exit_reason_display(value),data)

    def test_exit_reason_display_value_not_none(self):
        value = 'MOVING'
        rel_dict = dict(EXIT_REASONS)
        data = rel_dict.get(value, None)
        self.assertEqual(exit_reason_display(value),data)
