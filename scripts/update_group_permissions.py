from mande.permissions import update_group_perms
from django.contrib.auth.models import Group, Permission

def run():
    print '***Updating group permissions***'
    update_group_perms()
