#!/bin/bash
echo "Here we go! Cloning ansible"
git clone https://github.com/ansible/ansible.git
cd ansible
git checkout v2.9.14
cd ..
pip install -r ansible/requirements.txt
source ansible/hacking/env-setup
ansible --version

echo "Great. Now let's clone the deployment repo"
git clone git@gitlab.com:asianhope-mis/ansible-jethro.git

echo "That was fun, let's generate a hosts file"
echo "[testing]" > testing
echo $TESTING_ENDPOINT >> testing

echo "Add jethro_version to make sure we clone the right stuff"
# This variable is automatically set
echo "jethro_version: $CI_COMMIT_REF_NAME" >> ansible-jethro/group_vars/testing

echo "jethro_version: $CI_COMMIT_REF_NAME"

echo "And now, let's run our playbook"
ansible-playbook ansible-jethro/redeploy-testing.yml -i testing --skip-tags "fresh" -vv
